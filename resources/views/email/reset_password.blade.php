@extends("layouts.master")
@section("content")

    <div class="grey-overlay"></div>

    <div class="email">

        <div style="margin: 5%"></div>

        <div class="row">
            <div class="reset">
                <div class="col s12">
                    <div class="center-align">
                        <h3 class="blue-text">Reset your password</h3>
                        <div class="divider"></div>
                        <div style="margin: 5%"></div>
                    </div>

                    <p class="center-align">Password</p>
                    <p><input name="password" required type="password" placeholder="Password" /></p>

                    <p class="center-align">Password Confirmation</p>
                    <p><input name="confirm_password" required type="password" placeholder="Password Confirmation" /></p>

                    <input type="hidden" value="{{$email_hash}}" name="email_hash" />

                    <button type="button" class="btn btn-success btn-flat right reset_password">Reset</button>

                </div>
            </div>
        </div>
    </div>

    <div class="pulse"></div>

    <script type="text/javascript">
        $('.grey-overlay').hide();
        $('.pulse').hide();
    </script>

    <style>
        .email{
            width: 50%;
            margin-left: 25%;
            text-align: center;
        }

        .email input{
            text-align: center;
        }

        .btn-flat{
            background-color: transparent;
            color: black;
        }

        .btn-flat:hover{
            background-color: #00A2DE;
            color: white;
        }
    </style>
    <script type="text/javascript" src="{{asset('js/reset.js')}}"></script>
@endsection