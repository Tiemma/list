
@extends("layouts.master")

@section("content")
    <div class="grey-overlay"></div>
    <div class="row">
        <div class="col s9 m9">
            <div class="email-form">
                <div style="margin-top: 5%"></div>
                <div class="email">
                    <p>Please enter your email: &nbsp;
                        <input type="email"  value="" required name="user_email" />
                    <p><button type="button" class="btn btn-flat border send_email right">Reset</button></p>
                    </p>
                </div>
            </div>
        </div>

        <div class="col m3 s3">
            <div class="ads-content">
                <img src="{{asset('/images/ad_side_2.png')}}" />
            </div>
        </div>
    </div>
    <div class="pulse"></div>

    <script type="text/javascript" src="{{asset('js/reset.js')}}"></script>
    <script type="text/javascript">
        $('.grey-overlay').hide();
        $('.pulse').hide();
    </script>


    <style>

        .email{
            position: relative;
            top: 60%;
            left: 10%;
            width: 50%;
            text-align: center;
        }

        /* .email-form{
             background-color: white;
             box-shadow: 2px 2px 3px 0 rgba(0,0,0,0.3) !important;
         }*/

        .email input{
            text-align: center;
            width: 90%;
        }

        .btn-flat{
            background-color: transparent;
            color: black;
        }

        .btn-flat:hover{
            background-color: #00A2DE;
            color: white;
        }

    </style>
@endsection
