@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{ url('/admin/companies') }}">Companies</a>
        </li>
        <li class="active">
            <strong>Edit company</strong>
        </li>
    </ol>


    <div class="row">
            <div class="col-md-12">
                
                <div class="panel panel-primary" data-collapsed="0">
                
                    <div class="panel-heading">
                        <div class="panel-title">
                            Company Details
                        </div>
                    </div>
                    
                    <div class="panel-body">
                                
                        <form action="{{ url('admin/company/'.$company['id'].'/edit') }}" method="post" role="form" enctype="multipart/form-data" class="form-horizontal form-groups validate">
                            
                            <div class="form-group">
                                <label for="company_name" class="col-sm-3 control-label">Company Name</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ $company['name'] }}" data-validate="required" data-message-required="Required" placeholder="Company Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="company_address" class="col-sm-3 control-label">Company Location</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="company_location" name="company_location" value="{{ $company['location'] }}" data-validate="required" data-message-required="Required" placeholder="Company Location">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="known_for" class="col-sm-3 control-label">Investing in</label>
                                
                                <div class="col-sm-5">
                                    <input class="form-control input-lg tagsinput" id="brief_profile" name="brief_profile" value="{{ $company['brief_profile'] }}" placeholder="press enter after each input" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Category</label>
                                
                                <div class="col-sm-5">
                                    <select class="form-control" name="industry_id" id="industry_id">
                                        <option value="{{ $company['industry_id'] }}" selected>Choose industry</option>
                                        @foreach($industries as $industry)
                                        <option value={{ $industry['id'] }}>{{ $industry['industry_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Trending</label>
                                
                                <div class="col-sm-5">
                                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                                        <input type="checkbox" {!! $company['trending'] ? "checked" : "" !!} name="trending"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Company Logo</label>
                                
                                <div class="col-sm-5">
                                    
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                            <img src="{{ $company['logo_url'] ? asset($company['logo_url']) : asset('img/techpoint.png') }}" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="company_logo" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Create</button>
                                    <button type="reset" class="btn">Reset</button>
                                </div>
                            </div>
                        
                        </form>
                        
                    </div>
                
                </div>
            
            </div>
        </div>
@endsection

@section('scripts')
      <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-tagsinput.min.js') }}"></script>
@endsection
