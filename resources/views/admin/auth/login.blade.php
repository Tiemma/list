@extends('admin.admin_login_layout')

@section('content')

<div class="login-form">
    
    <div class="login-content">
 
        <div class="form-login-error">
            <h3>Invalid login</h3>
            <p>For SUPER admin, enter <strong>demo</strong>/<strong>demo</strong> as email and password.</p>
        </div> 

        <!--{{--<form method="POST" role="form" id="form_login" >--}}-->
            <form action="{{ url('/admin/login') }}" method="POST" role="form">
            {!! csrf_field() !!}            
            <div class="form-group">
                
                <div class="input-group">
                    <div class="input-group-addon">
                        <i style="color:white" class="entypo-mail"></i>
                    </div>
                    
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" data-validate="email" />
                </div>
                
            </div>
            
            <div class="form-group">
                
                <div class="input-group">
                    <div class="input-group-addon">
                        <i style="color:white" class="entypo-key"></i>
                    </div>
                    
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                </div>
                
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-login">
                    <i class="entypo-login"></i>
                    Login In
                </button>
            </div>
            
        </form>
        
        
        <div class="login-bottom-links">
            
            <a href="" class="link">Forgot your password?</a>
            
        </div>
        
    </div>
</div>

@stop
