@extends('admin.admin_register_layout')
@section('content')

<div class="login-form">
		
		<div class="login-content">
			
			<form role="form" method="POST" action="{{ url('admin/register') }}" >
                {{ csrf_field() }} 
				
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Full Name" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user-add"></i>
						</div>
						
						<input type="text" class="form-control" name="username" value="{{ old('username') }}" id="username" placeholder="Username" data-mask="[a-zA-Z0-1\.]+" data-is-regex="true" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-mail"></i>
						</div>
						
						<input type="text" class="form-control" name="email" value="{{ old('email') }}" id="email" data-mask="email" placeholder="E-mail" autocomplete="off" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-lock"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Choose Password" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-lock"></i>
						</div>
						
						<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Enter Password again" autocomplete="off" />
					</div>
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-success btn-block btn-login">
						<i class="entypo-right-open-mini"></i>
						Register
					</button>
				</div>
				
			</form>
			
			
			<div class="login-bottom-links">
				
				<a href="{{ url('/admin/login') }}" class="link">
					<i class="entypo-lock"></i>
					Return to Login Page
				</a>
				
				<br />
				
				<a href="#">List</a>  - <a href="#">Privacy Policy</a>
				
			</div>
			
		</div>
		
	</div>

@endsection