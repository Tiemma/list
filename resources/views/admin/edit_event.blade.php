@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{ url('/admin/events') }}">Events</a>
        </li>
        <li class="active">
            <strong>Edit event details</strong>
        </li>
    </ol>


    <div class="row">
            <div class="col-md-12">
                
                <div class="panel panel-primary" data-collapsed="0">
                
                    <div class="panel-heading">
                        <div class="panel-title">
                            Event Details
                        </div>
                    </div>
                    
                    <div class="panel-body">
                                
                        <form action="{{ url('admin/event/'.$event['id'].'/edit') }}" method="post" role="form" enctype="multipart/form-data" class="form-horizontal form-groups validate">
                            
                            <div class="form-group">
                                <label for="event_name" class="col-sm-3 control-label">Event Name</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="event_name" name="event_name" value="{{ $event['name'] }}" data-validate="required" placeholder="Event Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event_address" class="col-sm-3 control-label">Event Address</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="event_address" name="event_address" value="{{ $event['address'] }}" data-validate="required" placeholder="Event Location">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event_link" class="col-sm-3 control-label">Event Link</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="event_link" name="registration_link" value="{{ $event['registration_link'] }}" data-validate="required" placeholder="Event Link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event_brief" class="col-sm-3 control-label">Event Description</label>
                                
                                <div class="col-sm-5">
                                    <textarea class="form-control autogrow" id="event_brief" name="event_brief"  data-validate="required" placeholder="Event Description">{{ $event['brief_profile'] }}</textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="event_contact_name" class="col-sm-3 control-label">Event Contact Name</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="event_contact_name" data-validate="required" name="event_contact_name" value="{{ $event['contact_name'] }}" placeholder="Event Contact Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event_contact_email" class="col-sm-3 control-label">Event Contact Email</label>
                                
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                        <input type="text" class="form-control" id="event_contact_email" name="event_contact_email" value="{{ $event['contact_email'] }}" data-validate="Required, email" placeholder="Event Contact Email">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event_contact_phone" class="col-sm-3 control-label">Event Contact Mobile</label>
                                
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="entypo-phone"></i></span>
                                        <input type="text" class="form-control" id="event_contact_phone" name="event_contact_phone" value="{{ $event['contact_phone_no'] }}" data-validate="number,minlength[11],maxlength[11]" placeholder="Event Contact Mobile">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Category</label>
                                
                                <div class="col-sm-5">
                                    <select class="form-control" name="industry_id" id="industry_id">
                                        <option value="{{ $event['industry_id'] }}" selected>Choose industry</option>
                                        @foreach($industries as $industry)
                                        <option value="{{ $industry['id'] }}">{{ $industry['industry_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Trending</label>
                                
                                <div class="col-sm-5">
                                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                                        <input type="checkbox" name="trending" {{ $event['trending'] ? 'checked' : "" }}/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Event Logo</label>
                                
                                <div class="col-sm-5">
                                    
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                            <img src="{{ $event['logo_url'] ? asset($event['logo_url']) : asset('img/techpoint.png') }}" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="event_logo" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <button type="reset" class="btn">Reset</button>
                                </div>
                            </div>
                        
                        </form>
                        
                    </div>
                
                </div>
            
            </div>
        </div>
@endsection

@section('scripts')
        <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
@endsection
