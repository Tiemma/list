<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="List Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">

	<title>List | Admin</title>

	<link rel="stylesheet" href="{{ asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons/entypo/css/entypo.css') }}">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/neon-core.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/neon-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/neon-forms.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

	<script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="#">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu fixed">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="{{ url('/admin/home') }}">
						<img src="{{ asset('assets/images/techpoint.png') }}" width="120" alt="" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>

			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="{{Request::path() == '/admin' || Request::path() == '/admin/home' ? 'active' : ''}} opened">
					<a href="{{ url('/admin/home') }}">
						<i class="entypo-gauge"></i>
						<span class="title">Dashboard</span>
					</a>
				</li>

				<li class="">
					<a href="{{ url('/admin/users') }}">
						<i class="entypo-users"></i>
						<span class="title">Users on List</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/startups') }}">
						<i class="entypo-layout"></i>
						<span class="title">Startups on List</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/investors') }}">
						<i class="entypo-briefcase"></i>
						<span class="title">Investors</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/events/') }}">
						<i class="entypo-calendar"></i>
						<span class="title">Events</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/schools') }}">
						<i class="entypo-book"></i>
						<span class="title">Schools</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/companies') }}">
						<i class="entypo-window"></i>
						<span class="title">Companies</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/industries') }}">
						<i class="entypo-network"></i>
						<span class="title">Industries</span>
					</a>
				</li>
				<li class="">
					<a href="{{ url('/admin/investments') }}">
						<i class="entypo-suitcase"></i>
						<span class="title">Investments</span>
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="main-content">
				
		<div class="row hidden-print">
			
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
				
				<ul class="user-info pull-left pull-none-xsm">
					
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{ asset('assets/images/thumb-1@2x.png') }}" alt="" class="img-circle" width="44" />
							{{ Auth::guard('admin')->user()->name }}
						</a>
						
					</li>
				</ul>
									
				
			</div>

			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
				<ul class="list-inline links-list pull-right">
					<li>
						<a href="{{ url('/admin/logout') }}">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
			</div>

		</div>



	<hr />

		@yield('content')

	<!-- Footer -->
		<footer class="main">
			
			&copy; 2016 <strong>List</strong> Admin designed by <a href="http://iqubesolutions.com.ng" target="_blank">iQube Labs</a>

		</footer>
	</div>
</div>


<!-- Bottom scripts (common) -->
<script src="{{ asset('assets/js/gsap/TweenMax.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/joinable.js') }}"></script>
<script src="{{ asset('assets/js/resizeable.js') }}"></script>
<script src="{{ asset('assets/js/neon-api.js') }}"></script>
<script src="{{ asset('assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>

<!-- JavaScripts initializations and stuff -->
<script src="{{ asset('assets/js/neon-custom.js') }}"></script>

<!-- Demo Settings -->
<script src="{{ asset('assets/js/neon-demo.js') }}"></script>

@yield('scripts')

<script src="{{ asset('assets/js/toastr.js') }}"></script>
	@if (session('error'))
	    <script type="text/javascript">
	        var opts = {
	        "closeButton": true,
	        "debug": false,
	        "positionClass": "toast-top-full-width",
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "timeOut": "5000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut"
	    };
	    
	    toastr.error("{{session('error')}}", "Charme", opts);
	    </script>
	@endif

	@if (session('success'))
	    <script type="text/javascript">
	        var opts = {
	        "closeButton": true,
	        "debug": false,
	        "positionClass": "toast-top-right",
	        "onclick": null,
	        "showDuration": "300",
	        "hideDuration": "1000",
	        "timeOut": "5000",
	        "extendedTimeOut": "1000",
	        "showEasing": "swing",
	        "hideEasing": "linear",
	        "showMethod": "fadeIn",
	        "hideMethod": "fadeOut"
	    };
	    
	    toastr.success("{{session('success')}}", "Charme", opts);
	    </script>
	@endif
</body>
</html>