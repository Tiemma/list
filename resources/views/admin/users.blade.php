@extends('admin.admin_area_layout')
@section('content')
<ol class="breadcrumb bc-3" >
	<li>
		<a href="{{ url('/admin/home') }}"><i class="fa fa-home"></i>Home</a>
	</li>
	<li>
		<a href="{{ url('/admin/users') }}">Management</a>
	</li>
	<li class="active">
		<strong>Users</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-3 col-sm-5">
		<form action="{{ url('/admin/user/search') }}" method="get" role="form" class="search-form-full">
			<div class="form-group">
				<input type="text" class="form-control" name="search" id="search-input" placeholder="Search ..." />
				<i class="entypo-search"></i>
			</div>

		</form>

	</div>
	<div class="col-md-6"></div>
	<div class="col-md-3">
		<a class="btn btn-blue btn-icon icon-left" href="{{ url('/admin/user/create') }}"><i class="entypo-user-add"></i>Create new user</a>
	</div>
</div>

@if(isset($query))
<div class="row">
    <div class="col-md-3 col-sm-5"><h3>{{ $total }} result(s) for <strong>{{ $query }}</strong></h3></div>
    <div class="col-md-6"></div>
</div>
@endif

<!-- Member Entries -->
@foreach ($users as $user)
	<!-- Single Member -->
<div class="member-entry">
		
	<a href="#" class="member-img">
		<img src="{{ $user['profile_pics_url'] ? asset($user['profile_pics_url']) : asset('img/techpoint.png') }}" class="img-rounded img-responsive" alt="Profile Picture"/>
		<i class="entypo-forward"></i>
	</a>
	
	<div class="member-details">
		<h4>
			<a href="#">{{ $user['first_name'] }} {{ $user['last_name'] }}</a>
			@if($user['confirmation_status'])
				<i style="color:green" class="fa fa-spin fa-star"></i>
			@else
				<i class="fa fa-spin fa-star-o"></i>
			@endif
		</h4>
		
		<!-- Details with Icons -->
		<div class="row info-list">
			
			<div class="col-sm-3">
				<i class="entypo-book"></i>
				School <a href="#">{{ $schools->find($user['school_id'])['name'] or 'Unavailable' }}</a>
			</div>
			
			<div class="col-sm-3">
				<i class="entypo-mail"></i>
				<a href="#">{{ $user['email'] or 'Unavailable' }}</a>
			</div>
			
			<div class="col-sm-3">
				<i class="entypo-phone"></i>
				<a href="#">{{ $user['phone_number'] or 'Unavailable' }}</a>
			</div>
			
			<div class="col-sm-2">
				
				<a class="btn btn-xs btn-blue btn-icon icon-left" href="#"><i class="entypo-info"></i>View More</a>
			</div>

			<div class="clear"></div>
			
			<div class="col-sm-3">
				<i class="entypo-user"></i>
				<a href="#">{{ $user['sex'] or 'Unavailable' }}</a>
			</div>
			
			<div class="col-sm-3">
				<i class="entypo-twitter"></i>
				<a href="#">{{-- unserialize($user['social_channel'])['twitter'] ?  unserialize($user['social_channel'])['twitter'] : --}} {{ 'Unavailable' }}</a>
			</div>
			
			<div class="col-sm-3">
				<i class="entypo-facebook"></i>
				<a href="#">{{-- unserialize($user['social_channel'])['facebook'] ? unserialize($user['social_channel'])['facebook'] : --}} {{'Unavailable' }}</a>
			</div>

			<div class="col-sm-3">
				
				<a class="btn btn-xs btn-green btn-icon icon-left" href="{{ url('/admin/user/'.$user['id'].'/edit') }}"><i class="entypo-pencil"></i>Update details</a>
			</div>

			<div class="clear"></div>
			
			<div class="col-sm-3">
				
			</div>
			
			<div class="col-sm-3">
				<i class="entypo-linkedin"></i>
				<a href="#">{{-- unserialize($user['social_channel'])['linkedin'] ? unserialize($user['social_channel'])['linkedin'] : --}} {{ 'Unavailable' }}</a>
			</div>

			<div class="col-sm-3">
				DOB: <a href="#"><i class="entypo-calendar"></i>{{ $user['dob'] or 'Unavailable' }}</a>
			</div>

			<div class="col-sm-3">
				<form action="{{ url('/admin/user/'.$user['id'].'/delete') }}" method="POST">
					{!! csrf_field() !!}
					{{ method_field('DELETE') }}
					
                    <button class="btn btn-xs btn-red btn-icon icon-left"><i class="entypo-cancel"></i>Delete User</button>
				</form>
			</div>

		</div>
	</div>
	
</div>

@endforeach

@endsection

@section('scripts')
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons/font-awesome/css/font-awesome.min.css') }}">

	<!-- Imported scripts on this page -->
	<script src="{{ asset('assets/js/tocify/jquery.tocify.min.js') }}"></script>

@endsection