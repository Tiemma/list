@extends('admin.admin_area_layout')
@section('content')
<ol class="breadcrumb bc-3" >
	<li>
		<a href="home"><i class="fa fa-home"></i>Home</a>
	</li>
	<li>
		<a href="#">List</a>
	</li>
	<li class="active">
		<strong>Schools</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-3 col-sm-5">
		<h2>Schools</h2>
	</div>
	<div class="col-md-6"></div>
	<div class="col-md-3">
		<a class="btn btn-blue btn-icon icon-left" href="{{ url('/admin/school/create') }}"><i class="entypo-plus"></i>Add new school</a>
	</div>
</div>

<div>
<br />
		
		<script type="text/javascript">
		jQuery( document ).ready( function( $ ) {
			var $table1 = jQuery( '#table-1' );
			
			// Initialize DataTable
			$table1.DataTable( {
				"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bStateSave": true
			});
			
			// Initalize Select Dropdown after DataTables is created
			$table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
				minimumResultsForSearch: -1
			});
		} );
		</script>
		
		<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr class="replace-inputs">
					<th>School Name</th>
					<th>Location</th>
					{{--<th>Known For</th>--}}
					<th>Brief Summary</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($schools as $school)
				<tr class="">
					<td>{{ $school['name'] }}
						@if($school['trending'])
							<i style="color:green" class="fa fa-spin fa-star"></i>
						@else
							<i class="fa fa-spin fa-star-o"></i>
						@endif</td>
					<td>{{ $school['location'] }}</td>
					{{--<td>{{ $school['known_for'] }}</td>--}}
					<td>{{ $school['brief_summary'] }}</td>
					<td>
						<form action="{{ url('/admin/school/'.$school['id'].'/delete') }}" method="POST">
							{!! csrf_field() !!}
							{{ method_field('DELETE') }}			
		    	            <a href="{{ url('/admin/school/'.$school['id'].'/edit') }}" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>

							<button class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</button>
						</form>

						{{--<a href="#" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
						
						<a href="#" class="btn btn-info btn-sm btn-icon icon-left">
							<i class="entypo-info"></i>
							Profile
						</a>--}}
						
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>School Name</th>
					<th>Location</th>
					{{--<th>Known for</th>--}}
					<th>Brief Summary</th>
					<th>Action</th>
				</tr>
			</tfoot>
		</table>
		
		<br />


	</div>
		<script src="{{ asset('assets/js/datatables/datatables.js') }}"></script>

	
@endsection

@section('scripts')
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/js/datatables/datatables.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/js/select2/select2-bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/js/select2/select2.css') }}">

	<!-- Imported scripts on this page -->
	<script src="{{ asset('assets/js/tocify/jquery.tocify.min.js') }}"></script>
	<script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
	
@endsection