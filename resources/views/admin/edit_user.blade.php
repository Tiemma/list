@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{url('/admin/users')}}">Users</a>
        </li>
        <li class="active">
            <strong>Edit Details</strong>
        </li>
    </ol>
    <div class="profile-env hidden-print">

        <header class="row">

            <div class="col-sm-2">

                <small>
                    <a href="#" class="profile-picture">
                        <img src="{!! $user['profile_pics_url'] ? asset($user['profile_pics_url']) : asset('img/techpoint.png')!!}" class="img-responsive img-circle" style="max-width: 150px; max-height: 100px"/>
                    </a>
                </small>

            </div>

            <div class="col-sm-6">

                <ul class="profile-info-sections">
                    <li>
                        <div class="profile-name">
                            <strong>
                                <a href="#">{{$user['first_name']}} {{$user['last_name']}}</a>
                            </strong>
                        </div>
                    </li>

                </ul>

            </div>

        </header>
    </div>
    <br/>

    <form action="{{ url('/admin/user/'.$user['id'].'/edit') }}" id="form" method="post" enctype="multipart/form-data" class="form-horizontal form-wizard validate">
        {!! csrf_field() !!}

        <div class="tab-content">
            <h4>Personal details</h4>
            <hr />
            <div class="form-group">
                <label for="first_name" class="col-sm-3 control-label">First Name</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{!!$user['first_name']!!}" data-validate="required">
                </div>
            </div>

            <div class="form-group">
                <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{!! $user['last_name'] !!}" data-validate="required">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Email</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-mail"></i>
                        </div>
                        <input type="text" class="form-control" name="email" id="email" value="{!! $user['email'] !!}" data-validate="required,email"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Password</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>

                        <input type="password" class="form-control" name="password" id="password" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Phone Number</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-phone"></i>
                        </div>

                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{!! $user['phone_number']!!}" data-validate="number,minlength[11],maxlength[11]"/>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Date of Birth</label>
                
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <a href="#"><i class="entypo-calendar"></i></a>
                        </div>
                        
                        <input type="text" name="DOB" value="{!! $user['dob']  !!}" class="form-control datepicker" data-mask="date" data-format="dd/mm/yyyy">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Gender</label>
                
                <div class="col-sm-3">
                    <select name="gender" class="form-control">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label class=" control-label col-sm-3">Confirm Status</label>
                
                <div class="col-sm-5">
                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                        <input type="checkbox" {{ $user['confirmation_status'] ? "checked" : "" }} name="status"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                    <label class="col-sm-3 control-label">Brief Profile</label>

                <div class="col-sm-5">
                    <textarea name="brief_profile" id="brief_profile" rows="4" cols="65">{!! $user['brief_profile'] !!}</textarea>
                </div>
            </div>

           <div class="form-group">
                <label class="col-sm-3 control-label">Twitter</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-twitter"></i>
                        </div>
                        <input type="text" id="twitter" class="form-control" value="{{-- unserialize($user['social_channel'])['twitter'] --}}" name="twitter" placeholder="Twitter username"><br/><br/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Facebook</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-facebook"></i>
                        </div>
                        <input type="text" class="form-control" id="facebook" value="{{-- unserialize($user['social_channel'])['facebook'] --}}" name="facebook" placeholder="Facebook username"><br/><br/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Linkedin</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-linkedin"></i>
                        </div>
                        <input type="text" class="form-control" id="linkedin" value="{{-- unserialize($user['social_channel'])['linkedin'] --}}" name="linkedin" placeholder="linkedin username">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-3">Skill</label>

                <div class="col-sm-5">
                    <input type="text" value="{{ $skills }}" name="skills" class="form-control tagsinput" />
                </div>
            </div>

            
            <div class="form-group">
                <label class="control-label col-sm-3">School</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-book"></i>
                        </div>

                        <select name="school_id" id="school_id" class="select2" data-allow-clear="true" data-placeholder="Select a school...">
                            <option></option>
                            <optgroup label="Schools" value selected>
                                @foreach($schools as $school)
                                    <option value="{{ $school['id'] }}">{{ $school['name'] }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        
            <div class="form-group">
                <label class="col-sm-3 control-label">Profile Pic Upload</label>
                
                <div class="col-sm-5">
                    
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                            <img src="{{ $user['profile_pics_url'] ? asset($user['profile_pics_url']) : asset('img/techpoint.png') }}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                        <div>
                            <span class="btn btn-white btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="profile_pics_url" accept="image/*">
                            </span>
                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                    
                </div>
            </div>

            <br />
            
            <h4>Projects</h4>
            <hr />
            <div class="form-group">
                <label for="project_name" class="col-sm-3 control-label">Name</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="project_name" name="project_name" value="{{ "" }}" >
                </div>
            </div>
            
            <div class="form-group">
                <label for="project_role" class="col-sm-3 control-label">Role</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="project_role" name="project_role" value="{{ "" }}">
                </div>
            </div>
            
            <div class="form-group">
                    <label class="col-sm-3 control-label">Project Description</label>

                <div class="col-sm-5">
                    <textarea name="project_description" id="project_description" rows="4" cols="65">{!! "" !!}</textarea>
                </div>
            </div>
            
            <br />

{{--
            <h4>Work Experience</h4>
            <hr />

            <strong>Current &amp; Past Jobs</strong>
            <br />
            <br />
            
            <div class="row">
            
                <div class="col-sm-1">
                    <label class="control-label">&nbsp;</label>
                    <p class="text-right">
                        <span class="label label-info">1</span>
                    </p>
                </div>
                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="job_position_1">Company Name</label>
                        <input class="form-control" name="" id="job_position_1" placeholder="Your current job" />
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="job_position_1">Job Position</label>
                        <input class="form-control" name="" id="job_position_1" placeholder="Your current position" />
                    </div>
                </div>
                
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="job_position_start_date_1">Start Date</label>
                        <input class="form-control datepicker" name="" id="job_position_start_date_1" placeholder="(Optional)" />
                    </div>
                </div>
                
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="job_position_end_date_1">End Date</label>
                        <input class="form-control datepicker" name="" id="job_position_end_date_1" placeholder="(Optional)" />
                    </div>
                </div>
                
            </div>
            --}}

            <br />
            <br />

            <hr />

            <div class="col-sm-7"></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Update Details</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </div>

        </div>
    </form>
    
@endsection

@section('scripts')
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="{{ url('assets/js/daterangepicker/daterangepicker-bs3.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">

    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/daterangepicker/daterangepicker.js') }}"></script> 
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-tagsinput.min.js') }}"></script>

@endsection