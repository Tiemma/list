@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{url('/admin/users')}}">Users</a>
        </li>
        <li class="active">
            <strong>New user</strong>
        </li>
    </ol>
    

    <div class="well well-sm">
        <h4>Please fill the details to register new user.</h4>
    </div>

    <form action="{{ url('admin/user/create') }}" id="form" method="post" enctype="multipart/form-data" class="form-groups form-horizontal validate">
        {!! csrf_field() !!}

        <div class="tab-content">

            <div class="form-group">
                <label for="first_name" class="col-sm-3 control-label">First Name</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="first_name" name="first_name" data-validate="required">
                </div>
            </div>

            <div class="form-group">
                <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="last_name" name="last_name" data-validate="required">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Email</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-mail"></i>
                        </div>
                        <input type="text" class="form-control" name="email" id="email" data-validate="required,email"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Password</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>

                        <input type="password" class="form-control" name="password" id="password" data-validate="required,minlength[6]" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Phone Number</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-phone"></i>
                        </div>

                        <input type="text" class="form-control" id="phone_number" name="phone_number" data-validate="number,minlength[11],maxlength[11]"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Date of Birth</label>
                
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <a href="#"><i class="entypo-calendar"></i></a>
                        </div>
                        
                        <input type="text" name="DOB" class="form-control datepicker" data-mask="date" data-format="dd/mm/yyyy">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Gender</label>
                
                <div class="col-sm-3">
                    <select name="gender" class="form-control">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label class=" control-label col-sm-3">Confirm Status</label>
                
                <div class="col-sm-5">
                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                        <input type="checkbox" name="status"/>
                    </div>
                </div>
            </div>

            

            <br/>
            
            <div class="col-sm-3"></div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
                <button type="reset" class="btn">Reset</button>
            </div>

        </div>
    </form>

    @endsection

@section('scripts')
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="{{ url('assets/js/daterangepicker/daterangepicker-bs3.css') }}">

    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/daterangepicker/daterangepicker.js') }}"></script> 
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>

@endsection