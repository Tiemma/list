@extends('admin.admin_area_layout')
@section('content')
<ol class="breadcrumb bc-3" >
	<li>
		<a href="home"><i class="fa fa-home"></i>Home</a>
	</li>
	<li>
		<a href="#">List</a>
	</li>
	<li class="active">
		<strong>Investments</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-3 col-sm-5">
		<h2>Investments</h2>
	</div>
	<div class="col-md-6"></div>
	<div class="col-md-3">
		{{--<a class="btn btn-blue btn-icon icon-left" href="{{ url('/admin/investment/create') }}"><i class="entypo-plus"></i>Add new industry</a>--}}
	</div>
</div>

<div>
<br />
		
		<script type="text/javascript">
		jQuery( document ).ready( function( $ ) {
			var $table1 = jQuery( '#table-1' );
			
			// Initialize DataTable
			$table1.DataTable( {
				"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bStateSave": true
			});
			
			// Initalize Select Dropdown after DataTables is created
			$table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
				minimumResultsForSearch: -1
			});
		} );
		</script>
		
		<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr class="replace-inputs">
					<th>#</th>
					<th>Investor</th>
					<th>Startup</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				@foreach($investments as $id => $investment)
				<tr class="">
					<td>{{ $id+1 }}
					<td>{{ $investors->find($investment['investor_id'])['name'] }}</td>
					<td>{{ $startups->find($investment['startup_id'])['startup_name'] or 'Unavailable' }}</td>
					<td>{{ $investment['amount'] }}			
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Investor</th>
					<th>Startup</th>
					<th>Amount</th>
				</tr>
			</tfoot>
		</table>
		
		<br />


	</div>
		<script src="{{ asset('assets/js/datatables/datatables.js') }}"></script>

	
@endsection

@section('scripts')
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/js/datatables/datatables.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/js/select2/select2-bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/js/select2/select2.css') }}">

	<!-- Imported scripts on this page -->
	<script src="{{ asset('assets/js/tocify/jquery.tocify.min.js') }}"></script>
	<script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
	
@endsection