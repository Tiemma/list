@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{ url('/admin/investors') }}">Investors</a>
        </li>
        <li class="active">
            <strong>New investor</strong>
        </li>
    </ol>


    <div class="row">
            <div class="col-md-12">
                
                <div class="panel panel-primary" data-collapsed="0">
                
                    <div class="panel-heading">
                        <div class="panel-title">
                            Investor Details
                        </div>
                    </div>
                    
                    <div class="panel-body">
                                
                        <form action="{{ url('admin/investor/create') }}" method="post" role="form" enctype="multipart/form-data" class="form-horizontal form-groups validate">
                            
                            <div class="form-group">
                                <label for="investor_name" class="col-sm-3 control-label">Investor</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="investor_name" name="investor_name" data-validate="required" placeholder="Investor Name or group">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="investor_email" class="col-sm-3 control-label">Investor Email</label>
                                
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                        <input type="text" class="form-control" id="investor_email" name="email" data-validate="required,email" placeholder="Investor Email">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="investor_phone" class="col-sm-3 control-label">Investor Mobile</label>
                                
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="entypo-phone"></i></span>
                                        <input type="text" class="form-control" id="event_contact_phone" name="investor_phone" data-validate="number,minlength[11],maxlength[11]" placeholder="Investor Mobile">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Date of birth</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="DOB" data-mask="date" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event_address" class="col-sm-3 control-label">Brief Profile</label>
                                
                                <div class="col-sm-5">
                                    <textarea class="form-control autogrow" id="investor_profile" name="investor_profile" data-validate="required" placeholder="Brief Profile"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Company</label>
                                
                                <div class="col-sm-5">
                                    
                                    <select name="company_id" class="select2" data-allow-clear="true" data-placeholder="Select one company...">
                                        <option></option>
                                        <optgroup label="Companies">
                                            @foreach($companies as $company)
                                                <option value="{{ $company['id'] }}">{{ $company['name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Startup invested in</label>
                                
                                <div class="col-sm-5">
                                    
                                    <select name="startup_id" class="select2" data-placeholder="Select startup(s)" >
                                        @foreach($startups as $startup)
                                            <option value="{{ $startup['id'] }}" >{{ $startup['startup_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Trending</label>
                                
                                <div class="col-sm-5">
                                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                                        <input type="checkbox" name="trending"/>
                                    </div>
                                </div>
                            </div>

                         {{--   <div class="form-group">
                                <label class="col-sm-3 control-label">Investor profile pic</label>
                                
                                <div class="col-sm-5">
                                    
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                            <img src="{{ asset('img/techpoint.png')}}" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="profile_pic" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>--}}

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Create</button>
                                    <button type="reset" class="btn">Reset</button>
                                </div>
                            </div>
                        
                        </form>
                        
                    </div>
                
                </div>
            
            </div>
        </div>
@endsection

@section('scripts')
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="{{ asset('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/js/select2/select2.css') }}">
    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>

@endsection
