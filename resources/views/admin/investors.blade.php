@extends('admin.admin_area_layout')
@section('content')
<ol class="breadcrumb bc-3" >
	<li>
		<a href="home"><i class="fa fa-home"></i>Home</a>
	</li>
	<li>
		<a href="#">List</a>
	</li>
	<li class="active">
		<strong>Investors</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-3 col-sm-5">
		<form action="{{ url('/admin/investor/search') }}" method="get" role="form" class="search-form-full">
			<div class="form-group">
				<input type="text" class="form-control" name="search" id="search-input" placeholder="Search..." />
				<i class="entypo-search"></i>
			</div>

		</form>
	</div>
	<div class="col-md-6"></div>
	<div class="col-md-3">
		<a class="btn btn-blue btn-icon icon-left" href="{{ url('/admin/investor/create') }}"><i class="entypo-plus"></i>Add new investor</a>
	</div>
</div>

@if(isset($query))
<div class="row">
    <div class="col-md-3 col-sm-5"><h3>{{ $total }} result(s) for <strong>{{ $query }}</strong></h3></div>
    <div class="col-md-6"></div>
</div>
@endif

@foreach($investors as $investor)
		<!-- Single Member -->
		<div class="member-entry">
				
			<a href="#" class="member-img">
				<img src="{{ $investor['profile_pics_url'] ? asset($investor['profile_pics_url']) : asset('img/techpoint.png') }}" class="img-rounded" />
				<i class="entypo-forward"></i>
			</a>
			
			<div class="member-details">
				<h4>
					<a href="#">{{ $investor['name'] }}</a>
					@if($investor['trending'])
						<i style="color:green" class="fa fa-spin fa-star"></i>
					@else
						<i class="fa fa-spin fa-star-o"></i>
					@endif
				</h4>
				
				
				<!-- Details with Icons -->
				<div class="row info-list">
					
					<div class="col-sm-4">
						<i class="entypo-briefcase"></i>
						Member of <a href="#">{{ $companies->find($investor['company_id'])['name'] or 'Unavailable' }}</a>
					</div>
					
					<div class="col-sm-4">
						<i class="entypo-mail"></i>
						<a href="#">{{ $investor['email'] }}</a>
					</div>

					<div class="col-sm-4">
						<a class="btn btn-green btn-xs btn-icon icon-left" href="{{ url('/admin/investor/'.$investor['id'].'/edit') }}"><i class="entypo-pencil"></i>Update details</a>
					</div>
					
					<div class="clear"></div>

					<div class="col-sm-4">
						Invested in <a href="#">{{ $startups->find($investor['startup_id'])['startup_name'] or 'Unavailable' }}</a>
					</div>

					<div class="col-sm-4">
						<i class="entypo-phone"></i>
						<a href="#">{{ $investor['phone_number'] or 'Unavailable'}}</a>
						</div>

					<div class="col-sm-4">
						<form action="{{ url('/admin/investor/'.$investor['id'].'/delete') }}" method="POST">
							{!! csrf_field() !!}
							{{ method_field('DELETE') }}			
		    	            <button class="btn btn-red btn-xs btn-icon icon-left"><i class="entypo-cancel"></i>Delete Investor</button>
						</form>
					</div>

					<div class="clear"></div>

					<div class="col-sm-4">
						
					</div>

					<div class="col-sm-4">
						<i class="entypo-calendar"></i>
						DOB: <a href="#">{{ $investor['dob'] or 'Unavailable'}}</a>
					</div>

					<div class="col-sm-4">
						
					</div>
			</div>
			
		</div>
	</div>

@endforeach

@endsection

@section('scripts')
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons/font-awesome/css/font-awesome.min.css') }}">

	<!-- Imported scripts on this page -->
	<script src="{{ asset('assets/js/tocify/jquery.tocify.min.js') }}"></script>

@endsection