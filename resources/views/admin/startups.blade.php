@extends('admin.admin_area_layout')
@section('content')
<ol class="breadcrumb bc-3 hidden-print" >
    <li>
        <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
    </li>
    <li>
        <a href="{{url('/admin/startups')}}">Management</a>
    </li>
    <li class="active">
        <strong>Startups</strong>
    </li>
</ol>

<div class="row">
    <div class="col-md-3 col-sm-5">
        <form action="{{ url('/admin/startup/search') }}" method="get" role="form" class="search-form-full">
            <div class="form-group">
                <input type="text" class="form-control" name="search" id="search-input" placeholder="Search..." />
                <i class="entypo-search"></i>
            </div>

        </form>

    </div>
    <div class="col-md-6"></div>
    <div class="col-md-3">
        <a class="btn btn-blue btn-icon icon-left" href="{{ url('/admin/startup/create') }}"><i class="entypo-plus"></i>Create new startup</a>
    </div>
</div>

@if(isset($query))
<div class="row">
    <div class="col-md-3 col-sm-5"><h3>{{ $total }} result(s) for <strong>{{ $query }}</strong></h3></div>
    <div class="col-md-6"></div>
</div>
@endif

<!-- Member Entries -->
@foreach ($startups as $startup)
<div class="member-entry">
    <a href="#" class="member-img">
        <img src="{!! $startup['logo_url'] ? asset($startup['logo_url']) : asset('img/techpoint.png') !!}" class="img-rounded" alt="Logo" />
        <i class="entypo-forward"></i>
    </a>

    <div class="member-details">
        <h4>
            <a href="#">{{ $startup['startup_name'] }}</a>
            @if($startup['confirmation_status'])
                <i style="color:green" class="fa fa-spin fa-star"></i>
            @else
                <i class="fa fa-spin fa-star-o"></i>
            @endif
        </h4>

        <!-- Details with Icons -->
        <div class="row info-list">
                
            <div class="col-sm-3">
                <i class="entypo-book"></i>
                Founder <a href="#">{{ $users->find($startup['user_id'])['first_name'] }} {{ $users->find($startup['user_id'])['last_name'] }}</a>
            </div>

            <div class="col-sm-3">
                <i class="entypo-suitcase"></i>
                Industry <a href="#">{{ $industries->find([$startup['industry_id']])['industry_name'] or 'Unknown' }}</a>
            </div>

            <div class="col-sm-3">
            </div>
            
            <div class="col-sm-3">
                <a class="btn btn-xs btn-blue btn-icon icon-left" href="#"><i class="entypo-info"></i>View More</a>
            </div>

            <div class="clear"></div>

            <div class="col-sm-3">
            </div>

            <div class="col-sm-3">
                <i class="entypo-place"></i>
                Address <a href="#">{{ $startup['address'] }}</a>
            </div>

            <div class="col-sm-3">
            </div>
            
            <div class="col-sm-2">
                <a class="btn btn-xs btn-green btn-icon icon-left" href="{{ url('/admin/startup/'.$startup['id']).'/edit' }}"><i class="entypo-pencil"></i>Update details</a>
            </div>

            <div class="clear"></div>

            <div class="col-sm-3">
            </div>
            
            <div class="col-sm-3">
            </div>

            <div class="col-sm-3">
            </div>

            <div class="col-sm-3">
                <form action="{{ url('/admin/startup/'.$startup['id'].'/delete') }}" method="POST">
                    {!! csrf_field() !!}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-xs btn-red btn-icon icon-left"><i class="entypo-cancel"></i>Delete Startup</button>
                </form>
            </div>

        </div>
    </div>

</div>

@endforeach

@endsection

@section('scripts')
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-icons/font-awesome/css/font-awesome.min.css') }}">

    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/tocify/jquery.tocify.min.js') }}"></script>

@endsection