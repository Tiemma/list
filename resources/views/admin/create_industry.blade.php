@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{ url('/admin/industries') }}">Industries</a>
        </li>
        <li class="active">
            <strong>New industry</strong>
        </li>
    </ol>


    <div class="row">
            <div class="col-md-12">
                
                <div class="panel panel-primary" data-collapsed="0">
                
                    <div class="panel-heading">
                        <div class="panel-title">
                            industry Details
                        </div>
                    </div>
                    
                    <div class="panel-body">
                                
                        <form action="{{ url('admin/industry/create') }}" method="post" role="form" enctype="multipart/form-data" class="form-horizontal form-groups validate">
                            
                            <div class="form-group">
                                <label for="industry_name" class="col-sm-3 control-label">Industry Name</label>
                                
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="industry_name" name="industry_name" data-validate="required" placeholder="Industry Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="industry_profile" class="col-sm-3 control-label">Industry Profile</label>
                                
                                <div class="col-sm-5">
                                    <textarea class="form-control autogrow" id="industry_profile" name="industry_profile" data-validate="required" placeholder="Brief description"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Trending</label>
                                
                                <div class="col-sm-5">
                                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                                        <input type="checkbox" name="trending"/>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Create</button>
                                    <button type="reset" class="btn">Reset</button>
                                </div>
                            </div>
                        
                        </form>
                        
                    </div>
                
                </div>
            
            </div>
        </div>

    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>

@endsection

@section('scripts')

@endsection