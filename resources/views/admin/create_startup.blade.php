@extends('admin.admin_area_layout')
@section('content')
    <ol class="breadcrumb bc-3 hidden-print" >
        <li>
            <a href="{{url('/admin/home')}}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li>
            <a href="{{url('/admin/startups')}}">Startups</a>
        </li>
        <li class="active">
            <strong>Create new startup</strong>
        </li>
    </ol>
    
    <form action="{{ url('admin/startup/create') }}" id="form" method="post" enctype="multipart/form-data" class="form-groups form-horizontal validate">
        {!! csrf_field() !!}

        <div class="tab-content">

            <h4>Startup details</h4>
            <hr />
            <div class="form-group">
                <label for="startup_name" class="col-sm-3 control-label">Startup Name</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="startup_name" name="startup_name" data-validate="required">
                </div>
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="address" name="address" data-validate="required">
                </div>
            </div>

            <div class="form-group">
                    <label class="col-sm-3 control-label">Brief Profile</label>

                <div class="col-sm-5">
                    <textarea name="brief_profile" id="brief_profile" rows="4" cols="70"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Founder</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        <select name="user_id" id="user_id" class="select2" data-allow-clear="true" data-placeholder="Select a user...">
                            <option></option>
                            <optgroup label="Users" value selected>
                                @foreach($users as $user)
                                    <option value={{ $user['id'] }}>{{ $user['first_name'] }} {{ $user['last_name'] }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>

           <div class="form-group">
                <label class="col-sm-3 control-label">CAC Registered</label>
                
                <div class="col-sm-5">
                    <div class="bs-example">            
                        <div class="make-switch" data-on="success" data-off="warning">
                            <input type="checkbox" name="cac_registered" checked>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">CAC Registration Date</label>
                
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <a href="#"><i class="entypo-calendar"></i></a>
                        </div>
                        
                        <input type="text" name="cac_date" class="form-control datepicker" data-mask="date" data-format="dd/mm/yyyy">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Industry</label>
                
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-suitcase"></i>
                        </div>
                        <select name="industry_id" id="industry_id" class="form-control">
                            <option value selected>Select Industry</option>
                            @foreach($industries as $industry)
                                <option value="{{ $industry['id'] }}">{{ $industry['industry_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Trending</label>
                
                <div class="col-sm-5">
                    <div class="bs-example">            
                        <div class="make-switch" data-on="success" data-off="warning">
                            <input type="checkbox" name="trending" checked>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="form-group">
                <label class=" control-label col-sm-3">Confirm Status</label>
                
                <div class="col-sm-5">
                    <div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
                        <input type="checkbox" name="status"/>
                    </div>
                </div>
            </div>
{{--
            <div class="form-group">
                <label class="col-sm-3 control-label">Logo Upload</label>
                
                <div class="col-sm-5">
                    
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                            <img src="{{ asset('img/techpoint.png') }}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                        <div>
                            <span class="btn btn-white btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="logo_url" accept="image/*">
                            </span>
                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                    
                </div>
            </div>--}}

            <h4>Investment</h4>
            <hr />

            <div class="form-group">
                <label class="control-label col-sm-3">Investor</label>

                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>

                        <select name="investor_id" id="user_id" class="select2" data-allow-clear="true" data-placeholder="Select an investor...">
                            <option></option>
                            <optgroup label="Users" value selected>
                                @foreach($investors as $investor)
                                    <option value={{ $investor['id'] }}>{{ $investor['name'] }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="amount" class="col-sm-3 control-label">Investment amount</label>
                
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="amount" name="amount" data-validate="required">
                </div>
            </div>

            <br />
            <hr />
            <br/>
            
            <div class="col-sm-3"></div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
                <button type="reset" class="btn">Reset</button>
            </div>

        </div>
    </form>

    @endsection

@section('scripts')
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="{{ url('assets/js/daterangepicker/daterangepicker-bs3.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">

    <!-- Imported scripts on this page -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/daterangepicker/daterangepicker.js') }}"></script> 
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-tagsinput.min.js') }}"></script>

@endsection