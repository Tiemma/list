@extends('layouts.app')
@section('content')
<title>Techpoint | Login</title>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form name = 'login' class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <br/><br/>
                        <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <i class="prefix material-icons">email</i>
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                            <label for="email">E-Mail Address</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email')}}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <i class="prefix material-icons">vpn_key</i>
                                <input id="password" type="password" name="password" required>
                                <label for="password">Password</label>
                            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                        <div class="group switch">
                            <label>
                                Off
                                <input type="checkbox">
                                <span class="lever"></span>
                                Remember Me
                            </label>
                        </div>

                        <div class="group button">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
