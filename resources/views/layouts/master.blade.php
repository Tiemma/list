<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Techpointlist Login</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/materialize.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/techpointlist.css') }}">
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.3.min.js') }}"></script>
    <script>


        function showLogin() {
            $(document).ready(function () {
                $('#login-panel').fadeIn('slow');
                $('ul.tabs').tabs('select_tab', 'login');
            });
        }

        function showRegister() {
            $(document).ready(function () {
                $('#login-panel').fadeIn('slow');
                $('ul.tabs').tabs('select_tab', 'register');
            });
        }

    </script>
</head>

<body>
<header>
@section('header')
    <div id="search-page">
        <a href=""><i class="material-icons close-btn">clear</i></a>
        <div class="container">
            <div class="row">
                <div class="search-pack">

                </div>
            </div>
        </div>
    </div>
    <nav>
        <a href="#" data-activates="mobile-demo" class="button-collapse hide-on-med-and-up"><i class="material-icons">menu</i></a>
        <div style="margin-top: 10px"></div>
        <div class="container">
            <div class="row">
                <div class="col s12 m3">
                    <a href="{{asset('/')}}"><img src="{{asset('/images/techpointlogo.png')}}" class="master-logo"></a>
                    @if(Auth::guest())
                        <ul id="upper-nav" class="right hide-on-med-and-up">
                            <li><a class="nav-login" href="#">Login</a></li>
                            <li><a class="nav-register" href="#">Register</a></li>
                        </ul>
                    @else
                        <span class="hide-on-med-and-up right">
                            <ul id="upper-nav" class="right">
                                <a href="#" class='dropdown-button' active data-activates='dropdown2'
                                   data-constrainwidth="false" data-beloworigin="true">
                            <span class="master-pic">
                                 <span class="name right">&nbsp;&nbsp;{{ Auth::user()->first_name }}</span>
                                @if(isset(Auth::user()->profile_pics_url))
                                    @if(strpos(Auth::user()->profile_pics_url, 'http'))
                                        <img src="{{Auth::user()->profile_pics_url}} ?>" class="master"/>
                                    @else
                                        <img src="{{asset(Auth::user()->profile_pics_url)}}" class="master"/>
                                    @endif
                                @else
                                    <img src="{{asset('/images/techpointlogo.png')}}" class="master"/>
                                @endif
                            </span>
                                </a>
                                <ul id='dropdown2' class='dropdown-content' style="width: 200px">
                                     <li><a href="{{ url('/user/'.Auth::user()->id) }}">Profile</a></li>
                            <li><a href="{{ url('/startupselect') }}">Startups</a></li>
                            <li><a href="{{ url('/registerstartup') }}">Register Startup</a></li>
                            <li><a href="{{ url('/logout') }}">Logout</a></li>
                                </ul>
                            </ul>
                            @endif
                    </span>
                </div>


                <div class="col m6">
                    <img class="responsive-img ad-image" src="{{asset('images/ads.png')}}"/>
                </div>


                @if(Auth::guest())
                    <ul id="upper-nav" class="right hide-on-small-only">
                        <li><a class="nav-login" href="#">Login</a></li>
                        <li><a class="nav-register" href="#">Register</a></li>
                    </ul>
                @else
                    <ul id="upper-nav" class="right hide-on-small-only">
                        <a href="#" class='dropdown-button' active data-activates='dropdown1'
                           data-constrainwidth="false" data-beloworigin="true">
                            <span class="master-pic">
                                <span class="name right">&nbsp;&nbsp;{{ Auth::user()->first_name }}</span>
                                @if(isset(Auth::user()->profile_pics_url))
                                    @if(strpos(Auth::user()->profile_pics_url, 'http'))
                                        <img src="{{Auth::user()->profile_pics_url}} ?>" class="master"/>
                                    @else
                                        <img src="{{asset(Auth::user()->profile_pics_url)}}" class="master"/>
                                    @endif
                                @else
                                    <img src="{{asset('/images/techpointlogo.png')}}" class="master border"/>
                                @endif
                            </span>
                        </a>

                        <ul id='dropdown1' class='dropdown-content' style="width: 200px">
                            <li><a href="{{ url('/user/'.Auth::user()->id) }}">Profile</a></li>
                            <li><a href="{{ url('/startupselect') }}">Startups</a></li>
                            <li><a href="{{ url('/registerstartup') }}">Register Startup</a></li>
                            <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                    </ul>
                @endif
            </div>
            <div class="divider"></div>
            <ul id="low-nav" class="hide-on-small-only">
                <li><a href="{{asset('/home')}}">TRENDING</a></li>
                <li><a href="{{asset('/companies')}}">BUSINESSES</a></li>
                <li><a href="coming">INVESTORS</a></li>
                <li><a href="{{asset('/schools')}}">SCHOOLS</a></li>
                <li><a href="{{asset('/events')}}">EVENTS</a></li>
                <li><a href="{{asset('/people')}}">PERSONALITIES</a></li>
            </ul>
        </div>

        <ul class="side-nav" id="mobile-demo">
            <li>
                <a href="{{asset('/')}}">
                    <img src="{{asset('/images/techpointlogo.png')}}" style="max-width: 70%"/>
                </a>
            </li>
            <li><a href="{{asset('/home')}}">TRENDING</a></li>
            <li><a href="{{asset('/companies')}}">BUSINESSES</a></li>
            <li><a href="{{asset('/coming')}}">INVESTORS</a></li>
            <li><a href="{{asset('/schools')}}">SCHOOLS</a></li>
            <li><a href="{{asset('/events')}}">EVENTS</a></li>
            <li><a href="{{asset('/people')}}">PERSONALITIES</a></li>
        </ul>
    </nav>


@show

</header>
<section id="login-panel">
    <div class="container login-box">
        <a href=""><i class="material-icons" id="login-close">clear</i></a>
        <a href="#"><img class="login-logo" src="{{ asset('images/techpointlogo.png') }}"></a>
        <div class="row">
            <div class="col s12 m6 push-m3 login-panel">
                <ul class="tabs">
                    <li class="tab col s6 m6"><a class="active" href="#login">Login</a></li>
                    <li class="tab col s6 m6 "><a href="#register"> Register</a></li>
                </ul>
                <div class="divider"></div>
                <!-- Login Form -->
                <div id="login" class="col s12 m12">
                    <div class="row">
                        <span><p class="center-align">Login With</p></span>
                    </div>
                    <div class="row">
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn" href="facebook">Facebook</a>
                        </div>
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn" href="twitter">Twitter</a>
                        </div>
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn" href="linkedin">Linkedin</a>
                        </div>
                    </div>
                    <div class="col s12 m12">
                        <h3 class="center-align"><strong>OR</strong></h3>
                    </div>
                    <div class="row">
                        <form name = 'login' class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            @if($errors->first('email'))
                                <script>
                                    showLogin();
                                </script>
                                <div class="row">
                                    <span>
                                        <p class="red-text center-align">Your password or email is incorrect</p>
                                    </span>
                                </div>
                            @endif
                            <div class="row">
                                <div class="input-field col s10 m10 push-s1 push-m1">
                                    <input id="email" name="email" type="email" required class="validate">
                                    <label class="blue-text" for="email">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s10 m10 push-s1 push-m1">
                                    <input id="password" name="password" type="password" required class="validate">
                                    <label class="blue-text" for="password">Password</label>
                                    <a class='right' href="{{url('/reset')}}" title='Reset your password'>Forgot your password?</a>
                                    <div style="margin: 3%"></div>
                                </div>
                            </div>

                            <button type="submit" class="btn-large btn-round waves-effect waves-light blue login-button">Login</button>
                        </form>

                    </div>


                </div>
                <!-- Register Form -->
                <div id="register" class="col s12 m12">
                    <form class="col s12" method="post" action="{{ url('/register') }}" >
                        {{ csrf_field() }}
                        @if($errors->first('password'))
                            <script>
                                showRegister();
                            </script>
                            <div class="row">
                                <span>
                                    <p class="red-text center-align">{{$errors->first('password')}}</p>
                                </span>
                            </div>
                        @endif
                        <div  class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="first_name" name="first_name" value="" type="text" required class="validate">
                                <label class="blue-text" for="first_name">First Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="last_name" name="last_name" value="" type="text" required class="validate">
                                <label class="blue-text" for="first_name">Last Name</label>
                            </div>
                        </div>
                        {{--@if($errors->first('email'))--}}
                        {{--<script>--}}
                        {{--showRegister();--}}
                        {{--</script>--}}
                        {{--<div class="row">--}}
                        {{--<span>--}}
                        {{--<p class="red-text center-align">{{$errors->first('email')}}</p>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="reg_email" name="email" type="email" class="validate">
                                <label class="blue-text" for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="reg_password" name="password" type="password" required class="validate">
                                <label class="blue-text" for="password">Password</label>
                            </div>
                        </div>
                        @if($errors->first('password_confirmation'))
                            {{--<script>--}}
                            {{--showRegister();--}}
                            {{--</script>--}}
                            {{--<div class="row">--}}
                            {{--<span>--}}
                            {{--<p class="red-text center-align">{{$errors->first('password_confirmation')}}</p>--}}
                            {{--</span>--}}
                            {{--</div>--}}
                        @endif
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="password_confirmation" name="password_confirmation" required type="password" class="validate">
                                <label class="blue-text" for="password_confirm">Confirm Password</label>
                            </div>
                        </div>
                        <button type="submit" class="btn-large btn-round waves-effect waves-light blue login-button">Register</button>
                    </form>
                </div>
            </div>
        </div>

        <div class=""></div>

    </div>

</section>

    @yield('content')

<div class="col m2 s2">
    <div class="ads-content"></div>
</div>

<div class="widget-container">
{{--
    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
--}}
</div>



 <footer class="page-footer">
         <div class="row">
             <div class="footer-copyright col s10 push-s1">
                 <div class="col s4">© 2017 Copyright</div>
                 <div class="col s4 push-s4">Techpoint List</div>
                 <a class="grey-text text-lighten-4 right" href="#!">
                 </a>
             </div>
         </div>
     </div>
    </footer>
</body>


<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('/js/footer.js')}}"></script>
<script type="text/javascript" src="{{ asset('/js/materialize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/techpointlist.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/events.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/functions.js') }}"></script>
<script type="text/javascript" src="{{asset('/js/search.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/rate.js')}}"></script>

{{--
<div id="fb-root"></div>
--}}

{{--<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>--}}

<script>
    function navRegister() {
        $('.nav-register').on('click', function (event) {
            event.preventDefault();
            $('#login-panel').fadeIn('slow');
            $('ul.tabs').tabs('select_tab', 'register');
        });
    }

    function navLogin() {
        $('.nav-login').on('click', function (event) {
            event.preventDefault();
            $('#login-panel').fadeIn('slow');
            $('ul.tabs').tabs('select_tab', 'login');
        });
    }

    $(document).ready(function () {
        $(".button-collapse").sideNav();

    });
</script>
<style>
    .dropdown-content li {
        padding-top: 0px;
    //min-height: 23 px;
    }

    nav .container .dropdown-content a {
        transition: background-color .3s;
        font-size: 1rem;
        color: #000000;
        display: block;
        padding: 5px 15px;
        cursor: pointer;
        height: 30px;
    }

    #upper-nav .name {
        position: relative;
    }

    footer .row{
        margin-bottom: 0px !important;
    }
</style>


</html>
