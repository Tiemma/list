<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Techpointlist Login</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/materialize.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/techpointlist.css') }}">
    <style>
        .overlay {
            height: 0%;
            width: 100%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: rgb(0, 0, 0);
            background-color: rgba(255, 255, 255, 0.9);
            overflow-y: scroll;
            transition: 0.5s;
        }

        .overlay .closebtn {
            position: absolute;
            top: 20px;
            right: 45px;
            font-size: 60px;
            color: rgb(0, 0, 0);
        }

        @media screen and (max-height: 450px) {
          .overlay {overflow-y: auto;}
          .overlay .closebtn {
            font-size: 40px;
            top: 15px;
            right: 35px;
          }
        }
    </style>

{{--This is to open the tab and the second allows for the switch--}}
<script type="text/javascript" src="{{ asset('/js/jquery-2.1.3.min.js') }}"></script>
    <script>
      function openNav() {
        document.getElementById("myNav").style.height = "100%";
    }


    $(document).ready(function(){
        $('ul.tabs').tabs();
    });
    </script>


</head>
<body id="app-layout">
    <nav>
    {{--<a href="#" data-activates="slide-out" class="button-collapse hide-on-med-and-up"><i class="material-icons">menu</i></a>--}}
    <div class="container">
            <div class="row">
                <div class="col s12 m3 "><a href="{{ url('/') }}"><img src="{{ asset('/images/techpoint.png') }}" style="max-width: 80%"></a></div>
                <div class="col s12 m6">
                    <form action="" method="get">
                        {{ csrf_field() }}
                        <div class="type" style="display: none">
                            All
                        </div>
                        <input type="search" id="search" autocomplete="off" onclick="$('.wrapper').slideDown();">
                        <script>
                            $( ".wrapper" ).blur(function() {
                                $('.wrapper').slideUp();
                            });
                        </script>
                        <style>
                            .wrapper{
                                position: absolute;
                                width: 40%;
                                color: black;
                                background-color: white;
                                border: 1px  solid steelblue;
                                z-index: 10000;
                            }

                            .collapsible{
                                box-shadow: 0 0px 0px 0 rgba(0,0,0,0.16),0 0px 0px 0 rgba(0,0,0,0.12);
                                border-top: 0px;
                            }

                            .collapsible-body {
                                border-bottom: 0px;
                            }

                            .dropdown-content li{
                                min-height: 30px;
                            }
                        </style>

                        <div class="wrapper">
                            <ul class="collapsible" data-collapsible="accordion" style="background-color: white;">
                                <li style="background-color: white;color: black;">
                                    <div class="collapsible-header active" style="color:black;border: 1px solid transparent; background-color: white;width: 100%;font-weight: bolder"><span class="blue-text"><i class="material-icons">build</i>Options</span></div>
                                    <div class="collapsible-body" style="background-color: white;color: black;">
                                        <p style="padding-top: 10px;padding-bottom: 10px;" ><span class="blue-text" ></span>
                                        </p>
                                        <p style="padding-top: 10px;padding-bottom: 10px;"><span class="blue-text">Search by Type</span>
                                            <select id='select_type' class="browser-default"  onchange="change()">
                                                <option selected>
                                                    All
                                                </option>
                                                <option>
                                                    User
                                                </option>
                                                <option>
                                                    Investor
                                                </option>
                                                <option>
                                                    Company
                                                </option>
                                                <option>
                                                    Startup
                                                </option>
                                                <option>
                                                    Investor
                                                </option>
                                            </select>
                                        </p>

                                        <p style="padding-top: 10px;padding-bottom: 10px;"><span class="blue-text">Search by Location</span>
                                            <select class="browser-default">
                                                <option selected>
                                                    Nigeria
                                                </option>
                                                <option>
                                                    Other
                                                </option>
                                            </select>
                                        </p>

                                        <p style="padding-top: 10px;padding-bottom: 10px;" ><span class="blue-text" >Search by Category</span>
                                            <select class="browser-default">
                                                <option>
                                                    Name
                                                </option>
                                                <option>
                                                    Class
                                                </option>
                                            </select>
                                        </p>

                                    </div>
                                </li>
                            </ul>
                            <div class="collection" id="result" style="float: right;color: black; border-color: transparent;width: 60%;">
                                <span style="color: black; border-color: transparent;"><h2>No results</h2></span>
                            </div>
                        </div>


                    </form>
                </div>
                @if(Auth::guest())
                <ul id="upper-nav" class="right">
                    <li><a onclick="openNav()">Login</a></li>
                    <li><a onclick="openNav();$(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'register');
  });
      ">Register</a></li>
                </ul>
                @else
                    <ul class="upper-nav" class="right" style="left: 5%; position: relative">
                        <li><a href=#>Profile pic</a></li>
                    </ul>
                    <ul id="upper-nav" style="width: 12%" class="right">
                        <a class='dropdown-button' active href="#" data-activates='dropdown1' data-beloworigin="true">{{ Auth::user()->first_name }}</a>
                                                <ul id='dropdown1' class='dropdown-content'>
                                                        <li><a href="{{ url('/update') }}">Update profile</a></li>
                                                    <li><a href="{{ url('/logout') }}">Logout</a></li>
                                                 </ul>
                    </ul>
                @endif
            </div>
            <div class="divider"></div>
            <ul id="low-nav" class="">
                <li><a href="#">TRENDING</a></li>
                <li><a href="#">COMPANIES</a></li>
                <li><a href="#">INDUSTRIES</a></li>
                <li><a href="#">SCHOOLS</a></li>
                <li><a href="#">INNOVATIONS</a></li>
                <li><a href="#">EVENTS</a></li>
                <li><a href="#">ENTREPRENEURS</a></li>
            </ul>
    </div>
</nav>
    @yield('content')

<!-- The overlay -->
<div id="myNav" class="overlay">

  <!-- Button to close the overlay navigation -->
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

  <!-- Overlay content -->
  <div class="">
    <div class="container login-box">
            <a href="#"><img class="login-logo" src="{{ asset('img/techpoint.png') }}"></a>
        <div class="row">
            <div class="col s12 m6 push-m3 login-panel">
                <ul class="tabs">
                    <li class="tab col s6 m6"><a class="active" href="#login">Login</a></li>
                    <li class="tab col s6 m6 "><a href="#register"> Register</a></li>
                </ul>
                <div class="divider"></div>

                <!-- Login Form -->
                <div id="login" class="col s12 m12">
                    <div class="row">
                        <h5 class="center-align">Login With</h5>
                    </div>
                    <div class="row">
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn">Facebook</a>
                        </div>
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn">Twitter</a>
                        </div>
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn">Linkedin</a>
                        </div>
                    </div>
                    <div class="col s12 m12">
                        <h3 class="center-align"><strong>OR</strong></h3>
                    </div>
                    <div class="row">
                        <form name = 'login' class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                            <div class="row col s12" >
                                <div class="input-field col s6 m10 push-s3 push-m1">
                                    <input id="email" name="email" type="email" class="validate">
                                    <label class="blue-text" for="email">Email</label>
                                </div>
                            </div>
                            @if($errors->first('email'))
                                    <script>
                                    openNav()
                                     $(document).ready(function(){
                                        $('ul.tabs').tabs('select_tab', 'login');
                                      });
                                    </script>
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('email')}}</div>
                                @endif

                            <div class="row col s12">
                                <div class="input-field col s10  push-s3 push-m1">
                                    <input id="password" name="password" type="password" class="validate">
                                    <label class="blue-text" for="password">Password</label>
                                </div>
                            </div>
                            @if($errors->first('password'))
                                    <script>
                                    openNav()
                                 $(document).ready(function(){
                                    $('ul.tabs').tabs('select_tab', 'login');
                                  });
                                    </script>
                                    <div class="card-panel red lighten-3 col s4 m10 push-s4 push-m1">{{$errors->first('password')}}</div>
                            @endif

                           <div class="row">
                            <div class="col s4 push-s4" style="width: 100%;left: -3%">
                            <button class="btn-large btn-round waves-effect waves-light blue login-button">Login</button>
                            </div>
                            </div>
                            
                        </form>

                    </div>
                </div>

                <!-- Register Form -->
                <div id="register" class="col s12 m12">
                    <div class="row">
                        <h5 class="center-align">Register Using</h5>
                    </div>
                    <div class="row">
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn">Facebook</a>
                        </div>
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn">Twitter</a>
                        </div>
                        <div class="col s4 m4 l4">
                            <a class="blue waves-effect waves-light btn btn-round social-btn">Linkedin</a>
                        </div>
                    </div>
                    <div class="col s12 m12">
                        <h3 class="center-align"><strong>OR</strong></h3>
                    </div>
                    <form class="col s12" method="post" action="{{ url('/register') }}" >
                        {{ csrf_field() }}
                        <div  class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="first_name" name="first_name" value="" type="text" class="validate">
                                <label class="blue-text" for="first_name">First Name</label>
                            </div>
                        </div>
                         @if($errors->first('first_name'))
                                    <script>
                                    openNav();
document.getElementById('register').style.display = 'block'; document.getElementById('login').style.display = 'none';
                                    </script>
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('first_name')}}</div>
                            @endif
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="last_name" name="last_name" value="" type="text" class="validate">
                                <label class="blue-text" for="first_name">Last Name</label>
                            </div>
                        </div>
                         @if($errors->first('last_name'))

<script>
                                    openNav();
 $(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'register');
  });
                                    </script>                 <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('last_name')}}</div>
                            @endif
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="reg_email" name="email" type="email" class="validate">
                                <label class="blue-text" for="email">Email</label>
                            </div>
                        </div>
                         @if($errors->first('email'))

<script>
                                    openNav();
 $(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'register');
  });
                                    </script>                                   <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('email')}}</div>
                            @endif
                        <div class="row">
                            <div class="input-field col s10 m10 push-s1 push-m1">
                                <input id="reg_password" name="password" type="password" class="validate">
                                <label class="blue-text" for="password">Password</label>
                            </div>
                        </div>
                         @if($errors->first('password'))

<script>
                                    openNav();
 $(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'register');
  });
                                    </script>                 <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('password')}}</div>
                            @endif
                            <div class="row">
                                <div class="input-field col s10 m10 push-s1 push-m1">
                                    <input id="password_confirmation" name="password_confirmation" type="password" class="validate">
                                    <label class="blue-text" for="password_confirm">Confirm Password</label>
                                </div>
                            </div>
                             @if($errors->first('password_confirmation'))

<script>
                                    openNav();
 $(document).ready(function(){
    $('ul.tabs').tabs('select_tab', 'register');
  });
                                    </script>                 <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('password_confirmation')}}</div>
                            @endif
                        <div class="row">
                            <div class="col s4 push-s4" style="width: 100%;left: -4%">
                            <button class="btn-large btn-round waves-effect waves-light blue login-button">Register</button>
                            </div>
                            </div>
                    </form>
            </div>
        </div>
    </div>
      </div>
      </div>
</div>
<script type="text/javascript" src="{{ asset('/js/materialize.min.js') }}"></script>
<script  type="text/javascript" src="{{ asset('/js/techpointlist.js') }}"></script>
<script  type="text/javascript" src="{{ asset('/js/events.js') }}"></script>
<script  type="text/javascript" src="{{ asset('/js/search.js') }}"></script>
<script type="text/javascript">

    $('.button-collapse').sideNav({
                menuWidth: 300, // Default is 240
                edge: 'left', // Choose the horizontal origin
                closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
            }
    );


  function change() {
      $('.type').html($('#select_type').find(':selected').text());
      var type='type='+$('#select_type').find(':selected').text().trim();
      document.cookie=type;
      $('#result').fadeOut('slow');
      fetch();
      $('#result').fadeIn('400');

  }

    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }

    document.cookie='type=All';

</script>
</body>
</html>
