@extends('layouts.master')

@section('content')
<section id="profile-header">
    <div style="height: 75vh">
        <div class="profile-header">
            <div class="black-layer">
                <div class="row">
                    <div class="col s12 m3">
                        <div class="pic-container">
                            <?php $img_source = "" ?>
                            @if($user->profile_pics_url == NULL)
                                <?php $img_source = asset('/images/techpoint-square.png'); ?>
                            @else
                                <?php $img_source = asset($user->profile_pics_url); ?>
                            @endif
                            <img src="{{asset($img_source)}}" class="responsive-img p-pic circle" style="height: 100%">
                        </div>
                    </div>
                    <div class="col s12 m6">
                        <div class="profile-title">
                            <h3 class="center-align white-text">{{$user->first_name." ".$user->last_name}}</h3>
                            @if(count($experience) > 0)
                                @foreach($experience as $work_experience)
                                    @if($work_experience->employed_status == 1)
                                        <h3 class="center-align white-text">{{$work_experience->role}} at {{$work_experience->company_name}}</h3>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="col s12 m3">
                        @if($user->id == Auth::user()->id)
                            <a href="{{url('/update')}}"><button class="btn btn-large waves-effect grey grey-text lighten-3 hide-on-small-only"> Edit </button></a>
                            <a href="{{url('/update')}}"><button class="btn waves-effect grey grey-text lighten-3 black-text hide-on-med-and-up"> Edit </button></a>
                        @endif
                        <div class="row social-icons">
                            <?php
                            $social_channels = explode('|', $user->social_channel);
                            ?>
                            @for($i = 0; $i < count($social_channels);++$i)
                                <?php
                                $handleAndMedia = explode('*', $social_channels[$i]);
                                ?>
                                @if($handleAndMedia[0] != NULL)
                                    @if($handleAndMedia[1] == "Facebook")
                                        <div class="col s4 m4">
                                            <a target="_blank" href="http://facebook.com/{{$handleAndMedia[0]}}">
                                                <img src="{{asset('images/facebook-logo.png')}}" alt="facebook icon">
                                            </a>
                                        </div>
                                    @elseif($handleAndMedia[1] == "Twitter")
                                        <div class="col s4 m4">
                                            <a target="_blank" href="http://twitter.com/{{$handleAndMedia[0]}}">
                                                <img src="{{asset('images/twitter-social.png')}}" alt="twitter icon">
                                            </a>
                                        </div>
                                    @elseif($handleAndMedia[1] == "LinkedIn")
                                        <div class="col s4 m4">
                                            <a target="_blank" href="http://linkedin.com/in/{{$handleAndMedia[0]}}">
                                                <img src="{{asset('images/linkedin-logo.png')}}" alt="linkedin icon">
                                            </a>
                                        </div>

                                    @endif
                                @endif
                            @endfor
                        </div>
                    </div>
                </div>
                <div class="row">
                </div>
            </div>

        </div>
    </div>
</section>
<!--- Profile Header Ends -->
<section id="About">

    <div class="container">
        <div class="bordered">
            <div>
                <h2 class="blue-text">About</h2>
            </div>
            <div class="divider"></div>
            <div class="row">
                <p>
                    @if (Auth::user()->brief_profile != NULL)
                        {{Auth::user()->brief_profile}}
                    @else
                        <p>No description found</p>
                    @endif
                </p>
            </div>
        </div>
    </div>
    </div>
</section>
<!--- End of overview -->

<section id="founded">
    <div class="container">
        <div class="bordered">
            <div>
                <h2 class="blue-text">Founded</h2>
            </div>
            <div class="divider"></div>
            @if(count($startup) > 0)
        @foreach($startup as $startups)
            <div class="row">
                <div class="col s3 m4">
                    <div class="ent-thumbnail">
                    @if(isset($startups->logo_url))
                        <img src="{{asset($startups->logo_url)}}" class="responsive-img">
                    @else
                        <img src="{{asset('/image/techpoint.jpg')}}" class="responsive-img">
                    @endif
                    </div>
                </div>
                <div class="col s8 push-s1 m8">
                    <span><h3><strong>{{$startups->startup_name}}</strong></h3></span>
                    <div>
                        <p>{{$startups->startup_brief}}</p>
                    </div>
                </div>
            </div>
                <div class="divider"></div>
            @endforeach
                @else
                <p>No startups found</p>
                @endif
        </div>
    </div>
</section>

<section id="work-experience">
    <div class="container">
        <div class="bordered">
            <div>
                <h2 class="blue-text">Work Experience</h2>
            </div>
            <div class="divider"></div>
            @if(count($experience) > 0)
                @foreach($experience as $work_experience)
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="col s4"><h3>Company</h3></div>
                                <div class="col s8"><p>{{$work_experience->company_name}}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h3>Period</h3></div>
                                <div class="col s8">
                                    <p>
                                        <?php
                                        $start_date = explode(' ', $work_experience->start_date);
                                        $end_date = explode(' ', $work_experience->end_date);
                                        ?>
                                        @if($work_experience->employed_status == 1)
                                            {{$months[$start_date[0]].' '.$start_date[1]}} till date
                                        @else
                                            {{$months[$start_date[0]].' '.$start_date[1]}} till {{$months[$end_date[0]].' '.$end_date[1]}}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h3>Role</h3></div>
                                <div class="col s8"><p>{{$work_experience->role}}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h3>Work Description</h3></div>
                                <div class="col s8">
                                    <p>@if(count($work_experience->description) > 0)
                                    {{$work_experience->description}}
                                    @else
                                    No decription available
                                    @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col m3 hide-on-small-only">

                        </div>
                    </div>
                @endforeach
            @else
                <div class="divider"></div>
                <p>No work experience found</p>
            @endif
        </div>
    </div>
</section>
@if(isset($skills))
<section id="skills">
    <div class="container">
        <div class="bordered">
            <div>
                <h2 class="blue-text">Skills</h2>
            </div>
            <div class="divider"></div>
            <div class="row">
                <div class="span-wrapper">

                        <?php $skill = explode('|', $skills->name) ?>
                        @foreach($skill as $skills)
                                <span class="col s3 m2" style="padding-bottom: 20px;">{{$skills}}</span>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection