@extends('layouts.master')

@section('content')
    <form method="POST" action="{{asset('reviews')}}">
        <section id="review">
            <div class="container">
                <div class="bordered">
                    <div class="row">
                        <h2 class="blue-text">Write a review</h2>
                        <div class="divider"></div>
                        <div class="divider"></div>
                        <div class="col s8 push-s2">
                            <div style="margin: 1.5%"></div>
                            <textarea required name="comment" class="review-post" rows="10" cols="30"></textarea>
                            <input type="hidden" name="id" value="{{$id}}"/>
                            @if($errors->first('comment'))
                                <li class="red-text">This field is required</li>
                            @endif
                            <button class="btn btn-success right">Post</button>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </section>
    </form>

    <section id="review">
        <div class="container">
            <div class="bordered">
                <div class="row">
                    <h2 class="blue-text">Reviews</h2>
                    <div class="divider"></div>
                </div>

                <?php $i = 0; ?>
                @foreach($ratings as $rating)
                    <div class="divider"></div>
                    <div class="divider"></div>
                    <div class="row">
                        <?php
                        $users = \App\User::where('id', $rating_user[$i])->get(array('first_name', 'last_name', 'profile_pics_url'))
                        ?>
                        @foreach($users as $user)
                            <div class="col s2">
                                <img class="responsive-images review-pic circle" src="{{asset($user->profile_pics_url)}}"/>
                            </div>
                            <div class="col s9 review-header">
                                <div class="reviewer blue-text col s5">{{$user->first_name}} {{$user->last_name}}</div>
                                <div class="reviewer-company col s4 grey-text"> - CEO, HealthFacts NG</div>
                            </div>
                        @endforeach
                        <div class="col s9 review-content">
                            @if(strlen($rating->comment) > 400)
                                <?php
                                $comment = explode(' ', substr($rating->comment, 0, 400));
                                $comment[count($comment) - 1] = NULL;
                                $comment = implode(' ', $comment);
                                ?>
                                {{$comment}}<a href="#" class="right col s3"><span class="col s9 push-s2" onclick="$(this).parents('div').next('.review-content').html('Div')">...Read more</span></a>
                            @else
                                {{$rating->comment}}
                            @endif
                        </div>
                    </div>
                    <?php ++$i ?>
                @endforeach
            </div>
        </div>
    </section>
@endsection