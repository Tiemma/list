@extends('layouts.master')

@section('content')
    <div class="container events-container">
        <div class="row">
            <div class="col s12 m6 push-m3 search-container">
                <br />
                <input id="search-box" placeholder="Search Events" name="search" class="black-text search-box center" type="search">
            </div>
        </div>
        <div style="margin-top: 2.5%"></div>
        <div class="row add-more">
            <?php $i = 0 ?>
            @foreach($events as $event)
                @if($i%3 == 0 and $i != 0)
                    <div class="col s12 margin-top"></div>
                @else
                    <div class="col s12 hide-on-med-and-up margin-top"></div>
                @endif
                <div class="group">
                    <div class="col s3 grey-background no-padding">
                        <div class="col s12 no-padding">
                            <a href="{{asset("/event/$event->id")}}">
                                <img src="{{asset($event->logo_url)}}" class="event-icon z-depth-1" />
                                <div style="margin: 2%"></div>
                                <div class="col s8">
                                    <h2 style="font-weight: bolder">{{$event->name}}</h2>
                                </div>
                            </a>
                            <div class="col s4 icons-small ">
                                <img src="{{asset('/images/facebook-logo.png')}}" class="event-social-icons" />
                                <img src="{{asset('/images/twitter-social.png')}}" class="event-social-icons" />
                            </div>
                            <div class="col s9">
                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s3">
                                            <img src="{{asset('/images/location.png')}}" class="events-icon"/>
                                        </div>
                                        <div class="col s9"><h5>{{$event->address}}</h5></div>
                                    </div>
                                </div>
                                <div style="margin: 5%"></div>
                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s3">
                                            <img src="{{asset('/images/calendar.png')}}" class="events-icon"/>
                                        </div>
                                        <div class="col s3 hide-on-small-only">
                                            <h5>{{$event->event_date}}</h5>
                                        </div>
                                        <div class="col s6 hide-on-small-only">
                                            <a href="{{asset("/event/$event->id")}}">
                                                <button type="button" class="btn right register-button">Read More</button>
                                            </a>
                                        </div>
                                        <div class="col s9 hide-on-med-and-up"><h5>14th, January 2014.</h5></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 hide-on-med-and-up no-padding">
                            <a href="{{asset("/event/$event->id")}}">
                                <button type="button" class="btn right register-button">Read More</button>
                            </a>
                        </div>
                    </div>
                    <div class="col s1"></div>
                    <?php ++$i ?>
                </div>
            @endforeach
        </div>
    </div>

   {{-- @if(count(\App\Event::all()) > 15)
        <div class="row">
            <div class="col s4 push-s4">
                <button type="button" class="show-more btn btn-large">Show More</button>
            </div>
        </div>
    @endif--}}
    <div class="col s12 loader-container none" id="loader">
        <div class="loader">
            <div class="stacked-progress-bars">
                <div class="progress-bar"></div>
                <div class="progress-bar"></div>
                <div class="progress-bar"></div>
            </div>
        </div>
    </div>

    <input type="hidden" class="noOfEvents" value="{{count(\App\Event::all())}}" />


    <style>
        .events-icon{
            max-width: 30px;
        }

        h5{
            margin-top: 0%;
        }

        h2{
            margin-top: 0%;
        }

        .margin-top{
            margin-top: 2.5%;
        }

        .register-button{
            width: 80px;
            padding: 0px;
            font-size: 0.8em;
            position: absolute;
            margin-left: 3%;
        }

        @media only screen and (max-width: 570px){
            .btn{
                padding: 0px;
                margin-right: calc(50% - 40px);
                width: 70%;
                font-size: 10px;
                margin-left: 0;
                margin-bottom: 10%;
            }

            .margin-top{
                margin-top: 25%;
            }


            .grey-background{
                width: 83% !important;
                margin-left: 8.5% !important;
            }
        }

        .show-more{
            //position: absolute;
            width: 100%;
        }

        .events-container{
            width: 90% !important;
            padding: 5% !important;
        }

    </style>

    <script>
        $(document).ready(function(){
            init();
        });
    </script>


@endsection