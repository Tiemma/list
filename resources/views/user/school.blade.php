@extends('layouts.master')

@section('content')
    <section id="profile-header">
        <!--<div style="height: 45vh">-->
        <div class="profile-header">
            <div class="black-layer">
                <div class="row">
                    <div class="col s12 m3"></div>
                    <div class="col s12 m6">
                        <div class="profile-title">
                            <h3 class="center-align white-text">{{$school->name}}</h3>
                        </div>
                    </div>
                    <div class="col s12 m3"></div>
                </div>
            </div>
        </div>
    </section>

    <div style="width: 90%; margin-left: 5%">
        <div class="row">
            <div class="col s9">
                <section>
                    <div class="bordered no-padding">
                        <div class="row">
                            <div class="col s3">
                                <img class="school-icon "  src="{{asset($school->logo_url)}}" />
                                <h4 class="center-align">{{$school->name}}</h4>
                            </div>
                            <div class="col s9">
                                <h3 class="blue-text">Description</h3>
                                <div class="divider"></div>
                                <p>{{$school->brief_summary}}</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col s3">
                <section>
                    <div class="bordered no-padding">
                        <div class="row">
                            <div style="padding: 5%">
                                <div class="col s3">
                                    <img src="{{asset('/images/location.png')}}" class="events-icon tooltipped"  data-position="bottom" data-delay="50" data-tooltip="Location"/>
                                </div>
                                <div class="col s9"><h5>{{$school->location}}</h5></div>
                            </div>
                            <div style="margin: 5%"></div>
                            <div class="col s12">
                                <div class="col s3">
                                    <img src="{{asset('/images/calendar.png')}}" class="events-icon tooltipped"  data-position="bottom" data-delay="50" data-tooltip="Date Founded"/>
                                </div>
                                <div class="col s9"><h5>{{$school->date_founded}}</h5></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>



    <style>
        .profile-header{
            background-image: url({{asset('/images/school.jpg')}});
            background-size: 100% 100%;
        }

        .bordered{
            margin-top: 0 !important;
        }
    </style>

@endsection