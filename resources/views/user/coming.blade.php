@extends('layouts.master')

@section('content')
    <h1 class="page-center">Coming soon!</h1>
    <style>
        .page-center{
            position: absolute;
            top: 50%;
            left: 40%;
            right: 40%;
        }

        .page-footer{
            position: absolute !important;
            bottom: 0 !important;
        }
    </style>
@endsection
