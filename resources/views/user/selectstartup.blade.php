
@extends('layouts.master')


@section('content')
<section id="startup">
    <div id="select-panel" class="container">
        <div class="bordered">
            <div>
                <h2 class="grey-text">Select a startup to update</h2>
            </div>
            <div class="divider"></div>
            @if(empty($startups))
                <div class="row">
                    <div class="col s12 ">
                        <h4 class="center-align grey-text">Sorry, you have not registered any startup</h4>
                    </div>
                </div>
            @endif
            @if($startups)
                @foreach($startups as $startup)
                    <div class="row">
                        <div class="col s4 m4 l4">
                            <h3 class="grey-text">{{$startup->startup_name}}</h3>
                        </div>
                        <div class="col s3 m4 l4" style="margin-left: -10px">
                            <button class="btn waves-effect blue" onclick="location.href='{{url("/startup/".$startup->id)}}'" type="button">View</button>
                        </div>
                        <div class="col s3 m3 l3" style="margin-left: 20px" >
                            <button class="btn waves-effect blue" onclick="location.href='{{url("/updatestartup/$startup->id")}}'" type="button">Update</button>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
@endsection