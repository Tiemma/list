@extends('layouts.master')


@section('content')
    @if(!empty($startup))
        <?php
        $startup_id = $startup->id;
        $previous = false;
        if(strpos(URL::previous(),'registerstartup') !== false){
            $previous = 'register';
        }
        if(strpos(URL::previous(),'updatestartup') !== false){
            $previous = 'update';
        }
        ?>

        @if($previous == 'register')
            <div class="container">
                <div class="row">
                    <div class="success-banner">
                        <p class="center-align">You have successfully registered your startup</p>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($_COOKIE['update']))
            <div class="center-align" id="success" style="width: 70%; margin-left: 15%;">
                <div class="card-panel col s4 push-s4" style="background-color: rgba(33, 150, 243, 0.7); border: 2px solid #4f9acc; border-radius: 10px;">
                    <span id="x" style="font-size: 200%;position: relative;margin-top: -2%;cursor: pointer;font-weight: 100; float: right;" onclick="$('#success').hide()">x</span>
                    <span style="color: white">Your data was successfully updated</span>
                </div>
            </div>
            <script>
                setTimeout(function(){ $('#success').fadeOut(2000); }, 2000)
            </script>
        @endif
        <form method="POST" action="{{url('/updatestartup/'.$startup_id)}}" enctype="multipart/form-data" onsubmit="founder_check()">
            <section id="Overview">
                <div class="container">
                    <div class="bordered">
                        <div>
                            <h2 class="grey-text">Overview</h2>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col s12 m8 l8">
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Name</h4>
                                    </div>
                                    <div class="col s8">
                                        <input name="startup_name" value="{{$startup->startup_name}}" type="text" class="input-field">
                                        @if($errors->has('startup_name'))<p class="error">{{$errors->first('startup_name')}}</p> @endif
                                    </div>
                                    <div class="row">
                                        <div class="col s4">
                                            <h4>Short Description</h4>
                                        </div>
                                        <div class="col s8">
                                            <textarea name="startup_brief" class="s-textarea">{{$startup->startup_brief}}</textarea>
                                            @if($errors->has('startup_brief'))<p class="error">{{$errors->first('startup_brief')}}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row founder-add col m12" >
                                    <div class="col s4 left"><h4>Founder's Name(s)</h4></div>
                                    <div class="founder-span col m4">
                                        <input type="text" class="autocomplete" required placeholder="Username / Email" oninput="search($(this))"/>
                                    </div>

                                    <div class="col m4 right">
                                        <ul  class='items' style="box-shadow: 0 2px 10px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);">
                                        </ul>
                                    </div>
                                </div>
                                <style>
                                    div.chip{
                                        width:  100% !important;
                                        font-size: 10px !important;
                                        cursor: pointer;
                                    }
                                </style>
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Category</h4>
                                    </div>
                                    <div class="col s4 input-field">
                                        <?php $category = array('Health', 'Finance', 'Telecoms', 'Building and Infrastructure', 'Banking', 'NGO', 'Other') ?>
                                        <select name="category" class="browser-default" required>
                                            <option value="" disabled selected>Choose your option</option>
                                            @foreach($category as $key=>$option)
                                                @if($key  == $startup->category)
                                                    <option selected value="{{$key}}">{{$option}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$option}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col s4 none other-category">
                                        <input type="text" placeholder="Enter your category" name="other-category" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Website</h4>
                                    </div>
                                    <div class="col s8 input-field">
                                        <input name="website" value="{{$startup->website}}" placeholder="http://startupwebsite.com" class="col s12 input-field"/>
                                        @if($errors->has('website'))<p class="error">{{$errors->first('website')}}</p> @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Social</h4>
                                    </div>
                                    <div class="col s8 input-field">
                                        <input name="facebook" placeholder="Username e.g ...com/username" class="col s12 input-field" value="{{$startup->facebook}}"/>
                                        <input name="twitter" placeholder="Handle e.g ...com/username" class="col s12 input-field" value="{{$startup->twitter}}"/>
                                        <input name="linkedin" placeholder="Username e.g .../in/username" class="col s12 input-field" value="{{$startup->linkedin}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4 l4 hide-on-small-only">
                                @if(!empty($startup->logo_url))
                                    <div class="profile-pic large">
                                        <img class="startup-image responsive-img" src="{{asset($startup->logo_url)}}" />
                                        <div>
                                            <input class="upload none" accept=".jpeg, .jpg, .png" type="file" name="profile_pic" id="profile_pic ">
                                        </div>
                                    </div>
                                    <div class="row col s8 push-s2 update">
                                        <button type="button" class="select-img btn btn-flat col s3">Update</button>
                                        <button type="button" class="remove-img btn btn-flat col s3 remove" onclick="remove_picture()">Remove</button>
                                    </div>
                                @else
                                    <div class="profile-pic large">
                                        <img class="startup-image responsive-img" src="{{$startup->logo_url}}" />
                                        <div>
                                            <input class="upload none" accept=".jpeg, .jpg, .png" type="file" name="profile_pic" id="profile_pic ">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button type="button" class="select-img  btn btn-flat">Select Image</button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script>
                function remove_picture(){
                    $('.update').append(`<input type='hidden' name='remove' class='remove_pic' value='1' />`);
                }
            </script>
            <!--- End of overview --->

            <section id="comapany-details">
                <div class="container">
                    <div class="bordered">
                        <div>
                            <h2 class="grey-text">Company Details</h2>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col s12 m8 l8">
                                <div class="row">
                                    <div class="col s4"><h4>Founded</h4></div>
                                    <div class="col s8">
                                        <input name="founded" value="{{$startup->founded}}" type="text" class="input-field">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4"><h4>Contact</h4></div>
                                    <div class="col s8 input-field">
                                        <input name="phone_number" value="{{$startup->phone_number}}" placeholder="Phone number" class="col s12 input-field"/>
                                        <input name="email" value="{{$startup->email}}" type="email" placeholder="Email Address" class="col s12 input-field"/>
                                        @if($errors->has('email'))<p class="error">{{$errors->first('email')}}</p> @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4"><h4>Team Size</h4></div>
                                    <div class="col s8 input-field">

                                        <select name="team_size" class="browser-default">
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                    $('select[name=team_size] option[value={{isset($startup->team_size) ? $startup->team_size : 0}}]').attr('selected', 'selected');
                                                })
                                            </script>
                                            <option value="" disabled selected>please select from the list</option>
                                            <option value="1">1 - 5</option>
                                            <option value="2">5 - 10</option>
                                            <option value="3">10 - 15</option>
                                            <option value="4">15 - 20</option>
                                            <option value="5">20 - 25</option>
                                            <option value="6">25 - 50</option>
                                            <option value="7">50 - 100</option>
                                            <option value="8">Over 100</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4"><h4>Long Description</h4></div>
                                    <div class="col s8 input-field">
                                    <textarea name="startup_long" placeholder="Long description of your startup 2000 words" class="col s12 input-field l-textarea">
                                        {{$startup->startup_long}}
                                    </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4 hide-on-small-only">

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="founding-rounds">
                <div class="container">
                    <div class="bordered">
                        <div>
                            <h2 class="grey-text">Founding Rounds</h2>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Date</h4>
                                    </div>
                                    <div class="col s8">
                                        <input value="{{$startup->date}}" name="date" type="date" class="datepicker">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Amount</h4>
                                    </div>
                                    <div class="col s8 input-field">
                                        <input value="{{$startup->amount}}" name="amount" placeholder="Amount in Naira or Dollars" class="col s12 input-field"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Round</h4>
                                    </div>
                                    <div class="col s8 input-field">
                                        <select name="round" class="browser-default">
                                            <option value="" disabled selected>please select from the list</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4">
                                        <h4>Investor</h4>
                                    </div>
                                    <div class="col s8 input-field">
                                        <input value="{{$startup->investor}}" name="investor" placeholder="investor name or company" class="col s12 input-field"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4 l4 hide-on-small-only">

                            </div>
                        </div>
                        <button type="submit" class="btn waves-effect waves-light blue submit-btn" onclick="founder_check()">Update Startup</button>
                    </div>
                </div>
            </section>
        </form>
        <script type="text/javascript" class="script_remove">


            $(document).ready(function(){

                $.get(baseUrl+"/public/search/update_startup?id={{$startup->id}}")
                    .done(function (data) {
                        try {
                            var url = null;
                            var array = $.map(data, function (value, index) {
                                return [value];
                            });

                            if (array[0].length > 0) {
                                array[0].forEach(function (entry) {

                                    try {
                                        if (!/^(f|ht)tps?:\/\//i.test(entry['profile_pics_url'].replace(/ /g, ''))) {
                                            url = baseUrl +
                                                '/public' +
                                                entry['profile_pics_url'];
                                        } else {
                                            url = entry['profile_pics_url'];
                                        }
                                    } catch (e) {
                                        url = baseUrl + '/public/images/techpoint.png';
                                    }

                                    var text = `
                            <div class="chip">
                                <img  src="` + url + `" alt="User" />` +
                                        entry['first_name'] +
                                        ' ' +
                                        entry['last_name'] +
                                        `<i class="material-icons close" onclick="remove_email('` + entry['email'] + `')">close</i>
                                 <input type="hidden" value="` + entry['email'] + `" name="founder[]" id="founder_name" />
                            </div>
`;

                                    emails_added.push(entry['email']);

                                    $('.founder-span').append(text);
                                });

                            }
                        }catch(e){
                        }


                    });
                $('.script_remove').remove();

            });

        </script>


    @endif


@endsection
