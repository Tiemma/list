@extends('layouts.master')

@section('content')

    {{--<div class="container">
        <div id="welcome-banner" class="row">
            <div class="welcome-banner">
                <h2 class="center-align"><strong>Welcome to Techpoint List</strong></h2>
            </div>
        </div>
    </div>--}}

    <div class="container">
        <div class="row">
            <div class="col s9 m9 search-container">
                <br />
                <form method="get" action={{asset("/search")}}>
                    <input id="search-box" placeholder="search.." name="search" class="black-text search-box center" type="search">
                    <div class="search-board row white black-text lighten-4">
                        <div class="labels">
                            <span class="col s4 filter grey-text">Type</span>
                            <span class="col s4 filter grey-text">Category</span>
                            <span class="col s4 filter grey-text">Location</span>
                        </div>
                        <div class="input-field col s4 ">
                            <select name="type" class="category browser-default" id="type">
                                <option value="" selected disabled>search type</option>
                                <option value="user">User</option>
                                <option value="investor">Investor</option>
                                <option value="startup">Startup</option>
                                <option value="company">Company</option>
                                <option value="project">Project</option>
                            </select>

                        </div>
                        <div class="col s4 m4 input-field">
                            <select name="category" class="category browser-default" id="category">
                                <option value="" selected disabled>by category</option>
                                <option value="power">Power</option>
                                <option value="agric">Agric</option>
                            </select>
                        </div>
                        <div class="input-field col s4">
                            <select name="location" class="location browser-default" id="location">
                                <option value="" selected disabled>by location</option>
                                <option value="Ibadan">Ibadan</option>
                                <option value="Lagos">Lagos</option>
                            </select>
                        </div>
                        <button class="search-btn btn blue white-text" id="search">Search</button>
                        <span class="col s12 search-close"><a href=" "> <i class="material-icons small">keyboard_arrow_up</i></a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row" style="padding: 1%">
        <div class="trending col s9 m9">
            <div class="row">
                <div class="col s10 m10">
                    <h1>TRENDING </h1>
                    <div class="underline"></div>
                </div>
            </div>
            <div class="row">
                <div class="col m12 s12">
                    <div class="col s3 m3 landing">
                        <div class="btn blue">SCHOOLS</div>
                        <div class="divider"></div>
                        @foreach($schools as $school)
                            <div class="card">
                                <div class="card-image z-depth-1">
                                    <img src="{{asset($school->logo_url)}}">
                                </div>
                                <div class="card-content">
                                    <p class="truncate">{{$school->brief_summary}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="{{asset("/school/$school->id")}}">Read More...</a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col s3 m3 landing">
                        <div class="btn blue">EVENTS</div>
                        <div class="divider"></div>
                        @foreach($investors as $investor)
                            <div class="card">
                                <div class="card-image z-depth-1">
                                    <img src="{{asset($investor->profile_pics_url)}}">
                                </div>
                                <div class="card-content">
                                    <p class="truncate">{{$investor->brief_profile}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="{{asset("/event/$investor->id")}}">Read More...</a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col s3 m3 landing">
                        <div class="btn blue">COMPANIES</div>
                        <div class="divider"></div>
                        @foreach($startups as $startup)
                            <div class="card">
                                <div class="card-image z-depth-1">
                                    <img src="{{asset($startup->logo_url)}}">
                                </div>
                                <div class="card-content">
                                    <p class="truncate">{{$startup->startup_brief}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="{{asset("/startup/$startup->id")}}">Read More...</a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col s3 m3 landing">
                        <div class="btn blue">PERSONALITIES</div>
                        <div class="divider"></div>
                        @foreach($users as $user)
                            <div class="card">
                                <div class="card-image z-depth-1">
                                    <img src="{{asset($user->profile_pics_url)}}">
                                </div>
                                <div class="card-content">
                                    <p class="truncate">{{$user->brief_profile}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="{{asset("/user/$user->id")}}">Read More...</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col m3 s3">
            <div class="ads-content">
                <img src="{{asset('/images/ad_side_2.png')}}" />
                <div style="padding-bottom: 10%"></div>
                <img src="{{asset('/images/ads_side.png')}}" />
            </div>
        </div>
    </div>



    {{--
            <div class="row">
                <div class="btn blue">BUSINESSES</div>
            </div>
            <div class="row">
                @foreach($startups as $startup)
                <div class="row">
                    <div class="col m4">
                        <div class="card horizontal">
                            <div class="card-image col s3">
                                <img class="trend-img" src="{{asset($startup->logo_url)}}">
                            </div>
                            <div class="card-stacked">
                                <h2 class="header">Horizontal Card</h2>
                                <div class="card-content">
                                    <p>{{$startup->startup_brief}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="#">Read more...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                --}}{{--<div class="col s12 m4">
                    <div class="trend-card">I am a very simple card. I am good at containing small
                        bits of information. I am convenient because I require
                        little markup to use effectively.
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="trend-card">I am a very simple card. I am good at containing small
                        bits of information. I am convenient because I require
                        little markup to use effectively
                    </div>
                </div>
                <div class="col s3 m3 push-s9 push-m9">
                    <h3 class="right blue-text">more...</h3>
                </div>--}}{{--
            </div>
            <div class="divider"></div>
        </div>
    </section>

    <section id="events" class="landing-page">
        <div class="container">
            <div class="row">
            </div>
            <div class="row">
                <div class="btn blue">INVESTORS</div>
                <div class="divider"></div>
            </div>
            <div class="row">
                @foreach($investors as $investor)
                <div class="row">
                    <div class="col m4">
                        <div class="card horizontal">
                            <div class="card-image col s3">
                                <img class="trend-img" src="{{asset($investor->profile_pics_url)}}">
                            </div>
                            <div class="card-stacked">
                                <h2 class="header">Horizontal Card</h2>
                                <div class="card-content">
                                    <p>{{$investor->brief_profile}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="#">Read more...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="divider"></div>
        </div>
    </section>

--}}{{--
    <section id="events" class="landing-page">
        <div class="container">
            <div class="row">
            </div>
            <div class="row">
                <div class="btn blue">PROJECTS</div>
                <div class="divider"></div>
            </div>
            <div class="row">
                <div class="row">
                    <div class="col s12 m12">
                        <div class="card horizontal">
                            <div class="card-image">
                                <img class="trend-img" src="{{asset('/images/image.jpg')}}">
                            </div>
                            <div class="card-stacked">
                                <h2 class="header">Horizontal Card</h2>
                                <div class="card-content">
                                    <p>           The greatness and popularity of the Obafemi Awolowo University (OAU) is by no means an accident.
                                        Founded in 1962 as the University of Ife but rechristened by the Federal Military Government as the Obafemi Awolowo University on May 12, 1987,
                                        in honour of one of its most distinguished founding fathers and eminent nationalist, politician, lawyer, stateman and former chancellor, Chief Jeremiah Obafemi Awolowo
                                    </p>
                                </div>
                                <div class="card-action">
                                    <a href="#">This is a link</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 m12">
                        <div class="card horizontal">
                            <div class="card-image">
                                <img class="trend-img" src="{{asset('/images/image.jpg')}}">
                            </div>
                            <div class="card-stacked">
                                <h2 class="header">Horizontal Card</h2>
                                <div class="card-content">
                                    <p>           The greatness and popularity of the Obafemi Awolowo University (OAU) is by no means an accident.
                                        Founded in 1962 as the University of Ife but rechristened by the Federal Military Government as the Obafemi Awolowo University on May 12, 1987,
                                        in honour of one of its most distinguished founding fathers and eminent nationalist, politician, lawyer, stateman and former chancellor, Chief Jeremiah Obafemi Awolowo
                                    </p>
                                </div>
                                <div class="card-action">
                                    <a href="#">This is a link</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                --}}{{----}}{{--<div class="col s12 m4">
                    <div class="trend-card">I am a very simple card. I am good at containing small
                        bits of information. I am convenient because I require
                        little markup to use effectively.
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="trend-card">I am a very simple card. I am good at containing small
                        bits of information. I am convenient because I require
                        little markup to use effectively
                    </div>
                </div>
                <div class="col s3 m3 push-s9 push-m9">
                    <h3 class="right blue-text">more...</h3>
                </div>--}}{{----}}{{--
            </div>
            <div class="divider"></div>
        </div>
    </section>--}}
@endsection
