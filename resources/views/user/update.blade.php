@extends('layouts.master')

@section('content')

    <script>
        {{--This initialises the checkbox fields and sets their values in the hidden text field--}}
        {{--Do not remove from here as the functions has to be defined before its called --}}
        function check(i){
            if(document.getElementById('work'+i).checked) value = "on";
            else value = "off";
            document.getElementById('unchecked'+i).value = value;
        }
    </script>
    <form action="/update" method="POST" enctype="multipart/form-data" onsubmit="chips()">
        {{csrf_field()}}
        {{--This section is for creating arrays from strings stored in the db and to also check whether it even exists in the first place--}}

        {{--Check whether there are any schools in the db--}}
        @if($user_school != NULL)
            <?php $school_name = $user_school['0']['original']['name'] ?>
        @else
            <?php $school_name = '' ?>
        @endif

        {{--Check whether the social media channel column data exists--}}
        @if(count($user['social_channel'])!= NULL)
            <?php $user_channels = explode('|',$user['social_channel']) ?>
        @else
            <?php $user_channels = array(); ?>
        @endif

        @if(session('success'))
        <div class="center-align" id="success" style="width: 70%; margin-left: 15%;">
            <div class="card-panel blue lighten-6 col s4 push-s4">
                <span id="x" style="font-size: 200%;position: relative;margin-top: -3%;opacity: 1; cursor: pointer;font-weight: 100; float: right;" onclick="document.getElementById('success').style.display = 'none'">x</span>
                <span style="color: white">Your data was successfully updated
            </div>
        </div>
        @endif

        <section id="main-details">
            <div class="container">
                <div class="details">
                    <div>
                        <h2 class="grey-text lighten-5">Update Profile</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12 m8">
                            <div class="row">
                                <div class="input-field col s12">
                                    <h5 class="center-align">
                <span class="col s4" class="col s4" style="float: left">
                    Names &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                                        <input id="name" placeholder="Enter your name" name="name" value="{{$user['first_name'].' '.$user['last_name']}}" type="text" class="validate col s8 " required style=" width: 50%; height: 1rem;"></h5>
                                </div>
                                @if($errors->first('name'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('name')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <h5 class="center-align">
                                        <span class="col s4" style="float: left">Email Address</span>
                                        <input id="email" name="email" placeholder="Enter your email" required type="email" class="validate col s8" value="{{$user['email']}}"  style=" width: 50%; height: 1rem;">
                                    </h5>
                                </div>
                                @if($errors->first('email'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('email')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <span class="col s4" style="float: left">Phone Number</span>
                                        <div class="col s7">
                                            <select class="browser-default col s3 left-align select border">
                                                <option value="234">+234</option>
                                            </select>
                                            @if($user['phone_number'] == NULL)
                                                <input id="phone" required placeholder="Enter your phone number" name="phone_number" type="number"  class="validate col s10" style=" width: 60%; height: 1rem;">
                                            @else
                                                <input id="phone" required placeholder="Enter your phone number" value="{{$user['phone_number']}}" name="phone_number" type="number" value="{{$user['phone_number']}}" class="validate col s10" style=" width: 60%; height: 1rem;">
                                            @endif

                                        </div>
                                    </h5>
                                </div>
                                @if($errors->first('phone_number'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('phone_number')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <h5 class="center-align"><span class="col s4" style="float: left">Date of Birth&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        @if($user['dob'] != NULL)
                                            <select class="browser-default col s2 left-align select" required name="day">
                                                <option disabled selected>Day</option>
                                                <?php $dob = explode(' ', $user['dob']) ?>
                                                @for($i=1;$i < 32; $i++)
                                                    @if($i == $dob[0])
                                                        <option selected value="{{$i}}">{{$i}}</option>
                                                    @else
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endif
                                                @endfor
                                            </select>
                                            <select class="browser-default col s2 left-align select" required name="month">
                                                <option disabled selected>Month</option>
                                                @for($i=1;$i < count($months); ++$i)
                                                    @if($i == $dob[1])
                                                        <option selected value="{{$i}}">{{$months[$i]}}</option>
                                                    @else
                                                        <option value="{{$i}}">{{$months[$i]}}</option>
                                                    @endif
                                                @endfor
                                            </select>
                                            <select class="browser-default col s2 left-align select"  required name="year">
                                                <option disabled selected>Year</option>
                                                @for($i=1900;$i <= date("Y"); $i++)
                                                    @if($i == $dob[2])
                                                        <option selected value="{{$i}}">{{$i}}</option>
                                                    @else
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endif                                            @endfor
                                            </select>
                                        @else
                                            <select class="browser-default col s2 left-align select" required name="day">
                                                <option disabled selected>Day</option>
                                                @for($i=1;$i < 32; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                            <select class="browser-default col s2 left-align select" required name="month">
                                                <option disabled selected>Month</option>
                                                @for($i=1;$i < count($months); ++$i)
                                                    <option value="{{$i}}">{{$months[$i]}}</option>
                                                @endfor
                                            </select>
                                            <select class="browser-default col s2 left-align select"  required name="year">
                                                <option disabled selected>Year</option>
                                                @for($i=1900;$i <= date("Y"); $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        @endif
                                    </h5>
                                </div>
                                @if($errors->first('day'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('day')}}</div>
                                @endif
                                @if($errors->first('month'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('month')}}</div>
                                @endif
                                @if($errors->first('year'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('year')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <span class="col s4" style="float: left;">School Attended</span>
                                        <div class="col s8">
                                            <select class="browser-default col s9 left-align select" name="school">
                                                <option disabled selected>Choose your school</option>
                                                @foreach($schools as $school)
                                                    @if($school_name == $school)
                                                        <option selected value="{{$school}}">{{$school}}</option>
                                                    @else
                                                        <option value="{{$school}}">{{$school}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </h5>
                                </div>
                                @if($errors->first('school'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('school')}}</div>
                                @endif
                            </div>

                        </div>

                        <div class="col s12 m4">
                            <br />
                            <img src="
                            @if($user['picture_url'] == NULL) {{asset('/img/techpoint.png')}}
                            @else {{$user['picture_url']}}
                            @endif
                                    " class="file_div responsive-img right-align circle" id="logo" style="cursor: pointer;border: 1px solid #4f9acc;">
                            <input accept=".jpeg, .jpg, .png" type="file" id="file"  style="display: none;" name="logo" >
                            <button type="button" class="btn btn-success center-align picture" onclick="upload()" style="display:none;width:100%">Remove the picture</button>
                            @if($user['picture_url'] == NULL)
                                <div class="blue-text upload center-align">Upload profile photo</div>
                            @else
                                <div class="blue-text upload center-align">Change profile photo</div>
                            @endif
                            {{--Logo is just the name for the user's profile picture--}}
                            @if($errors->first('logo'))
                                <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('logo')}}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="user-details">
            <div class="container">
                <div class="details">
                    <div>
                        <h2 class="grey-text lighten-5">Personal User Details</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12 m8">
                            <div class="row">
                                <div class="row">
                                    <div class="input-field col s12" >
                                        <h5 class="center-align">
                                            <span class="col s4" style="float: left">Brief Profile</span>
                                            <textarea id="profile" placeholder="Brief Profile" name="profile"  class="validate col s4 materialize-textarea" style=" width: 50%; height: 1rem">@if($user['brief_profile'] != NULL){{$user['brief_profile']}}@endif</textarea>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            @if($errors->first('profile'))
                                <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('profile')}}</div>
                            @endif

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <span class="col s4" style="float: left;">Gender</span>
                                        <div class="col s8">
                                            <select class="browser-default col s3 left-align select" style="width: 80%" name="gender" required>
                                                @if($user['sex'] == NULL)
                                                    <option disabled selected>Gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="0">Female</option>
                                                @else
                                                    @if($user['sex'] == 1)
                                                        <option disabled>Gender</option>
                                                        <option selected value="1">Male</option>
                                                        <option value="0">Female</option>

                                                    @else
                                                        <option disabled>Gender</option>
                                                        <option  value="1">Male</option>
                                                        <option selected value="0">Female</option>
                                                    @endif
                                                @endif
                                            </select>
                                        </div>
                                    </h5>
                                </div>
                                @if($errors->first('gender'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('gender')}}</div>
                                @endif
                            </div>
                            <div id="channel">
                                <?php $channels = array('Facebook', 'Twitter', 'Instagram', 'Youtube', 'SnapChat') ?>
                                <?php $other_channels = array('LinkedIn', 'FourSquare', 'Skype', 'About.Me', 'BeHance', 'Reddit', 'Other') ?>
                                @if(empty($user_channels))
                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <span class="col s4" style="float: left">Social Channel</span>
                                                <div class="col s8">
                                                    <select class="browser-default col s3 left-align select border" name="channel[]">
                                                        <option disabled selected="">Select a media channel</option>
                                                        @foreach($channels as $channel)
                                                            <option value="{{$channel}}">{{$channel}}</option>
                                                        @endforeach
                                                        <optgroup label="Other Channels">
                                                            @foreach($other_channels as $channel)
                                                                <option value="{{$channel}}">{{$channel}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                    <input id="handle" required placeholder="Enter your social handle" name="handle[]" type="text" class="validate col s4" style=" width: 55%; height: 1rem;">
                                                </div>
                                            </h5>
                                        </div>
                                    </div>
                                @else
                                    @foreach($user_channels as $channel)
                                        @if(!empty($channel))
                                                <?php $user_details = explode('*', $channel) ?>
                                        <div class="row">
                                            <div class="input-field col s12" >
                                                <h5 class="center-align">
                                                    <span class="col s4" style="float: left">Social Channel</span>
                                                    <div class="col s8">
                                                        <select class="browser-default col s3 left-align select border" name="channel[]">
                                                            <option disabled>Select a media channel</option>
                                                            @foreach($channels as $channel)
                                                                    @if($user_details[1] == $channel)
                                                                        <option selected value="{{$channel}}">{{$channel}}</option>
                                                                    @else
                                                                        <option value="{{$channel}}">{{$channel}}</option>
                                                                    @endif
                                                            @endforeach
                                                            <optgroup label="Other Channels">
                                                                @foreach($other_channels as $channel)
                                                                    <option value="{{$channel}}">{{$channel}}</option>
                                                                @endforeach
                                                            </optgroup>
                                                        </select>
                                                        @if($user_details[0] == NULL)
                                                            <input id="handle"  placeholder="Enter your social handle" name="handle[]" type="text"  class="validate col s2" style=" width: 75%; height: 1rem;">
                                                        @else
                                                            <input id="handle"  placeholder="Enter your phone number" value="{{$user_details[0]}}" name="handle[]" type="text" class="validate col s2" style=" width: 50%; height: 1rem;">
                                                        @endif
                                                    </div>
                                                </h5>
                                            </div>
                                        </div>
                                            @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12" >
                                <h5 class="center-align">
                                    <button type="button" class="btn btn-success right-align col s2 push-s5" onclick="channel()">Add Channel</button>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="details-2">
            <div class="container">
                <div class="details">
                    <div>
                        <h2 class="grey-text lighten-5">Skills and Experience</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12 m8">
                            @if(count($projects) == NULL)
                                <div id="project">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <h5 class="center-align"><span class="col s4 m4" style="float: left">Projects worked on</span><input id="project_name" placeholder="Name of project" name="project_name[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem"></h5>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <textarea id="project_description" placeholder="Description" name="project_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem"></textarea>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @foreach($projects as $project)
                                    <div id="project">
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <h5 class="center-align"><span class="col s4 m4" style="float: left">Projects worked on</span><input id="project_name" placeholder="Name of project" value="{{$project['name']}}" name="project_name[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem"></h5>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s12" >
                                                <h5 class="center-align">
                                                    <textarea id="project_description" placeholder="Description" name="project_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem">{{$project['description']}}</textarea>
                                                </h5>
                                            </div>
                                        </div>
                                    </di    v>
                                @endforeach
                            @endif

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <button type="button" class="btn btn-success right-align col s4 offset-s6" onclick="project()">Add more projects</button>
                                    </h5>
                                </div>
                            </div>

                            <div id="experience">
                                @if(count($experiences) > 0)
                                    @foreach($experiences as $experience)
                                        <?php $val = rand() ?>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <h5 class="center-align">
                                                    <span class="col s4 m4" style="float: left">
                                                        Work place
                                                        <div class="col s12">(Present and Previous)</div>
                                                    </span>
                                                    <input id="company_name" placeholder="Name of company" name="company_name[]" type="text" value="{{$experience['company_name']}}" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                                </h5>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s12" >
                                                <h5 class="center-align">
                                                    <input id="role" placeholder="Role" name="role[]" value="{{$experience['role']}}" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                                </h5>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s12" >
                                                <h5 class="center-align">
                                                    <textarea id="project_description" placeholder="Description" name="work_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem">{{$experience['description']}}</textarea>
                                                </h5>
                                            </div>
                                        </div>
                                        @if($experience['start_date'] != NULL)
                                            <?php $from = explode(' ', $experience['start_date']) ?>
                                        @endif

                                        @if($experience['end_date'] != NULL)
                                            <?php $to = explode(' ', $experience['end_date']) ?>
                                        @endif
                                        <div class="row">
                                            <div clas="col s12">
                                                <div class="input-field col s8 offset-s4 pull-s1" >
                                                    <span class="col s1" style="font-size: 10px;margin-right: 13px;">From</span>
                                                    <select class="browser-default col s2  left-align border-bottom border select" name="from_month[]" style=" margin-right: 5px;">
                                                        @if($experience['start_date'] == NULL)
                                                            <option disabled selected>Month</option>
                                                            @for($i = 1; $i < count($months); $i++)
                                                                <option value="{{$i}}">{{$months[$i]}}</option>
                                                            @endfor
                                                        @else
                                                            <option disabled>Month</option>
                                                            @for($i=1;$i < count($months); ++$i)
                                                                @if($i == $from[0])
                                                                    <option value="{{$i}}" selected>{{$months[$i]}}</option>
                                                                @else
                                                                    <option value="{{$i}}">{{$months[$i]}}</option>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    </select>
                                                    <select class="browser-default col s2 left-align border select" name="from_year[]" class="border" style=" margin-right: 5px;">
                                                        <option disabled selected>Year</option>
                                                        @if($experience['start_date'] !=  NULL)
                                                            @for($i=1900;$i <= date("Y"); $i++)
                                                                @if($i == $from[1])
                                                                    <option selected value="{{$i}}">{{$i}}</option>
                                                                @else
                                                                    <option value="{{$i}}">{{$i}}</option>
                                                                @endif
                                                            @endfor
                                                        @else
                                                            @for($i=1900;$i <= date("Y"); $i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                        @endif
                                                    </select>
                                                    <span class="col s1" style="font-size: 10px;">To</span>
                                                    <select class="browser-default col s2  left-align border select" name="to_month[] select" style=" margin-right: 5px;">
                                                        @if($experience['end_date'] == NULL)
                                                            <option disabled selected>Month</option>
                                                            @for($i=1; $i < count($months); $i++)
                                                                <option value="{{$i}}">{{$months[$i]}}</option>
                                                            @endfor
                                                        @else
                                                            <option disabled>Month</option>
                                                            @for($i=1;$i < count($months); ++$i)
                                                                @if($i == $to[0])
                                                                    <option value="{{$i}}" selected>{{$months[$i]}}</option>
                                                                @else
                                                                    <option value="{{$i}}">{{$months[$i]}}</option>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    </select>
                                                    <select class="browser-default col s2 left-align border select" name="to_year[] select" style=" margin-right: 5px;">
                                                        <option disabled selected>Year</option>
                                                        @if($experience['end_date'] !=  NULL)
                                                            @for($i=1900;$i <= date("Y"); $i++)
                                                                @if($i == $to[1])
                                                                    <option selected value="{{$i}}">{{$i}}</option>
                                                                @else
                                                                    <option value="{{$i}}">{{$i}}</option>
                                                                @endif
                                                            @endfor
                                                        @else
                                                            @for($i=1900;$i <= date("Y"); $i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s12">
                                                <div class="col s5 offset-s7 pull-s1">
                                                    @if($experience['employed_status'] == NULL)
                                                        <input type="checkbox" class="checkbox" id="work{{$val}}" onchange="check({{$val}})">
                                                        <label for="work{{$val}}">Currently work here</label>
                                                    @else
                                                        <input type="checkbox" class="checkbox" checked id="work{{$val}}" onchange="check({{$val}})">
                                                        <label for="work{{$val}}">Currently work here</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                            <input type="text" hidden id="unchecked{{$val}}" value="off" name="work_status[]">
                                            <script>check({{$val}})</script>
                                    @endforeach
                                @else
                                    <?php $val = rand() ?>
                                    <div class="input-field col s12">
                                            <h5 class="center-align">
                                                    <span class="col s4 m4" style="float: left">
                                                        Work place
                                                        <div class="col s12">(Present and Previous)</div>
                                                    </span>
                                                <input id="company_name" placeholder="Name of company" name="company_name[]" type="text"  class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                            </h5>
                                        </div>

                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <input id="role" placeholder="Role" name="role[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                            </h5>
                                        </div>


                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <textarea id="project_description" placeholder="Description" name="work_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem"></textarea>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div clas="col s12">
                                            <div class="input-field col s8 offset-s4 pull-s1" >
                                                <span class="col s1" style="font-size: 10px;margin-right: 13px;">From</span>
                                                <select class="browser-default col s2  left-align border-bottom border select" name="from_month[]" style=" margin-right: 5px;">
                                                    <option disabled selected>Month</option>
                                                    @for($i=1; $i < count($months); $i++)
                                                        <option value="{{$i}}">{{$months[$i]}}</option>
                                                    @endfor
                                                </select>
                                                <select class="browser-default col s2 left-align border select" name="from_year[]" class="border" style=" margin-right: 5px;">
                                                    <option disabled selected>Year</option>
                                                    @for($i=1900;$i <= date("Y"); $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                                <span class="col s1" style="font-size: 10px;">To</span>
                                                <select class="browser-default col s2  left-align border select" name="to_month[] select" style=" margin-right: 5px;">
                                                    <option disabled selected>Month</option>
                                                    @for($i=1; $i < count($months); $i++)
                                                        <option value="{{$i}}">{{$months[$i]}}</option>
                                                    @endfor
                                                </select>
                                                <select class="browser-default col s2 left-align border select" name="to_year[] select" style=" margin-right: 5px;">
                                                    <option disabled selected>Year</option>
                                                    @for($i=1900;$i <= date("Y"); $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12">
                                            <div class="col s5 offset-s7 pull-s1">
                                                <input type="checkbox" class="checkbox" id="work{{$val}}" onchange="check({{$val}})">
                                                <label for="work{{$val}}">Currently work here</label>
                                                    </div>
                                        </div>
                                    </div>
                        <input type="text" hidden id="unchecked{{$val}}" value="off" name="work_status[]">
                        <script>check({{$val}})</script>
                        @endif
                            </div>
                        </div>

                        <div class="row">
                                <div class="input-field col s8  pull-s4" >
                                    <h5 class="center-align">
                                        <button type="button" class="btn btn-success right-align col s4 offset-s6" style="Width: 20%" onclick="experience()">Add Experience</button>
                                    </h5>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s12 m8">
                                    <h5 class="center-align"><span class="col s4" class="col s4" style="float: left">Skills &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <div class="chips chips-initial col s4" style="width: 60%;">
                                        </div>
                                    </h5>
                                </div>
                            </div>

                                    <button class="btn btn-success waves-effect waves-ripple waves-teal" style="width: 60%; margin-left:40%">Submits<i class="material_icons" style="float: right">Submit</i></button>

                        </div>
                </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <script>

        i = 1000;
        function experience() {
            var dummy = `
         <div class="row">
                                        <div class="input-field col s12">
                                            <h5 class="center-align">
                                                    <span class="col s4 m4" style="float: left">
                                                        Work place
                                                        <div class="col s12">(Present and Previous)</div>
                                                    </span>
                                                <input id="company_name" placeholder="Name of company" name="company_name[]" type="text"  class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                            </h5>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <input id="role" placeholder="Role" name="role[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <textarea id="project_description" placeholder="Description" name="work_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem"></textarea>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div clas="col s12">
                                            <div class="input-field col s8 offset-s4 pull-s1" >
                                                <span class="col s1" style="font-size: 10px;margin-right: 13px;">From</span>
                                                <select class="browser-default col s2  left-align border-bottom border select" name="from_month[]" style=" margin-right: 5px;">
                                                    <option disabled selected>Month</option>
                                                    @for($i=1; $i < count($months); $i++)
                            <option value="{{$i}}">{{$months[$i]}}</option>
                                                    @endfor
                            </select>
                            <select class="browser-default col s2 left-align border select" name="from_year[]" class="border" style=" margin-right: 5px;">
                                <option disabled selected>Year</option>
                                @for($i=1900;$i <= date("Y"); $i++)
                            <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                            </select>
                            <span class="col s1" style="font-size: 10px;">To</span>
                            <select class="browser-default col s2  left-align border select" name="to_month[] select" style=" margin-right: 5px;">
                                <option disabled selected>Month</option>
                                @for($i=1; $i < count($months); $i++)
                            <option value="{{$i}}">{{$months[$i]}}</option>
                                                    @endfor
                            </select>
                            <select class="browser-default col s2 left-align border select" name="to_year[] select" style=" margin-right: 5px;">
                                <option disabled selected>Year</option>
                                @for($i=1900;$i <= date("Y"); $i++)
                            <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <div class="col s5 offset-s7 pull-s1">
                            <input type="checkbox" onchange='check(`+i+`)' class="checkbox" id="work`+i+`">
                    <label for="work`+i+`">Currently work here</label>
                </div>
            </div>
        </div>
        <input type="text" hidden id="unchecked`+i+`" value="off" name="work_status[]">
        <script>check(`+i+`)<\/script>
`;
            document.getElementById('experience').insertAdjacentHTML( 'beforeend', dummy );
            ++i;



        }


    </script>

    <div style="margin-bottom: 5%"></div>

    <style>
        input:not([type]), input[type=text], input[type=password], input[type=email], input[type=url], input[type=time], input[type=date], input[type=datetime], input[type=datetime-local], input[type=tel], input[type=number], input[type=search], textarea.materialize-textarea {
            margin: 0px;
        }

        .select{
            width:18%;
            height: 1rem;
            border: 0px solid transparent;
            border-bottom: 1px solid #26a69a ;
            font-size: 12px;"
        padding: 0 0 0 0;
        }

        .border{
            border: 1px solid #9e9e9e;
        }

        .chip{
            border-radius: 4px;
        }

    </style>

@endsection