@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m6 push-m3 search-container">
                <br />
                <input id="search-box" placeholder="Search Schools" name="search" class="black-text search-box center" type="search">
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <div class="page right">
                    <?php $current_page = $schools->currentPage() ?>
                    <span>{{ $schools->count() * $schools->currentPage() }}&nbsp; - &nbsp;{{$schools->count() * $j = $current_page + 1 }}&nbsp; of &nbsp;{{$schools->total()}}</span>
                    <a href="@if($current_page != 1){{$schools->previousPageUrl()}} @else {{"#"}} @endif" class="btn-flat @if($current_page == 1) {{'disabled'}} @endif"><</a>
                    <a href="@if($current_page != $schools->total() / $schools->count()) {{$schools->nextPageUrl()}} @else {{"#"}} @endif" class="btn-flat  @if($current_page == $schools->total() / $schools->count()) {{'disabled'}} @endif">></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m12">
                <table class="highlight bordered striped centered ">
                    <thead class="z-depth-1">
                    <tr>
                        <th>School Name</th>
                        <th>Current VP</th>
                        <th>Address</th>
                        <th>Ranking</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($schools as $school)
                        <tr>
                            <td>
                                <span class="col s8">
                                    <a href="{{asset('/school/'.$school->id)}}">
                                        <span class="people-text right">{{$school->name}}</span>
                                        <img class="responsive-img circle left" src="{{asset($school->logo_url)}}" />
                                    </a>
                                </span>
                            </td>
                            <td>{{$school->current_vp}}</td>
                            <td>{{$school->location}}</td>
                            <td>{{$school->id}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="not-found"></div>
            </div>
        </div>
    </div>
    <style>
        .bordered{
            margin-top: 0px;
        }


        div.row{
            overflow-x: auto;
        }

    </style>
@endsection
