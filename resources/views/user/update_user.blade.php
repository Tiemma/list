@extends('layouts.master')

@section('content')
    <script>
                {{--This initialises the checkbox fields and sets their values in the hidden text field--}}
                {{--Do not remove from here as the function has to be defined before it is called --}}
        var j = 0;
        function check(i){
            j = i;
            if(document.getElementById('work'+i).checked) {
                value = "on";
                $('#hideifticked' + i).hide();
                document.getElementById('label'+i).style.display = 'block';
                $('#hideifticked' + i + ' select').removeAttr('required');
                document.getElementById('unchecked' + i).value = value;
            }else {
                value = "off";
                $('#hideifticked' + i).show();
                document.getElementById('label'+i).style.display = 'none';
                $('#hideifticked' + i + ' select').prop('required', 'true');
                document.getElementById('unchecked' + i).value = value;
            }
        }
    </script>
    <form action="{{ url('/update') }}" method="POST" enctype="multipart/form-data" onsubmit="chips()">
        {{csrf_field()}}
        {{--This section is for creating arrays from strings stored in the db and to also check whether it even exists in the first place--}}

        {{--Check whether there are any schools in the db--}}
        @if(count($user_school) > 0)
            <?php $school_name = $user_school->name ?>
        @else
            <?php $school_name = '' ?>
        @endif


        @if(isset($_COOKIE['success']))
            <div class="center-align" id="success" style="width: 70%; margin-left: 15%;">
                <div class="card-panel col s4 push-s4" style="background-color: rgba(33, 150, 243, 0.7); border: 4px solid #4f9acc; border-radius: 10px;">
                    <span id="x" style="font-size: 200%;position: relative;margin-top: -2%;cursor: pointer;font-weight: 100; float: right;" onclick="$('#success').hide()">x</span>
                    <span style="color: white">Your data was successfully updated</span>
                </div>
            </div>
            <script>
                setTimeout(function(){ $('#success').fadeOut(2000); }, 2000)
            </script>
        @endif

        <section id="main-details">
            <div class="container">
                <div class="details">
                    <div>
                        <h2 class="grey-text lighten-5">Update Profile</h2>
                        <div class="divider"></div>
                    </div>
                    <div class="divider"></div>

                    <div class="col s12 m4 hide-on-med-and-up">
                        <br />
                        {{-- Save the user's profile picture url in a variable named img_source--}}
                        @if(strpos(Auth::user()->profile_pics_url, 'http'))
                            <?php $img_source = Auth::user()->profile_pics_url;?>
                        @else
                            <?php $img_source = asset(Auth::user()->profile_pics_url); ?>
                        @endif

                        <div class="update-pic-container">
                            <img src="{{$img_source}}" class="file_div responsive-img right-align circle logo">
                        </div>

                        {{--This is the hidden file tag that later opens as the popup--}}
                        <input accept=".jpeg, .jpg, .png" type="file" class="file"  style="display: none;" name="logo" >


                        <!--This sets whether the button will remove or upload another picture-->
                        <div class="center-align">
                            <br>
                            @if($user->profile_pics_url == NULL)
                                <button type="button" class="btn-flat upload_button col-s4 push-s4 center-align waves-effect upload_button center-align black-text"> Select Image </button>
                                <button type="button" class="col-s4 remove_button btn-flat waves-effect black-text center-align picture none push-s4" onclick="remove_button()">Remove</button>
                            @else
                                <button type="button" class="btn-flat upload_button col-s4 push-s4 center-align none waves-effect upload_button center-align black-text">Change</button>
                                <button type="button" class="col-s4 remove_button btn-flat waves-effect black-text center-align picture push-s4" onclick="remove_button()">Remove</button>
                            @endif
                        </div>


                        {{--Logo is just the name for the user's profile picture--}}
                        @if($errors->first('logo'))
                            <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('logo')}}</div>
                        @endif
                        <br />
                        <input type="hidden" value="" name="remove" class="remove" />
                        <div class="divider"></div>
                    </div>

                    <div class="row">
                        <div class="col s12 m8">
                            <div class="row">
                                <div class="input-field col s12">
                                    <h5 class="center-align">
                    <span class="col s4">
                      Names
                    </span>
                                        <input id="name" placeholder="Enter your name" name="name" value="{{$user['first_name'].' '.$user['last_name']}}" type="text" class="validate col s8 " required style=" width: 50%; height: 1rem;"></h5>
                                </div>
                                @if($errors->first('name'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('name')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <h5 class="center-align">
                                        <span class="col s4">Email Address</span>
                                        <input id="email" name="email" placeholder="Enter your email" required type="email" class="validate col s8" value="{{$user['email']}}"  style=" width: 50%; height: 1rem;">
                                    </h5>
                                </div>
                                @if($errors->first('email'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('email')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <span class="col s4" style="float: left">Phone Number</span>
                                        <div class="col s7">
                                            <select class="browser-default col s3 left-align select border">
                                                <option value="234">+234</option>
                                            </select>
                                            <input id="phone" required placeholder="Enter your phone number" value="{{$user['phone_number']}}" name="phone_number" type="number" value="{{$user['phone_number']}}" class="validate col s10" style=" width: 60%; height: 1rem;">
                                        </div>
                                    </h5>
                                </div>
                                @if($errors->first('phone_number'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('phone_number')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <span class="col s3" style="float: left;">Gender</span>
                                        <div class="col s8">
                                            <select class="browser-default col s3 push-s1 left-align select" style="width: 80%" name="gender" required>
                                                @if($user['sex'] == NULL)
                                                    <option disabled value="" selected>Gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="0">Female</option>
                                                @else
                                                    @if($user['sex'] == 1)
                                                        <option disabled value="">Gender</option>
                                                        <option selected value="1">Male</option>
                                                        <option value="0">Female</option>

                                                    @else
                                                        <option disabled  value="">Gender</option>
                                                        <option  value="1">Male</option>
                                                        <option selected value="0">Female</option>
                                                    @endif
                                                @endif
                                            </select>
                                        </div>
                                    </h5>
                                </div>
                                @if($errors->first('gender'))
                                    <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('gender')}}</div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <h5 class="center-align"><span class="col s4">Date of Birth&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        @if($user['dob'] != NULL)
                                            <select class="browser-default col s2 left-align select" required name="day">
                                                <option disabled selected value="">Day</option>
                                                <?php $dob = explode(' ', $user['dob']) ?>
                                                @for($i=1;$i < 32; $i++)
                                                    @if($i == $dob[0])
                                                        <option selected value="{{$i}}">{{$i}}</option>
                                                    @else
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endif
                                                @endfor
                                            </select>

                                            <select class="browser-default col s2 left-align select" required name="month">
                                                @for($i=1;$i < count($months); ++$i)
                                                    @if($i == $dob[1])
                                                        <option selected value="{{$i}}">{{$months[$i]}}</option>
                                                    @else
                                                        <option value="{{$i}}">{{$months[$i]}}</option>
                                                    @endif
                                                @endfor
                                            </select>

                                            <select class="browser-default col s2 left-align select"  required name="year">
                                                <option disabled selected value="">Year</option>
                                                @for($i=1980;$i <= date("Y"); $i++)
                                                    @if($i == $dob[2])
                                                        <option selected value="{{$i}}">{{$i}}</option>
                                                    @else
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endif
                                                @endfor
                                            </select>

                                        @else

                                            <select class="browser-default col s2 left-align select" required name="day">
                                                <option disabled selected value="">Day</option>
                                                @for($i=1;$i < 32; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>

                                            <select class="browser-default col s2 left-align select" required name="month">
                                                <option disabled selected value="">Month</option>
                                                @for($i=1;$i < count($months); ++$i)
                                                    <option value="{{$i}}">{{$months[$i]}}</option>
                                                @endfor
                                            </select>

                                            <select class="browser-default col s2 left-align select"  required name="year">
                                                <option disabled selected value="">Year</option>
                                                @for($i=1980;$i <= date("Y"); $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        @endif
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12" >
                                    <h5 class="center-align">
                                        <span class="col s4" style="float: left;">School Attended</span>
                                        <div class="col s8">
                                            <select class="browser-default col s9 left-align select" name="school">
                                                <option disabled selected>Choose your school</option>
                                                @foreach($schools as $school)
                                                    @if($school_name == $school)
                                                        <option selected value="{{$school}}">{{$school}}</option>
                                                    @else
                                                        <option value="{{$school}}">{{$school}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </h5>
                                </div>
                            </div>

                        </div>

                        <div class="col s12 m4 hide-on-small-only">
                            <br />

                            <div class="update-pic-container">
                                <img src="{{$img_source}}" class="file_div responsive-img center-align circle logo">
                            </div>

                            <!--This sets whether the button will remove or upload another picture-->
                            <div class="center-align input-field" >
                                <br>
                                @if($user->profile_pics_url == NULL)
                                    <button type="button" class="btn-flat upload_button col-s4 push-s4 center-align waves-effect upload_button center-align black-text"> Select Image </button>
                                    <button type="button" class="col-s4 remove_button btn-flat waves-effect black-text center-align picture none push-s4" onclick="remove_button()">Remove</button>
                                @else
                                    <button type="button" class="btn-flat upload_button col-s4 push-s4 center-align  waves-effect upload_button center-align black-text">Change</button>
                                    <button type="button" class="col-s4 remove_button btn-flat waves-effect black-text center-align picture push-s4" onclick="remove_button()">Remove</button>
                                @endif
                            </div>


                            {{--Logo is just the name for the user's profile picture--}}
                            @if($errors->first('logo'))
                                <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('logo')}}</div>
                            @endif

                        </div>
                        <button class="btn btn-sucess waves-effect waves-ripple right" style="margin:10% 10% 0 0 ">Save</button>

                    </div>

                </div>
            </div>
        </section>

        <section id="user-details">
            <div class="container">
                <div class="details">
                    <div>
                        <h2 class="grey-text lighten-5">Personal User Details</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12 m8">
                            <div class="row">
                                <div class="row">
                                    <div class="input-field col s12" >
                                        <h5 class="center-align">
                                            <span class="col s4" style="float: left">Brief Profile</span>
                                            <textarea id="profile" placeholder="Brief Profile" name="profile"  class="validate col s4 materialize-textarea" style=" width: 50%; height: 1rem">@if($user['brief_profile'] != NULL){{$user['brief_profile']}}@endif</textarea>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            @if($errors->first('profile'))
                                <div class="card-panel red lighten-3 col s4 push-s4">{{$errors->first('profile')}}</div>
                            @endif



                            <?php
                            $social_channel_array = array();
                            try{
                                $social_channel_array = unserialize($user->social_channel);
                            }catch(Exception $e){}
                            ?>
                            <div id="channel">
                                <div class="row">
                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <span class="col s4 move-left">Facebook</span>
                                                <input placeholder="Username e.g ...com/username" value="{{$social_channel_array['facebook']}}" name="facebook" type="text" class="validate col s2" style=" width: 50%; height: 1rem;  margin-left: 3% !important">
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <span class="col s4 move-left">LinkedIn</span>
                                                <input placeholder="Username e.g .../in/username" value="{{$social_channel_array['linkedin']}}"   name="linkedin" type="text" class="validate col s2" style=" width: 50%; height: 1rem;  margin-left: 3% !important">
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row">
                                        <div class="input-field col s12" >
                                            <h5 class="center-align">
                                                <span class="col s4 move-left">Twitter</span>
                                                <input placeholder="Handle e.g ...com/username"  value="{{$social_channel_array['twitter']}}" name="twitter" type="text" class="validate col s2" style=" width: 50%; height: 1rem; margin-left: 3% !important">
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        </div>
                        <button class="btn btn-sucess waves-effect waves-ripple right" style="margin:0 10% 0 0 ">Save</button>
                    </div>
                </div>

            </div>
            </div>
        </section>

        <section id="details-2">
            <div class="container">
                <div class="details">
                    <div>
                        <h2 class="grey-text lighten-5">Skills and Experience</h2>
                    </div>
                    <div class="divider"></div>


                    <div class="row">
                        <div class="col s12 m12">
                            <div class="col s8 m8">
                                <div id="project">
                                    @if(count($projects) > 0)
                                        @foreach($projects as $project)
                                            <?php $project_name = unserialize($project->name);  ?>
                                            <?php $project_description = unserialize($project->description); ?>
                                            @foreach(array_combine($project_name, $project_description) as $name=>$description)
                                                <div class="project_remove">
                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <h5 class="center-align"><span class="col s4 m4" style="float: left">Projects worked on</span><input id="project_name" placeholder="Name of project" value="{{$name}}" name="project_name[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem"></h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <h5 class="center-align"><span class="col s4 m4" style="float: left"></span><input id="project_members" placeholder="Members of the project" value="{{null}}" name="members[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem"></h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="input-field col s12 m12" >
                                                            <h5 class="center-align">
                                                                <textarea id="project_description" placeholder="Description" name="project_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem">{{$description}}</textarea>
                                                            </h5>
                                                        </div>

                                                        <div class="col s12 m4 push-m7 push-s9">
                                                            <div class="input-field">
                                                                <button type="button" class="btn-flat waves-effect waves-light col s11 m4 pull-s4" style="margin-left: 10px; width: 90px; border: 1px solid #9e9e9e" onclick="$(this).closest('div.project_remove').remove()" style="border: 1px solid #9e9e9e">Remove</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    {{--
                                                    <button type="button" class="btn btn-success right-align col s3 push-m3 m3 hide-on-small-only" onclick="project()">Remove</button>
                                                    <button type="button" class="btn btn-success btn-small right-align col s3 push-s2 hide-on-med-and-up" onclick="project()">Remove</button>
                                                    --}}
                                                </div>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <script> $(document).ready(function(){project()}); </script>


                        <div class="col s12 m8">
                            <button type="button" class="btn btn-success right-align col s4 push-s6  hide-on-small-only" onclick="project()">Add More Projects</button>
                            <button type="button" class="btn btn-success btn-small right-align col s3 push-s4 m3 push-m8 hide-on-med-and-up" onclick="project()">Add More Projects</button>
                        </div>

                        <div class="divider"></div>

                        <div class="col s12 m12">
                            <div id="experience">
                                @if(count($experiences))
                                    @foreach($experiences as $experience)
                                        <?php $experience_name = unserialize($experience->company_name) ?>
                                        <?php $experience_role = unserialize($experience->role) ?>
                                        <?php $experience_description = unserialize($experience->description) ?>
                                        <?php $employed_status = unserialize($experience->employed_status) ?>
                                        <?php $k = 0 ?>
                                        <?php $j = 0 ?>

                                        @if($experience['start_date'] != NULL)
                                            <?php $from = explode('*', $experience['start_date']) ?>
                                        @endif

                                        @if($experience['end_date'] != NULL)
                                            <?php $to = explode('*', $experience['end_date']) ?>
                                        @endif

                                        @foreach($experience_name as $name)
                                            <?php $val = rand() ?>
                                            <div class="col s12 m8">
                                                <div class="experience_remove">
                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <h5 class="center-align">
                                    <span class="col s4 m4" style="float: left">
                                      Work Experience
                                    </span>
                                                                <input id="company_name" placeholder="Name of company" name="company_name[]" type="text" value="{{$name}}" class="company_name col s4 offset-s4" style=" width: 50%; height: 1rem">
                                                                <div class="drop-down white">
                                                                </div>
                                                            </h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="input-field col s12" >
                                                            <h5 class="center-align">
                                                                <input id="role" placeholder="Role" name="role[]" value="{{$experience_role[$k]}}" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
                                                            </h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="input-field col s12" >
                                                            <h5 class="center-align">
                                                                <textarea id="project_description" placeholder="Description" name="work_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem">{{$experience_description[$k]}}</textarea>
                                                            </h5>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div clas="col s12">
                                                            <div class="input-field col s8 offset-s4 pull-s1" >
                                                                <span class="col s1" style="font-size: 10px;margin-right: 12px;">From</span>
                                                                <select class="browser-default col s2  left-align border-bottom border select" name="from_month[]">
                                                                    <option disabled>Month</option>
                                                                    @for($i=1;$i < count($months); ++$i)
                                                                        @if($i == explode(" ", $from[$k])[0])
                                                                            <option value="{{$i}}" selected>{{$months[$i]}}</option>
                                                                        @else
                                                                            <option value="{{$i}}">{{$months[$i]}}</option>
                                                                        @endif
                                                                    @endfor
                                                                </select>
                                                                <select class="browser-default col s2 left-align border select" name="from_year[]" class="border" style="margin-right: 7px">
                                                                    <option disabled selected>Year</option>
                                                                    @for($i=1980;$i <= date("Y"); $i++)
                                                                        @if($i == explode(" ", $from[$k])[1])
                                                                            <option selected value="{{$i}}">{{$i}}</option>
                                                                        @else
                                                                            <option value="{{$i}}">{{$i}}</option>
                                                                        @endif
                                                                    @endfor
                                                                </select>
                                                                <span id="hideifticked{{$val}}">
                                      <span class="col s1" style="font-size: 10px;">To</span>
                                      <select class="browser-default col s2  left-align border select" name="to_month[]">
                                        @if(!$employed_status[$k] and isset($to[$j]))
                                              <option disabled>Month</option>
                                              @for($i=1;$i < count($months); ++$i)
                                                  @if($i == explode(" ", $to[$j])[0])
                                                      <option value="{{$i}}" selected>{{$months[$i]}}</option>
                                                  @else
                                                      <option value="{{$i}}">{{$months[$i]}}</option>
                                                  @endif
                                              @endfor
                                          @else
                                              <option disabled selected>Month</option>
                                              @for($i=1; $i < count($months); $i++)
                                                  <option value="{{$i}}">{{$months[$i]}}</option>
                                              @endfor
                                          @endif
                                      </select>
                                      <select class="browser-default col s2 left-align border select" name="to_year[]" >
                                        <option disabled selected>Year</option>
                                          @if(!$employed_status[$k] and isset($to[$j]))
                                              @for($i=1980;$i <= date("Y"); $i++)
                                                  @if($i == explode(" ", $to[$j])[1])
                                                      <option selected value="{{$i}}">{{$i}}</option>
                                                  @else
                                                      <option value="{{$i}}">{{$i}}</option>
                                                  @endif
                                              @endfor
                                              <?php ++$j ?>
                                          @else
                                              @for($i=1980;$i <= date("Y"); $i++)
                                                  <option value="{{$i}}">{{$i}}</option>
                                              @endfor
                                          @endif
                                      </select>
                                    </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <div class="col s5 offset-s2">
                                                                <input type="checkbox" @if($k == $experience->primary_work_index) {{'checked'}} @endif name="primary_work" id="primary{{$val}}" class="primary" />
                                                                <label id="label{{$val}}" for="primary{{$val}}">Primary Work Place</label>
                                                            </div>
                                                            <div class="col s5 pull-s1">
                                                                @if(!$employed_status[$k])
                                                                    <input type="checkbox" class="checkbox" id="work{{$val}}" onchange="check({{$val}})">
                                                                    <label for="work{{$val}}">Currently work here</label>
                                                                @else
                                                                    <input type="checkbox" class="checkbox" checked id="work{{$val}}" onchange="check({{$val}})">
                                                                    <label for="work{{$val}}">Currently work here</label>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m4 push-m7 push-s10">
                                                    <div class="input-field">
                                                        <button type="button" class="btn-flat waves-effect waves-light col s11 m4 pull-s4" style="margin-left: 10px; width: 90px; border: 1px solid #9e9e9e" onclick="$(this).closest('div.experience_remove').remove()" style="border: 1px solid #9e9e9e">Remove</button>
                                                    </div>
                                                </div>

                                                <input type="text" hidden id="unchecked{{$val}}" value="off" name="work_status[]">
                                                <script>$(document).ready(function(){check({{$val}})});</script>
                                            </div>
                                            <?php ++$k ?>
                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <script>  $(document).ready(function(){experience(); }); </script>
                        <div class="row" >
                            <div class="input-field col s12 m8" >
                                <button type="button" style="margin-top: 5%" class="btn btnsuccess right-align col s3 m4 push-s4 push-m6" onclick="experience()">Add More Experience</button>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="input-field col s12 m8">
                            <h5 class="center-align"><span class="col s4" class="col s4" style="float: left">Skills &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <div class="chips chips-initial col s4" style="width: 60%;">
                                </div>
                            </h5>
                        </div>
                    </div>

                    <div id="skills"></div>
                    <div id="work_index"></div>
                    <button class="btn btn-success btn-group-justified col s8" style="margin-left: 16.5%; width: 66.7% !important">Submit</button>
                </div>
            </div>
        </section>
    </form>
    <script>
        i = 1000;
        function experience() {
            var dummy = `
          <div class="experience_remove">
          <div class="row">
          <div class="col s12 m8 company_body">
          <div class="input-field col s12">
          <h5 class="center-align">
          <span class="col s4 m4" style="float: left">
          Work Experience
          </span>
          <div style="position: relative">
          <input id="company_name" placeholder="Name of company" name="company_name[]" type="text" required class="validate col s4  company_name offset-s4" style=" width: 50%; height: 1rem">
          <span class="drop-down"></span>
          </div>
          </h5>
          </div>

          <div class="row">
          <div class="input-field col s12">
          <h5 class="center-align">
          <input id="role" placeholder="Role" name="role[]" type="text"  required class="validate col s4 offset-s4" style=" width: 50%; height: 1rem">
          </h5>
          </div>
          </div>

          <div class="row">
          <div class="input-field col s12" >
          <h5 class="center-align">
          <textarea id="project_description" placeholder="Description" required name="work_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem"></textarea>
          </h5>
          </div>
          </div>


          <div class="row">
          <div clas="col s12">
          <div class="input-field col s8 offset-s4 pull-s1" >
          <span class="col s1" style="font-size: 10px;margin-right: 12px;">From</span>
          <select class="browser-default col s2  left-align border-bottom border select" required name="from_month[]">
          <option disabled selected value="">Month</option>
          @for($i = 1; $i < count($months); $i++)
                    <option value="{{$i}}">{{$months[$i]}}</option>
          @endfor
                    </select>
                    <select class="browser-default col s2 left-align border select" name="from_year[]" required class="border" style="margin-right: 7px">
                    <option disabled selected value="">Year</option>
                    @for($i=1980;$i <= date("Y"); $i++)
                    <option value="{{$i}}">{{$i}}</option>
          @endfor
                    </select>
                    <span id="hideifticked`+i+`">
          <span class="col s1" style="font-size: 10px;">To</span>
          <select class="browser-default col s2 left-align border select" required name="to_month[]">
          <option disabled selected value="">Month</option>
          @for($i=1; $i < count($months); $i++)
                    <option value="{{$i}}">{{$months[$i]}}</option>
          @endfor
                    </select>
                    <select class="browser-default col s2 left-align border select" required name="to_year[]" >
                    <option disabled selected value="">Year</option>
                    @for($i=1980;$i <= date("Y"); $i++)
                    <option value="{{$i}}">{{$i}}</option>
          @endfor
                    </select>
                    </span>
                    </div>
                    </div>
                    </div>

                    <div class="row">
                    <div class="input-field col s12">
                    <div class="col s5 offset-s2">
                    <input type="checkbox" name="primary_work" id="primary`+i+`" class="primary" />
          <label id="label`+i+`"  for="primary`+i+`">Primary Work Place</label>
          </div>
          <div class="col s5 pull-s1">

          <input type="checkbox" class="checkbox" id="work`+i+`" onchange="check(`+i+`)">
          <label  for="work`+i+`">Currently work here</label>
          </div>
          </div>
          </div>
          <div class="col s12 m4 push-m7 push-s10">
          <div class="input-field">
          <button type="button" class="btn-flat waves-effect waves-light col s11 m4 pull-s4" style="margin-left: 10px; width: 90px; border: 1px solid #9e9e9e" onclick="$(this).closest('div.experience_remove').remove()" style="border: 1px solid #9e9e9e">Remove</button>
          </div>
          </div>
          </div>
          <div class="col s12 m4 company_names"></div>


          <input type="text" hidden id="unchecked`+i+`" value="off" name="work_status[]">
          <script>check(`+i+`)<//script>
          </div>`
            document.getElementById('experience').insertAdjacentHTML( 'beforeend', dummy );
            check(i);
            company_search();
            ++i;
        }

        function remove_button() {
            $('.logo').attr('src', '{{asset('images/techpoint.png')}}');
            $('.remove_button').hide();
            $('.upload_button').show();
            $('.upload_button').html('Select Image');
            $('.file').val('');
            $('.remove').val('removed');
        }
    </script>

    <script>

        $(document).ready(function(){

            {{--This sets the user' s skillls in chips--}}
            $('.chips-initial').material_chip({
                placeholder: 'Enter a tag',
                secondaryPlaceholder: 'Press enter',
                <?php
                    isset($skills) ? $skill = explode(',', $skills->name) : $skill = array();;
                    ?>
                        @if(count($skill) > 0)
                data: [
                        @foreach($skill as $user_skill)
                        @if(!empty(trim($user_skill)))
                    {tag: '{{$user_skill}}'},
                    @endif
                    @endforeach
                ],
                @endif

            });

            $('#work_index').html("<input type='hidden' value='"+$('input[type=checkbox]').index($('input[name=primary_work]:checked')) / 2 +"' name='index'/>");

        });

        $('input').css('visibility', 'visible');

        $('.primary').click(function(){
            setTimeout('', 1000);
            $('.primary').not(this).prop('checked', false);
            $('#work_index').html("<input type='hidden' value='"+$('input[type=checkbox]').index($('input[name=primary_work]:checked')) / 2 +"' name='index'/>");
        });

    </script>

    <style>
        input:not([type]), input[type=text], input[type=password], input[type=email], input[type=url], input[type=time], input[type=date], input[type=datetime], input[type=datetime-local], input[type=tel], input[type=number], input[type=search], textarea.materialize-textarea {
            margin: 0px;
        }

        section .container{
            box-shadow: 0 1px 5px 0 rgba(0,0,0,0.3), 0 2px 2px 0 rgba(0,0,0,0.2) !important
        }

        button{
            font-size: 0.7em;
            width: auto !important;
        }
    </style>

@endsection
