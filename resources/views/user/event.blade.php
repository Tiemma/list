@extends('layouts.master')

@section('content')
    <section id="profile-header">
        <!--<div style="height: 45vh">-->
        <div class="profile-header">
            <div class="black-layer">
                <div class="row">
                    <div class="col s12 m3"></div>
                    <div class="col s12 m6">
                        <div class="profile-title">
                            <h3 class="center-align white-text">{{$event->name}}</h3>
                        </div>
                    </div>
                    <div class="col s12 m3"></div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">

            <div class="col s12 hide-on-med-and-up">
                <section>
                    <div class="bordered" style="padding: 0; margin: 0">
                        <div style="padding: 5%">
                            <div class="row">
                                <div class="col s12">
                                    <div class="col s3">
                                        <img src="{{asset('/images/location.png')}}" class="events-icon"/>
                                    </div>
                                    <div class="col s9"><h5>{{$event->address}}</h5></div>
                                </div>
                            </div>
                            <div style="margin: 5%"></div>
                            <div class="row">
                                <div class="col s12">
                                    <div class="col s3">
                                        <img src="{{asset('/images/calendar.png')}}" class="events-icon"/>
                                    </div>
                                    <div class="col s9"><h5>{{$event->event_date}}</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <a href="{{asset($event->registration_link)}}">
                    <button type="button" class="btn btn-large no-padding" style="width: 100%">Register</button>
                </a>
            </div>

            <div class="col s12 hide-on-med-and-up" style="margin-top: 10%"></div>

            <div class="col s8 content">
                <section>
                    <div class="bordered" style="margin: 0">
                        <div>
                            <h2 class="blue-text" style="margin-left: 5%">Description</h2>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col s11" style="margin: 5%">{{$event->brief_profile}}</div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col s4 hide-on-small-only">
                <section>
                    <div class="bordered" style="padding: 0; margin: 0">
                        <div style="padding: 5%">
                            <div class="row">
                                <div class="col s12">
                                    <div class="col s3">
                                        <img src="{{asset('/images/location.png')}}" class="events-icon tooltipped" data-position="bottom" data-delay="50" data-tooltip="Location"/>
                                    </div>
                                    <div class="col s9"><h5>{{$event->address}}</h5></div>
                                </div>
                            </div>
                            <div style="margin: 5%"></div>
                            <div class="row">
                                <div class="col s12">
                                    <div class="col s3">
                                        <img src="{{asset('/images/calendar.png')}}" class="events-icon tooltipped" data-position="bottom" data-delay="50" data-tooltip="Event Date"/>
                                    </div>
                                    <div class="col s9"><h5>{{$event->event_date}}</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div style="padding-top: 10%"></div>
                <a href="{{asset($event->registration_link)}}">
                    <button type="button" class="btn btn-large no-padding" style="width: 100%">Register</button>
                </a>

            </div>
        </div>
    </div>


    <style>
        .profile-header{
            background: url("{{asset($event->logo_url)}}");
            background-size: 100% 100%;
        }

        .profile-title {
            margin-top: 20%;
        }

        .content{

        }

        h5{
            margin: 0px;
        }

        @media only screen and (max-width: 800px){
            .content{
                width: 100% !important;
            }
        }
    </style>

@endsection
