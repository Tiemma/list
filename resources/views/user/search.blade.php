@extends('layouts.master')

{{--
<!--- Navbar and search box - - ->
@section('header')
    <nav>
        <a href="#" data-activates="slide-out" class="button-collapse hide-on-med-and-up"><i class="material-icons">menu</i></a>
        <div class="container">
            <div class="row">
                <div class="col s12 m3 "><img src="images/techpointlogo.png" style="max-width: 80%"></div>
                <div class="col s12 m8 search-container">
                    <form method="get" action="search">
                        <input class="black-text search-box" value="{{$search}}" name="search" type="search">
                        <div class="search-board test row white black-text lighten-4">
                            <span class="col s4 filter grey-text">Type</span>
                            <span class="col s4 filter grey-text">Category</span>
                            <span class="col s4 filter grey-text">Location</span>
                            <div class="input-field col s4 ">
                                <select class="browser-default" name="type" id="type">
                                    <option value="user" selected>User</option>
                                    <option value="investor">Investor</option>
                                    <option value="startup">Startup</option>
                                    <option value="company">Company</option>
                                    <option value="project">Project</option>
                                </select>
                            </div>
                            <div class="col s4 m4">
                                <select class="browser-default" id="category">
                                    <option value="" disabled selected>choose category</option>
                                    <option value="power">Power</option>
                                    <option value="agric">Agric</option>
                                </select>
                            </div>
                            <div class="input-field col s4">
                                <select class="browser-default" id="location">
                                    <option value="lagos" disabled selected>choose location</option>
                                    <option value="lagos" selected>Lagos</option>
                                    <option value="ibadan">Ibadan</option>
                                </select>
                            </div>
                            <button class="search-btn btn blue white-text" id="search">Search</button>

                            <span class="col s12 search-close"><a href=" "> <i class="material-icons small">keyboard_arrow_up</i></a></span>
                        </div>
                    </form>


                </div>

            </div>
        </div>

    </nav>

@endsection
<!- - Search View Logic-->
--}}
@section('content')
    <?php
    $live_url = getenv('REMOTE_ADDR');
    ?>
    <section id="list-search" class="white">
        <div class="container">
            <div class="row">
                @if(!empty($error))
                    <div class="col s12 m12">
                        <div class="search-error">
                         <span class="col s12 m12">
                            <img class="error-emoji" src="{{asset('images/sad.png')}}">
                        </span>
                        <h4 class=" grey-text center-align">
                            Sorry, we could not find anything related to <b>{{$search}}</b>
                        </h4>
                        </div>
                    </div>
                @endif
                @if(isset($users))
                    @foreach($users as $user)
                        <div class="col s12 m12">
                            <div class="search-result">
                                <a href="user/{{$user->id}}" ><p>{{$user->first_name}} {{$user->last_name}}</p></a>
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(isset($companies))
                    @foreach($companies as $company)
                        <div class="col s12 m12">
                            <div class="search-result">
                                <div class="col s4 m4">
                                    <p>{{$company->name}}</p>
                                </div>
                                <div class="col s6 m4">
                                    <p>{{$company->brief_profile}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

@endsection
