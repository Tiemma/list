@extends('layouts.app')


@section('content')
    <title>Techpointlist-{{$user->first_name." ".$user->last_name}}</title>
    <section id="profile-header">
        <div style="height: 75vh">
            <div class="profile-header">
                <div class="black-layer">
                    <div class="row">
                        <div class="col s12 m3">
                            <div class="pic-container">
                                <img src="{{$user->profile_pics_url}}" class="responsive-img p-pic">
                            </div>
                        </div>
                        <div class="col s12 m6">
                            <div class="profile-title">
                                <h3 class="center-align white-text">{{$user->first_name." ".$user->last_name}}</h3>
                                @if(count($experience) > 0)
                                    @foreach($experience as $work_experience)
                                        @if($work_experience->employed_status == 1)
                                            <h3 class="center-align white-text">{{$work_experience->role}} at {{$work_experience->company_name}}</h3>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col s12 m3">
                            <div class="row social-icons">
                                <div class="col s4 m4">
                                    <img src="images/facebook-logo.png" alt="facebook icon">
                                </div>
                                <div class="col s4 m4">
                                    <img src="images/twitter-social.png" alt="twitter icon">
                                </div>
                                <div class="col s4 m4">
                                    <img src="images/linkedin-logo.png" alt="linkedin icon">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--- Profile Header Ends -->

    <secition id="About">

        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">About</h2>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <p>{{$user->brief_profile}}</p>
                </div>
            </div>
        </div>
        </div>
    </secition>
    <!--- End of overview -->

    <section id="work-experience">
        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">Work Experience</h2>
                </div>
                @if(count($experience) > 0)
                    @foreach($experience as $work_experience)
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="col s4"><h3>Company</h3></div>
                                    <div class="col s8"><p>{{$work_experience->company_name}}</p></div>
                                </div>
                                <div class="row">
                                    <div class="col s4"><h3>Period</h3></div>
                                    <div class="col s8">
                                        <p>
                                            <?php
                                            $start_date = explode(' ', $work_experience->start_date);
                                            $end_date = explode(' ', $work_experience->end_date);
                                            ?>
                                            @if($work_experience->employed_status == 1)
                                                {{$months[$start_date[0]].' '.$start_date[1]}} till date
                                            @else
                                                {{$months[$start_date[0]].' '.$start_date[1]}} till {{$months[$end_date[0]].' '.$end_date[1]}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4"><h3>Role</h3></div>
                                    <div class="col s8"><p>{{$work_experience->role}}</p></div>
                                </div>
                                <div class="row">
                                    <div class="col s4"><h3>Work Description</h3></div>
                                    <div class="col s8">
                                        <p>{{$work_experience->description}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col m3 hide-on-small-only">

                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="divider"></div>
                    <h3><i class="material-icons">prefix</i>No work experience found</h3>
                @endif
            </div>
        </div>
    </section>
    <section id="skills">
        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">Skills</h2>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <div class="span-wrapper">
                        <?php $skill = explode('|', $skills->name) ?>
                        @foreach($skill as $skills)
                            <span class="col s3 m2" >{{$skills}}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection