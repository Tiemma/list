@extends('layouts.master')

@section('content')
    <section id="profile-header">
        <!--<div style="height: 45vh">-->
        <div class="profile-header">
            <div class="black-layer">
                <div class="row">
                    <div class="col s12 m3">
                        <div class="pic-container">
                            @if(strpos(Auth::user()->profile_pics_url, 'http'))
                                <img src="{{Auth::user()->profile_pics_url}}"  class="responsive-img p-pic circle" style="height: 100%" />
                            @else
                                <img src="{{asset(Auth::user()->profile_pics_url)}}"  alt="profile pic" class="responsive-img p-pic circle" style="height: 100%" />
                            @endif

                        </div>
                    </div>
                    <div class="col s12 m6">
                        <div class="profile-title">
                            <h3 class="center-align white-text">{{$user->first_name." ".$user->last_name}} @if(isset($user->confirmation_status)) &nbsp;<img class="check_mark" src="{{asset('/images/checked.png')}}" /> @endif</h3>
                            @if(count($experience) > 0)
                                @foreach($experience as $experiences)
                                    <?php  $i = 0 ?>
                                    <?php $name = unserialize($experiences->company_name) ?>
                                    <?php $role = unserialize($experiences->role) ?>
                                    <?php $employed_status = unserialize($experiences->employed_status); ?>
                                    @if(count($name) > 0)
                                        @foreach($name as $company_name){{--Just using this to increment i until it matches the index--}}
                                        @if($employed_status[$i] == 1 and $experiences->primary_work_index == $i)
                                            <h3 class="center-align white-text">{{$role[$i]}} at {{$company_name}}</h3>
                                            <?php break; ?>
                                        @endif
                                        <?php ++$i ?>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="col s12 m3">
                        @if($user->id == Auth::user()->id)
                            <a href="{{url('/update')}}"><button class="btn btn-large waves-effect grey grey-text lighten-3 hide-on-small-only"> Edit </button></a>
                            <a href="{{url('/update')}}"><button class="btn waves-effect grey grey-text lighten-3 black-text hide-on-med-and-up"> Edit </button></a>
                        @endif
                        <div class="row social-icons">
                            <?php
                            $social_channel_array = unserialize($user->social_channel);
                            ?>
                            @if(isset($social_channel_array['facebook']))
                                <div class="col s3 m3">
                                    <a target="_blank" href="http://facebook.com/{{$social_channel_array['facebook']}}">
                                        <img src="{{asset('images/facebook-logo.png')}}" alt="facebook icon" class="circle">
                                    </a>
                                </div>
                            @endif
                            @if(isset($social_channel_array['twitter']))
                                <div class="col s3 m3">
                                    <a target="_blank" href="http://twitter.com/{{$social_channel_array['twitter']}}">
                                        <img src="{{asset('images/twitter-social.png')}}" alt="twitter icon" class="circle">
                                    </a>
                                </div>
                            @endif
                            @if(isset($social_channel_array['linkedin']))
                                <div class="col s3 m3">
                                    <a target="_blank" href="http://linkedin.com/in/{{$social_channel_array['linkedin']}}">
                                        <img src="{{asset('images/linkedin-logo.png')}}" alt="instagram icon" class="circle">
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--</div>-->
    </section>
    <!--- Profile Header Ends -->
    <section id="About">

        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">About</h2>
                </div>
                <div class="divider"></div>
                <div class="row">
                    @if ($user->brief_profile != NULL)
                        <p>
                            {{$user->brief_profile}}
                        </p>
                    @else
                        <p class="center-align">No description found</p>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </section>
    <!--- End of overview -->

    <section id="founded">
        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">Startup(s) Founded</h2>
                </div>
                <div class="divider"></div>
                @if(count($startup) > 0)
                    @foreach($startup as $startups)
                        <div class="row">
                            <div class="col s3 m4">
                                <div class="ent-thumbnail">
                                    @if(isset($startups->logo_url))
                                        <img src="{{asset($startups->logo_url)}}" class="responsive-img">
                                    @else
                                        <img src="{{asset('images/techpoint.png')}}" class="responsive-img">
                                    @endif
                                </div>
                            </div>
                            <div class="col s8 push-s1 m8">
                                <span><h3><a href="{{asset('/startup/'.$startups->id)}}"><strong>{{$startups->startup_name}}</strong></a></h3></span>
                                <div>
                                    <p>{{$startups->startup_brief}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                    @endforeach
                @else
                    <p class="center-align">No startups found</p>
                @endif
            </div>
        </div>
    </section>

    <!--

  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
    </li>
  </ul>


    -->


    <section id="project">
        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">Projects worked on</h2>
                    <div class="divider"></div>
                </div>
                @if(isset($projects[0]->name))
                    @if(count(unserialize($projects[0]->name))))
                    <ul class="collapsible" data-collapsible="accordion">
                        @foreach($projects as $project)
                            @foreach(array_combine(unserialize($project->name), unserialize($project->description)) as $project_name=>$project_description)
                                <li>
                                    <div class="collapsible-header"><i class="material-icons">group</i><h3 class="blue-text">{{$project_name}}</h3></div>
                                    <div class="collapsible-body">
                                        <div class="col s12">
                                            <div class="row">
                                                <div class="col s4 blue-text"><h3>Name</h3></div>
                                                <div class="col s8"><p>{{$project_name}}</p></div>
                                            </div>
                                            <div class="row" id="project">
                                                <div class="col s4 blue-text"><h3>Description</h3></div>
                                                <div class="col s8"><p>{{$project_description}}</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                    @endif
                @else
                    <p class="center-align">No projects found</p>
                @endif
            </div>
        </div>
    </section>

    <section id="work-experience">
        <div class="container">
            <div class="bordered ">
                <div>
                    <h2 class="blue-text">Work Experience</h2>
                    <div class="divider"></div>
                </div>

                @if(isset($experience[0]->name))
                    <ul class="collapsible" data-collapsible="accordion">
                        @foreach($experience as $experiences)
                            <?php $name = unserialize($experiences->company_name) ?>
                            <?php $description = unserialize($experiences->description) ?>
                            <?php $role = unserialize($experiences->role) ?>
                            <?php
                            $arr = array();
                            $employed_status = unserialize($experiences->employed_status);
                            foreach($employed_status as $status){
                                $arr[] = $status;
                                arsort($arr);
                            }
                            ?>

                            @foreach($arr as $index=>$work_status)
                                <li>
                                    <div class="collapsible-header"><i class="material-icons">work</i><h3 class="blue-text">{{$name[$index]}}</h3></div>
                                    <div class="collapsible-body">
                                        <div class="row">
                                            <div class="col s4"><h3>Company</h3></div>
                                            <div class="col s8"><p>{{$name[$index]}}</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col s4"><h3>Period</h3></div>
                                            <div class="col s8">
                                                <p>
                                                    @if($experiences->start_date != NULL)
                                                        <?php $from = explode('*', $experiences->start_date) ?>
                                                    @endif

                                                    @if($experiences->end_date != NULL)
                                                        <?php $to = explode('*', $experiences->end_date) ?>
                                                    @endif

                                                    @if($work_status == 1)
                                                        {{$months[explode(" ", $from[$index])[0]].' '.explode(" ", $from[$index])[1]}} till date
                                                    @else
                                                        {{$months[explode(" ", $from[$index])[0]].' '.explode(" ", $from[$index])[1]}}  till {{$months[explode(" ", $to[$index])[0]].' '.explode(" ", $to[$index])[1]}}
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s4"><h3>Role</h3></div>
                                            <div class="col s8"><p>{{$role[$index]}}</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col s4"><h3>Work Description</h3></div>
                                            <div class="col s8">
                                                <p>@if(isset($description[$index]))
                                                    {{$description[$index]}}
                                                @else
                                                    <p class="center-align">No description available</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                @else
                    <div class="divider"></div>
                    <p class="center-align">No work experience found</p>
                @endif
            </div>
        </div>
    </section>
    <section id="skills">
        <div class="container">
            <div class="bordered">
                <div>
                    <h2 class="blue-text">Skills</h2>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <div class="span-wrapper">
                        <?php

                        isset($skills) ? $skill = explode(',', $skills->name) : $skill = array();
                        ?>
                        @if(count($skill) > 0 and !empty(trim($skill[0])))
                            @foreach($skill as $user_skill)
                                @if(trim($user_skill) != '' and isset($user_skill))
                                    <div class="collection col s2" style="margin-left: 1%; width: 15.56%">
                                        <a href="#" class="collection-item">{{$user_skill}}</a>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <p class="center-align">No skills found</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>

        .collection-item:hover{
            background-color: transparent !important;
        }

        .collection:hover{
            background-color: #e0e0e0;
        }

        .collection, .collection-item{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        .collection-item{
            text-align: center;
            color: #4f9acc !important;
        }

        .collapsible-body p{
            padding: 0;
        }

        .collapsible{
            box-shadow: none;
        }

        #work-experience p, #project p{
            margin: 0;
            margin-top: 3%;
        }

        .material-icons{
            margin-top: -1%;
        }

    </style>
@endsection
