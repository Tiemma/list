@extends('layouts.master')

@section('content')
    <form method="POST" action="{{asset('/reviews')}}">
        {{csrf_field()}}
        <section id="profile-header">
            <!--<div style="height: 75vh">-->
            <div class="profile-header">
                <div class="black-layer">
                    <div class="row">
                        <div class="col s12 m4 pic-center">
                            <div class="company-logo">
                                <img src="<?php echo isset($startup->logo_url) ? asset($startup->logo_url) : asset('images/techpoint.png') ?>" alt="company logo">
                            </div>
                            @foreach($users as $user)
                                <div class="founder-container">
                                    <img src="<?php if(strpos($user->profile_pics_url, 'http')){ echo $user->profile_pics_url; }else{echo asset($user->profile_pics_url);} ?>" class="responsive-images ent-pic">
                                </div>
                            @endforeach
                        </div>
                        <?php $category = array('Health', 'Finance', 'Business', 'Telecoms', 'Infrastructure') ?>
                        <div class="col s12 m4">
                            <div class="startup-title">
                                <h3 class="center-align white-text" >{{$startup->startup_name}} @if(isset($startup->confirmation_status)) &nbsp;<img class="check_mark" src="{{asset('/images/checked.png')}}" /> @endif</h3>
                                <h3 class="center-align white-text">{{$category[isset($startup->category) ? $startup->category : 0]}}</h3>
                                <h3 class="center-align white-text" style="text-decoration: underline"><a href="http://{{$startup->website}}" target="_blank">{{$startup->website}}</a></h3>
                            </div>
                        </div>
                        <div class="col s12 m4 startup">
                            <div class="row social-icons">
                                @if(!empty($startup->facebook))
                                    <div class="col s4 m4">
                                        <a target="_blank" href="http://facebook.com/{{$startup->facebook}}">
                                            <img src="{{asset('images/facebook-logo.png')}}" alt="facebook icon" class="circle">
                                        </a>
                                    </div>
                                @endif

                                @if(!empty($startup->twitter))
                                    <div class="col s4 m4">
                                        <a target="_blank" href="http://twitter.com/{{$startup->twitter}}">
                                            <img src="{{asset('images/twitter-social.png')}}" alt="twitter icon" class="circle">
                                        </a>
                                    </div>
                                @endif

                                @if(!empty($startup->linkedin))
                                    <div class="col s4 m4">
                                        <a target="_blank" href="http://linkedin.com/in/{{$startup->linkedin}}">
                                            <img src="{{asset('images/linkedin-logo.png')}}" alt="linkedin icon" class="circle">
                                        </a>
                                    </div>
                                @endif
                            </div>

                            <div class="rating-column">
                                <span>Rating:</span>
                                <span class="rating">
                                    @for($i = 5; $i >= 1; --$i)
                                        <span id="{{$i}}" class="rating rate-star">☆</span>
                                    @endfor
                                </span>
                                <a href="#review"><span class="review">{{count(\App\Rating::where('startup_id', $startup->id)->get())}} reviews</span></a>
                                <p class="rate-review">
                                    Rate and write a review
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--</div>-->
        </section>
        <section id="work-experience">
            <div class="container">
                <div class="bordered">
                    <div>
                        <h2 class="blue-text">Company Details</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12">
                            <div class="row" style="margin: 2%">
                                {{--Make the founder array--}}
                                <?php $i=0; ?>
                                @foreach($users as $user)
                                    @if(isset($user->first_name) and isset($user->last_name) and isset($user->profile_pics_url))
                                        @if($i==0 )
                                            <div class="col s4 left"><h3 >Founders</h3></div>
                                        @endif
                                        <div class="startup-container col s4">
                                            <div class="chip">
                                                <img src="<?php if(strpos($user->profile_pics_url, 'http')){ echo $user->profile_pics_url; }else{echo asset($user->profile_pics_url);} ?>" class="responsive-images ent-pic" alt="profile pic" />
                                                <span><a style="vertical-align: middle" href="{{asset("/user/$user->id")}}">{{$user->first_name}} {{$user->last_name}}</a></span>
                                            </div>
                                        </div>
                                        @if($i%2 == 1)
                                            <div class="col s4"><h3></h3></div>
                                        @endif
                                    @endif
                                    <?php ++$i ?>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col s4"><h3>Year Founded</h3></div>
                                <div class="col s8"><p>{{$startup->founded}}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h3>Team Size</h3></div>
                                <div class="col s8">
                                    <p>{{$startup->team_size}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h3>Description</h3></div>
                                <div class="col s8">
                                    <p>{{$startup->startup_brief}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s4 push-s7">
                                <a href="#modal1" class="btn btn-large btn-long waves-effect modal-trigger blue white-text lighten-1">Contact</a>

                                <div id="modal1" class="modal">
                                    <div class="modal-content">
                                        <h2>Contact</h2>
                                        <div class="row">
                                            <div class="col s3">Website:</div>
                                            <div class="col s9">{{$startup->website}}</div>
                                            <div class="col s3">Email:</div>
                                            <div class="col s9">{{$startup->email}}</div>
                                            <div class="col s3">Phone Number:</div>
                                            <div class="col s9">{{$startup->phone_number}}</div>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">CLOSE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col m3 hide-on-small-only">

                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section id="review">
            <div class="container">
                <div class="bordered">
                    <div class="row">
                        <h2 class="blue-text">Reviews</h2>
                        <div class="divider"></div>
                    </div>
                    <?php $i = 0; ?>
                    @foreach($ratings as $rating)
                        <div class="divider"></div>
                        <div class="row">
                            <?php
                            $users = \App\User::where('id', $rating->user_id)->get(array('first_name', 'id', 'last_name', 'profile_pics_url'));

                            $text = NULL;
                            ?>
                            <?php $experience = \App\WorkExperience::where('user_id', $rating->user_id)->get() ?>
                            @if(count($experience) > 0)
                                @foreach($experience as $experiences)
                                    <?php  $j = 0 ?>
                                    <?php $name = unserialize($experiences->company_name) ?>
                                    <?php $role = unserialize($experiences->role) ?>
                                    <?php $employed_status = unserialize($experiences->employed_status); ?>
                                    @if(count($name) > 0)
                                        @foreach($name as $company_name){{--Just using this to increment i until it matches the index--}}
                                        @if($employed_status[$j] == 1 and $experiences->primary_work_index == $j)
                                            <?php $text = $role[$j]." at ".$company_name ?>
                                        @endif
                                        <?php ++$j ?>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif

                            @foreach($users as $user)
                                <div class="col s2">
                                    <img class="responsive-images review-pic circle" src="{{$user->profile_pics_url}}"/>
                                </div>
                                <div class="col s9 review-header">
                                    <a href="{{asset("/user/$user->id")}}">
                                        <div class="reviewer blue-text col s4">{{$user->first_name}} {{$user->last_name}}</div>
                                    </a>
                                    <div class="reviewer-company col s5 grey-text"> - {{$text or "NULL"}}</div>
                                </div>
                            @endforeach
                            <div class="col s9 push-s1 review-content">
                                @if(strlen($rating->comment) > 200)
                                    <?php
                                    $comment = explode(' ', substr($rating->comment, 0, 400));
                                    $comment[count($comment) - 1] = NULL;
                                    $comment = implode(' ', $comment);
                                    ?>
                                    {{$comment}}<a href="#" class="right col s3"><span class="col s9 push-s2">...Read more</span></a>
                                @else
                                    {{$rating->comment or "No comment available"}}
                                @endif
                                <div style="margin: 2%"></div>
                            </div>
                        </div>
                        <?php ++$i ?>
                    @endforeach

                    @if(count($ratings) - 5 > 0)
                        <div class="row">
                            <div class="col s12">
                                <a href="{{url('/reviews/'.$startup->id)}}">
                                    <div class="center grey-text show-more"><i class="material-icons">details</i> Show More ({{count(\App\Rating::where('startup_id', $startup->id)->get()) - 5}} More) </div>
                                </a>
                            </div>
                        </div>
                    @endif

                    <div class="divider"></div>

                    <div class="row">
                        <h2 class="blue-text">Write a review</h2>
                        <div style="margin: 0% 3% 0 3%">
                            @if(isset($_COOKIE['success']))
                                <div class="center-align" id="success" style="width: 70%; margin-left: 15%;">
                                    <div class="card-panel col s4 push-s4" style="background-color: rgba(33, 150, 243, 0.7); border: 4px solid #4f9acc; border-radius: 10px;">
                                        <span id="x" style="font-size: 200%;position: relative;margin-top: -2%;cursor: pointer;font-weight: 100; float: right;" onclick="$('#success').hide()">x</span>
                                        <span style="color: white">Your data was successfully submitted</span>
                                    </div>
                                </div>
                                <script>
                                    setTimeout(function(){ $('#success').fadeOut(2000); }, 2000)
                                </script>
                            @endif
                            <div class="col s8 push-s2">
                                <div style="margin: 1.5%"></div>
                                <textarea required placeholder="What's your comment about this startup?" name="comment" class="review-post " rows="10" cols="30"></textarea>
                                <input type="hidden" name="id" id="id" value="{{$startup->id}}"/>
                                @if($errors->first('comment'))
                                    <li class="red-text">This field is required</li>
                                @endif
                                <button class="btn btn-success right">Post</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>



    <style>
        .chip{
            height: auto !important;
            width: 100% !important;
            background-color: transparent;
        }

        .chip img{
            border: 1px solid #9e9e9e;
        }

        .social-icons {
            position: absolute;
            top: 12px;
        //right: 10px;
        }

        .modal{
            box-shadow: 4px 4px 4px 0 rgba(0,0,0,0.22), 6px 2px 4px rgba(0,0,0,0.22);
        }
    </style>
    <script type="text/javascript">
        var click= 0;

        for (var i=5; i >= 1; --i){
            $('.rate-star#'+i).css('color',  "#9e9eff");
        }

    @if(isset($rating))
            <?php $count = \App\Rating::where('user_id', Auth::user()->id)->first(); ?>

            <?php $results = DB::select("select avg(rating_number) as rating_number from ratings where startup_id=$rating->startup_id"); ?> {{--Get the average rating for this startup--}}

                @if($results[0]->rating_number > 0)

        for (var i={{intval($results[0]->rating_number)}}; i >= 1; --i){
            $('.rate-star#'+i).css('color',  "gold");
        }
        click = {{intval($results[0]->rating_number)}};

        @endif
    @endif

            $('a[href=#review]').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
                || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 250
                    }, 1000);
                    return false;
                }
            }
        });
    </script>

@endsection