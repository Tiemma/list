@extends('layouts.master')


@section('content')
    <form method="POST" action="{{url('/registerstartup')}}" enctype="multipart/form-data" onsubmit="founder_check()">
        {{csrf_field()}}
        <section id="Overview">
            <div class="container">
                <div class="bordered">
                    <div>
                        <h2 class="grey-text">Overview</h2>
                        <div class="divider"></div>
                        <div class="divider"></div>
                    </div>
                    <div class="row">
                        <div class="col s12 m8 l8">
                            <div class="row">
                                <div class="col s4">
                                    <h4>Name</h4>
                                </div>
                                <div class="col s8">
                                    <input name="startup_name" value="{{ Input::old('startup_name') }}" type="text" class="input-field" required>
                                    @if($errors->has('startup_name'))<p class="error">*{{$errors->first('startup_name')}}</p> @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s4">
                                    <h4>Short Description</h4>
                                </div>
                                <div class="col s8">
                                    <textarea name="startup_brief" class="s-textarea" required>{{ Input::old('startup_brief') }}</textarea>
                                    @if($errors->has('startup_brief'))<p class="error">*{{$errors->first('startup_brief')}}</p> @endif
                                </div>
                            </div>
                            <div class="row founder-add col m12" >
                                <div class="col s4 left"><h4>Founder's Name(s)</h4></div>
                                <div class="founder-span col m4">
                                    <input type="text" class="autocomplete" required placeholder="Username / Email" oninput="search($(this))"/>
                                </div>

                                <div class="col m4 right">
                                    <ul  class='items z-depth-1'>
                                    </ul>
                                </div>
                            </div>
                            <style>
                                div.chip{
                                    width:  100% !important;
                                    font-size: 10px !important;
                                }
                            </style>

                            <div class="row">
                                <div class="col s4">
                                    <h4>Category</h4>
                                </div>
                                <div class="col s4 input-field">
                                    <?php $category = array('Health', 'Finance', 'Telecoms', 'Building and Infrastructure', 'Banking', 'NGO', 'Other') ?>
                                    <select name="category" class="browser-default" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        @foreach($category as $key=>$option)
                                            <option value="{{$key}}">{{$option}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col s4 none other-category">
                                    <input type="text" placeholder="Enter your category" name="other-category" required />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4">
                                    <h4>Website</h4>
                                </div>
                                <div class="col s8 input-field">
                                    <input name="website" value="{{ Input::old('startup_brief') }}" required placeholder="http://startupwebsite.com" class="col s12 input-field"/>
                                    @if($errors->has('website'))<p class="error">*{{$errors->first('website')}}</p> @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4">
                                    <h4>Social</h4>
                                </div>
                                <div class="col s8 input-field">
                                    <input name="facebook" placeholder="Facebook" class="col s12 input-field"  />
                                    @if($errors->has('facebook'))<p class="error">*{{$errors->first('facebook')}}</p> @endif
                                    <input name="twitter" placeholder="Twitter" class="col s12 input-field"  />
                                    @if($errors->has('twitter'))<p class="error">*{{$errors->first('twitter')}}</p> @endif
                                    <input name="linkedin" placeholder="LinkedIn" class="col s12 input-field"  />
                                    @if($errors->has('linkedin'))<p class="error">*{{$errors->first('linkedin')}}</p> @endif
                                </div>
                            </div>
                        </div>

                        <div class="col m4 l4 hide-on-small-only">
                            <div class="profile-pic large">
                                <img class="startup-image responsive-img" src="" />
                                    <input class="upload none" accept=".jpeg, .jpg, .png" type="file" name="profile_pic" id="profile_pic ">
                            </div>
                            <div class="row">
                                <button type="button" class="select-img  btn btn-flat">Select Image</button>
                            </div>
                            <!--
                            <div class="select-img btn-flat">
                                <span>Select Image</span>
                            </div>
                            -->
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success right">Save</button>
                    </div>
                </div>
            </div>
        </section>
        <!--- End of overview --->

        <section id="comapany-details">
            <div class="container">
                <div class="bordered">
                    <div>
                        <h2 class="grey-text">Company Details</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12 m8 l8">
                            <div class="row">
                                <div class="col s4"><h4>Founded</h4></div>
                                <div class="col s8">
                                    <input name="founded" required type="text" class="input-field">
                                    @if($errors->has('founded'))<p class="error">*{{$errors->first('founded')}}</p> @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h4>Contact</h4></div>
                                <div class="col s8 input-field">
                                    <input name="phone_number" placeholder="Phone number" class="col s12 input-field" required/>
                                    @if($errors->has('phone_number'))<p class="error">*{{$errors->first('phone_number')}}</p> @endif
                                    <input name="email" type="email" required placeholder="Email Address" class="col s12 input-field"/>
                                    @if($errors->has('email'))<p class="error">*{{$errors->first('email')}}</p> @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h4>Team Size</h4></div>
                                <div class="col s8 input-field">
                                    <select name="team_size" required class="browser-default">
                                        <option value="" disabled selected>please select from the list</option>
                                        <option value="1">1 - 5</option>
                                        <option value="2">5 - 10</option>
                                        <option value="3">10 - 15</option>
                                        <option value="4">15 - 20</option>
                                        <option value="5">20 - 25</option>
                                        <option value="6">25 - 50</option>
                                        <option value="7">50 - 100</option>
                                        <option value="8">Over 100</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4"><h4>Long Description</h4></div>
                                <div class="col s8 input-field">
                                    <textarea name="startup_long" placeholder="Long description of your startup 2000 words" class="col s12 input-field l-textarea"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col m4 hide-on-small-only">

                        </div>
                    </div>
                    <div class="row">
                        <button class="btn btn-success right">Save</button>
                    </div>
                </div>
            </div>
        </section>
        <section id="founding-rounds">
            <div class="container">
                <div class="bordered">
                    <div>
                        <h2 class="grey-text">Founding Rounds</h2>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="col s4">
                                    <h4>Date</h4>
                                </div>
                                <div class="col s8">
                                    <input name="date" type="date" class="datepicker">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4">
                                    <h4>Amount</h4>
                                </div>
                                <div class="col s8 input-field">
                                    <input name="amount" placeholder="Amount in Naira or Dollars" class="col s12 input-field"/>
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="col s4">
                                    <h4>Round</h4>
                                </div>
                                <div class="col s8 input-field">
                                    <select name="round" class="browser-default">
                                        <option value="" disabled selected>please select from the list</option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                        <option value="3">Option 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4">
                                    <h4>Investor</h4>
                                </div>
                                <div class="col s8 input-field">
                                    <input name="investor" placeholder="investor name or company" class="col s12 input-field"/>
                                </div>
                            </div>
                        </div>
                        <div class="col m4 l4 hide-on-small-only">--}}

                        </div>
                    </div>
                    <button type="submit" class="btn waves-effect waves-light blue submit-btn">Register Startup</button>
                </div>
            </div>
        </section>
    </form>

    <style>
        .chip{
            cursor: pointer;
            height: auto;
        }
    </style>
@endsection