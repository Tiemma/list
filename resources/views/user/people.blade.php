@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m6 push-m3 search-container">
                <br />
                <input id="search-box" placeholder="Search People" name="search" class="black-text search-box center" type="search">
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <div class="page right">
                    <?php $current_page = $users->currentPage() ?>
                    <span>{{ $users->count() * $users->currentPage() }}&nbsp; - &nbsp;{{$users->count() * $j = $current_page + 1 }}&nbsp; of &nbsp;{{$users->total()}}&nbsp;&nbsp;&nbsp;</span>
                    <a href="@if($current_page != 1){{$users->previousPageUrl()}} @else {{"#"}} @endif" class="btn-flat @if($current_page == 1) {{'disabled'}} @endif"><</a>
                    <a href="@if($current_page != $users->total() / $users->count()) {{$users->nextPageUrl()}} @else {{"#"}} @endif" class="btn-flat  @if($current_page == $users->total() / $users->count()) {{'disabled'}} @endif">></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <table class="highlight bordered striped centered">
                    <thead class="z-depth-1">
                    <tr>
                        <th>Full Name</th>
                        <th>Company</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Ranking</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <span class="col s12">
                                    <a href="{{asset("/user/".$user->id)}}">
                                    <span class="people-text right">{{$user->first_name}} {{$user->last_name}}</span>
                                    <img class="responsive-img circle left" src="<?php if(strpos($user->profile_pics_url, 'http')){ echo $user->profile_pics_url; }else{echo asset($user->profile_pics_url);} ?>" />
                                </a>
                                </span>
                            </td>
                            <?php $experience = \App\WorkExperience::where('user_id', $user->id)->get() ?>
                            <?php $company_role = NULL; $company =  NULL; ?>
                            @if(count($experience) > 0)
                                @foreach($experience as $experiences)
                                    <?php  $i = 0 ?>
                                    <?php $name = unserialize($experiences->company_name) ?>
                                    <?php $role = unserialize($experiences->role) ?>
                                    <?php $employed_status = unserialize($experiences->employed_status); ?>
                                    @if(count($name) > 0)
                                        @foreach($name as $company_name){{--Just using this to increment i until it matches the index--}}
                                        @if($employed_status[$i] == 1 and $experiences->primary_work_index == $i)
                                            <?php $company_role = $role[$i] ?>
                                            <?php $company = $company_name ?>
                                        @endif
                                        <?php ++$i ?>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                            <td>{{$company or "NULL"}}</td>
                            <td>{{$company_role or "NULL"}}</td>
                            <td>NULL</td>
                            <td>{{$user->id}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="not-found"></div>
            </div>
        </div>
    </div>
    <style>
        .bordered{
            margin-top: 0px;
        }


        div.row{
            overflow-x: auto;
        }

    </style>
@endsection
