@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m6 push-m3 search-container">
                <br />
                <form method="get" action={{asset("/search")}}>
                <input id="search-box" placeholder="Search Companies" name="search" class="black-text search-box center" type="search">
                <input type="hidden" value="company" name="type">
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <div class="page right">
                    <?php $count = max($startups->count(), $companies->count()) ?>
                    @if($count == $startups->count())
                        <?php $current_page = $startups->currentPage() ?>
                        <span>{{ $startups->count() * $startups->currentPage()  }}&nbsp; - &nbsp;{{$startups->count() * $j = $current_page + 1 }}&nbsp; of &nbsp;{{$startups->total()}}</span>
                        <a href="@if($current_page != 1){{$startups->previousPageUrl()}} @else {{"#"}} @endif" class="btn-flat @if($current_page == 1) {{'disabled'}} @endif"><</a>
                        <a href="@if($current_page != $startups->total() / $startups->count()) {{$startups->nextPageUrl()}} @else {{"#"}} @endif" class="btn-flat  @if($current_page == $startups->total() / $startups->count()) {{'disabled'}} @endif">></a>
                    @else
                        <?php $current_page = $companies->currentPage() ?>
                        <span>{{ $companies->count() * $companies->currentPage()  }}&nbsp; - &nbsp;{{$companies->count() * $j = $current_page + 1 }}&nbsp; of &nbsp;{{$companies->total()}}</span>
                        <a href="@if($current_page != 1){{$companies->previousPageUrl()}} @else {{"#"}} @endif" class="btn-flat @if($current_page == 1) {{'disabled'}} @endif"><</a>
                        <a href="@if($current_page != $companies->total() / $companies->count()) {{$companies->nextPageUrl()}} @else {{"#"}} @endif" class="btn-flat  @if($current_page == $companies->total() / $companies->count()) {{'disabled'}} @endif">></a>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m12">
                <table class="highlight bordered striped centered ">
                    <thead class="z-depth-1">
                    <tr>
                        <th>Full Name</th>
                        <th>Category</th>
                        <th>Primary Co-Founder</th>
                        <th>Address</th>
                        <th>Ranking</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php $i = 0; ?>
                    @foreach($companies as $company)
                        <tr>
                            <td>
                                <span class="col s12">
                                    <span class="people-text right">{{$company->name}}</span>
                                    <img class="responsive-img circle left" src="{{asset($company->logo_url)}}" />
                                </span>
                            </td>
                            <td>HealthFacts NG</td>
                            <td>Jeff Dean</td>
                            <td>{{$company->location}}</td>
                            <td>{{$company->id}}</td>
                        </tr>
                    @endforeach
                    @foreach($startups as $startup)
                        <tr>
                            <td>
                                <a href="{{asset('/startup/'.$startup->id)}}">
                                    <span class="col s12">
                                        <span class="people-text right">{{$startup->startup_name}}</span>
                                        <img class="responsive-img circle left" src="{{asset($startup->logo_url)}}" />
                                    </span>
                                </a>
                            </td>
                            </td>
                            <td>HealthFacts NG</td>
                            <td>Jeff Dean</td>
                            <td>{{$startup->address}}</td>
                            <td>{{$startup->id}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="not-found"></div>
            </div>
        </div>
    </div>
    <style>
        .bordered{
            margin-top: 0px;
        }


        div.row{
            overflow-x: auto;
        }

    </style>
@endsection
