// const BASEURL = "http://iqubeglobal.com/list/public/";

const BASEURL = "http://localhost:8000/";
const USER = BASEURL+"search/user";
const INVESTOR = BASEURL+"search/investor";
const STARTUP = BASEURL+"search/startup";


function searchUsers(searchTerm){
    var query = {
      "search" : searchTerm
    };
    $.ajax({
        type: 'GET',
        url: USER,
        data: query,
        success: function(response, textStatus, jqXHR){
                var results = response.data;
                renderUsers(results);
        },
        error: function (jqXHR,textStatus, err) {

        }
    });
}



function searchInvestor(){
    var query = {
        "search" : searchTerm
    };
    $.ajax({
        type: 'GET',
        url: INVESTOR,
        data: query,
        success: function(response, textStatus, jqXHR){
            var results = response.data;
            renderUsers(results);
        },
        error: function (jqXHR,textStatus, err) {

        }
    });
}

function searchStartups(searchTerm,category, location){
    var query = {
        "search" : searchTerm,
        "category": category,
        "location": location
    };
    $.ajax({
        type: 'GET',
        url: STARTUP,
        data: query,
        success: function(response, textStatus, jqXHR){
            var results = response.data;
            renderStartups(results);
        },
        error: function (jqXHR,textStatus, err) {

        }
    });
}


function renderUsers(data) {
    var dataArray = data.length;
    var newData = JSON.stringify(data);
    console.log(newData);
    if(jQuery.isEmptyObject(data)){
        $('.search-pack').append("<div class='search-card col s12'> " +
            "Sorry, " + "<b>"+$search_term.val()+"</b>" +" Was Not Found" +
            "</div>");
    }else{
        for(var i=0; i<dataArray; i++) {
            $('.search-pack').append(
                "<div class='search-card col s12'>"+"<a href="+BASEURL+'user/'+data[i].id+">"
                + data[i].last_name + " "  + data[i].first_name+
                "</a></div>");
        }
    }

}

function renderStartups(data) {
    var dataArray = data.length;
    var newData = JSON.stringify(data);
    console.log(newData);
    if(jQuery.isEmptyObject(data)){
        $('.search-pack').append("<div class='search-card col s12'> " +
            "Sorry, " + "<b>"+$search_term.val()+"</b>" +" Was Not Found" +
            "</div>");
    }else{
        for(var i=0; i<dataArray; i++) {
            $('.search-pack').append(
                "<div class='search-card col s12'>"+"<a href="+BASEURL+'startup/'+data[i].id+">"
                + data[i].startup_name+ "</a></div>");
        }
    }

}



//
// `
//         <div class="search-card col s12">`+
// data[i].last_name+ ' ' +data[i].first_name
//     ` </div>`


// .then(function (data) {
//     userArray = data;
//     for(var i=0; i<5; i++){
//         var first_name = userArray[i].first_name;
//         var last_name = userArray[i].last_name;
//         $('#country').append('<div class='+'col s12'+'>'+first_name+' '+last_name+' </div>');
//     }
// });