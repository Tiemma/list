/**
 * Created by root on 3/6/17.
 */
window.setInterval(function(){

    $('footer.page-footer').css('top', sticky_footer());

}, 2000);

function sticky_footer(){

    var windowHeight;

    if($(document).height() - $(window).height() > 0) {
        if ($(document).height() - parseInt($('footer.page-footer').css('top').replace(/\D/g, '')) > 50)
            windowHeight = $(document).height() + 40;
    }else{
        if ($(window).height() - $('footer.page-footer').css('top').replace(/\D/g, '') > 50)
            windowHeight = $(window).height() + 40;
    }
    return windowHeight+"px";
}

$(document).ready(function(){
    $('footer.page-footer').css('top', sticky_footer());
})
