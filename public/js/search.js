
var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
var url = null;
var emails_added = [];
var tags = '';
var original = '';
var experience_field = null;

$(document).ready(function(){
    try{
        original = $('tbody').html();
    }catch (e){

    }
});


function search(name) {

    if(name.val()) {

        tags = '';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post(baseUrl+"/public/search/register_startup", {name : name.val()})
            .done(function (data) {
                var array = $.map(data, function (value, index) {
                    return [value];
                });

                if (array[0].length > 0) {
                    array[0].forEach(function (entry) {
                        try {
                            if (!/^(f|ht)tps?:\/\//i.test(entry['profile_pics_url'].replace(/ /g, ''))) {
                                //Test if the url contains http after replacing all the whitespaces with blanks
                                url = baseUrl +
                                    '/public' +
                                    entry['profile_pics_url'];
                            } else {
                                url = entry['profile_pics_url'];
                            }
                        }catch(e){
                            url = baseUrl + '/public/images/techpoint.png';
                        }

                        if ($.inArray(entry['email'], emails_added) == -1) {
                            tags += `
            <div class="chip" onclick="text_value($(this))">
            <img class='image' src="` + url + `" alt="User">
            <span class="username">` +
                                entry['first_name'] +
                                ' ' +
                                entry['last_name'] +
                                `</span>
            <span class="email none">` + entry['email'] + `</span>
            </div>`;
                        }
                    });

                    $('.items').html(tags);

                }else{

                    $('.items').html(`<div class='chip'>
        <img style='padding: 2px' class='image' src="`+baseUrl+`/public/images/sad.png" alt="User">
        <h3>No user found!</h3>
        </div>`);
                }
            });
    }else{
        $('.items').html('');
    }
}

function text_value(chip) {
    username = chip.find('.username').html();
    email = chip.find('.email').html();
    image = chip.find('.image').prop('src');

    var text = `
  <div class="chip">
  <img  src="` + image + `" alt="User" />` +
        username +
        `<i class="material-icons close" onclick="remove_email('`+email+`')">close</i>
  <input type="hidden" value="`+ email +`" name="founder[]" id="founder_name" />
  </div>
  `;

    emails_added.push(email);

    $('.founder-span').append(text);

    $('.items').html('');
}

function remove_email(email){

    var index = emails_added.indexOf(email);

    emails_added.splice(index, 1);
}

$('.search-box').keyup(function() {

    var type = $(this).prop('placeholder').split(' ')[1];

    var name = $(this);

    if (name.val().trim()){

        if (type == 'People') {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(baseUrl + "/public/people", {name: name.val()})
                .done(function (data) {
                    var array = $.map(data, function (value, index) {
                        return [value];
                    });

                    tags = null;

                    if (array[0].length > 0) {
                        array[0].forEach(function (entry) {
                            try {
                                if (!/^(f|ht)tps?:\/\//i.test(entry['profile_pics_url'].replace(/ /g, ''))) {
                                    url = baseUrl +
                                        '/public' +
                                        entry['profile_pics_url'];
                                } else {
                                    url = entry['profile_pics_url'];
                                }
                            } catch (e) {
                                url = baseUrl + '/public/images/techpoint.png';
                            }

                            tags += `
            <tr>
            <td>
            <a href="` + baseUrl+"/public" +`/user/`+entry['id']+`">
            <span class="col s12">
            <span class="people-text right">` +
                                entry['first_name'] +
                                ' ' +
                                entry['last_name'] +
                                `</span>
            <img class="responsive-img circle left" src="` + url + `" />
            </span>
            </a>
            </td>
            <td>NULL</td>
            <td>NULL</td>
            <td>NULL</td>
            <td>` + entry['id'] + `</td>
            </tr>`;

                        });
                        $('.not-found').html('');
                        $('tbody').html(tags);
                    }else{
                        $('tbody').html('');
                        $('.not-found').html(`<div class='not-found'>
          <img style='padding: 2px' class='image' src="`+baseUrl+`/public/images/sad.png" alt="User">
          <h3>No user found!</h3>
          </div>`);
                    }
                });
        }else if(type == 'Companies'){
            $.post(baseUrl + "/public/companies", {name: name.val()})
                .done(function (data) {
                    var array = $.map(data, function (value, index) {
                        return [value];
                    });

                    tags = null;

                    if (array[0].length > 0) {
                        array[0].forEach(function (entry) {
                            try {
                                if (!/^(f|ht)tps?:\/\//i.test(entry['logo_url'].replace(/ /g, ''))) {
                                    url = baseUrl +
                                        '/public' +
                                        entry['logo_url'];
                                } else {
                                    url = entry['logo_url'];
                                }
                            } catch (e) {
                                url = baseUrl + '/public/images/techpoint.png';
                            }

                            tags += `
            <tr>
            <td>
            <span class="col s8">
            <span class="people-text right">`+entry['name']+`</span>
            <img class="responsive-img circle left" src="`+url+`" />
            </span>
            </td>
            <td>NULL</td>
            <td>NULL</td>
            <td>`+entry['location']+`</td>
            <td>`+entry['id']+`</td>
            </tr>`;

                        });
                        $('.not-found').html('');
                        $('tbody').html(tags);
                    }else{
                        $('tbody').html('');
                        $('.not-found').html(`<div class='not-found'>
          <img style='padding: 2px' class='image' src="`+baseUrl+`/public/images/sad.png" alt="User">
          <h3>No company found!</h3>
          </div>`);
                    }
                });
        }else if (type == 'Schools'){
            $.post(baseUrl + "/public/schools", {name: name.val()})
                .done(function (data) {
                    var array = $.map(data, function (value, index) {
                        return [value];
                    });

                    tags = null;

                    if (array[0].length > 0) {
                        array[0].forEach(function (entry) {
                            try {
                                if (!/^(f|ht)tps?:\/\//i.test(entry['logo_url'].replace(/ /g, ''))) {
                                    url = baseUrl +
                                        '/public' +
                                        entry['logo_url'];
                                } else {
                                    url = entry['logo_url'];
                                }
                            } catch (e) {
                                url = baseUrl + '/public/images/techpoint.png';
                            }

                            tags += `
            <tr>
            <td>
            <span class="col s8">
            <span class="people-text right">`+entry['name']+`</span>
            <img class="responsive-img circle left" src="`+url+`" />
            </span>
            </td>
            <td>NULL</td>
            <td>`+entry['location']+`</td>
            <td>`+entry['id']+`</td>
            </tr>`;

                        });
                        $('.not-found').html('');
                        $('tbody').html(tags);
                    }else{
                        $('tbody').html('');
                        $('.not-found').html(`<div class='not-found'>
          <img style='padding: 2px' src="`+baseUrl+`/public/images/sad.png" alt="User">
          <h3>No school found!</h3>
          </div>`);
                    }
                });
        } else if (type == 'Events'){
            $(document).ready(function(){
                if(!original)
                    original = $('.add-more').html();
            });
            $.post(baseUrl + "/public/events/search", {name: name.val()})
                .done(function (data) {
                    var array = $.map(data, function (value, index) {
                        return [value];
                    });
                    tags = "";
                    var i = 0;

                    if (array[0].length > 0) {
                        array[0].forEach(function (entry) {
                            try {
                                if (!/^(f|ht)tps?:\/\//i.test(entry['logo_url'].replace(/ /g, ''))) {
                                    url = baseUrl +
                                        '/public' +
                                        entry['logo_url'];
                                } else {
                                    url = entry['logo_url'];
                                }
                            } catch (e) {
                                url = baseUrl + '/public/images/techpoint.png';
                            }

                            if (i % 3 == 0) {
                                tags += `<div class="col s12 hide-on-small-only" style="padding-top: 5%"></div>`;
                            } else {
                                tags += `<div class="col s12 hide-on-med-and-up" style="padding-top: 5%"></div>`;
                            }
                            tags += `<div class="group">
            <div class="col s3 grey-background no-padding">
            <div class="col s12 no-padding">
            <a href="` + baseUrl + '/event/' + entry['id'] + `">
            <img src="` + url + `" class="event-icon z-depth-2" />
            <div style="padding: 2%"></div>
            <div class="col s8">
            <h2 style="font-weight: bolder">` + entry['name'] + `</h2>
            </div>
            </a>
            <div class="col s4 icons-small ">
            <img src="` + baseUrl + '/public/images/facebook-logo.png' + `" class="event-social-icons" />
            <img src="` + baseUrl + '/public/images/twitter-social.png' + `" class="event-social-icons" />
            </div>
            <div class="col s9">
            <div class="row">
            <div class="col s12">
            <div class="col s3">
            <img src="` + baseUrl + '/public/images/location.png' + `" class="events-icon"/>
            </div>
            <div class="col s9"><h5>` + entry['address'] + `</h5></div>
            </div>
            </div>
            <div style="margin: 5%"></div>
            <div class="row">
            <div class="col s12">
            <div class="col s3">
            <img src="` + baseUrl + '/public/images/calendar.png' + `" class="events-icon"/>
            </div>
            <div class="col s3 hide-on-small-only"><h5>` + entry['event_date'] + `</h5></div>
            <div class="col s6 hide-on-small-only">
            <a href="` + baseUrl + '/event/' + entry['id'] + `">
            <button type="button" class="btn right register-button">Read More</button>
            </a>
            </div>
            <div class="col s9 hide-on-med-and-up"><h5>` + entry['event_date'] + `</h5></div>
            </div>
            </div>
            </div>

            </div>
            <div class="col s12 hide-on-med-and-up no-padding">
            <a href="` + baseUrl + '/event/' + entry['id'] + `">
            <button type="button" class="btn right register-button">Read More</button>
            </a>
            </div>
            </div>
            <div class="col s1"></div>
            </div>`;
                            ++i;
                        });
                    }else{

                        $('.add-more').html(`<div class='not-found'>
          <img style='padding: 2px' class='image' src="`+baseUrl+`/public/images/sad.png" alt="User">
          <h3>No event found!</h3>
          </div>`);
                    }

                    if (tags.trim()) {
                        $('.add-more').html(tags);
                    }
                });
        }

    }else{
        if (type == 'Events')
            $('.add-more').html(original);
        else
            $('tbody').html(original);
    }
});



function show(){
    /*
     $('.show-more').hide();
     */
    $.post(baseUrl + "/public/events/show", {number: $('.grey-background').length})
        .done(function (data) {
            var array = $.map(data, function (value, index) {
                return [value];
            });

            tags = "";
            var i = 0;

            if (array[0].length > 0) {
                array[0].forEach(function (entry) {
                    try {
                        if (!/^(f|ht)tps?:\/\//i.test(entry['logo_url'].replace(/ /g, ''))) {
                            url = baseUrl +
                                '/public' +
                                entry['logo_url'];
                        } else {
                            url = entry['logo_url'];
                        }
                    } catch (e) {
                        url = baseUrl + '/public/images/techpoint.png';
                    }

                    if (i % 3 == 0) {
                        tags += `<div class="col s12 margin-top" ></div>`;
                    } else {
                        tags += `<div class="col s12 hide-on-med-and-up margin-top" ></div>`;
                    }
                    tags += `<div class="group">
        <div class="col s3 grey-background no-padding">
        <div class="col s12 no-padding">
        <a href="` + baseUrl + '/public/event/' + entry['id'] + `">
        <img src="` + url + `" class="event-icon z-depth-2" />
        <div style="padding: 2%"></div>
        <div class="col s8">
        <h2 style="font-weight: bolder">` + entry['name'] + `</h2>
        </div>
        </a>
        <div class="col s4 icons-small ">
        <img src="` + baseUrl + '/public/images/facebook-logo.png' + `" class="event-social-icons" />
        <img src="` + baseUrl + '/public/images/twitter-social.png' + `" class="event-social-icons" />
        </div>
        <div class="col s9">
        <div class="row">
        <div class="col s12">
        <div class="col s3">
        <img src="` + baseUrl + '/public/images/location.png' + `" class="events-icon"/>
        </div>
        <div class="col s9"><h5>` + entry['address'] + `</h5></div>
        </div>
        </div>
        <div style="margin: 5%"></div>
        <div class="row">
        <div class="col s12">
        <div class="col s3">
        <img src="` + baseUrl + '/public/images/calendar.png' + `" class="events-icon"/>
        </div>
        <div class="col s3 hide-on-small-only"><h5>` + entry['event_date'] + `</h5></div>
        <div class="col s6 hide-on-small-only">
        <a href="` + baseUrl + '/public/event/' + entry['id'] + `">
        <button type="button" class="btn right register-button">Read More</button>
        </a>
        </div>
        <div class="col s9 hide-on-med-and-up"><h5>` + entry['event_date'] + `</h5></div>
        </div>
        </div>
        </div>
        </div>
        <div class="col s12 hide-on-med-and-up no-padding">
        <a href="` + baseUrl + '/public/event/' + entry['id'] + `">
        <button type="button" class="btn right register-button">Read More</button>
        </a>
        </div>
        </div>
        <div class="col s1"></div>
        </div>`;
                    ++i;
                });

                if (tags.trim()) {
                    {
                        $('.add-more').append(tags);
                    }
                    $('#loader').hide();


                }
            }
        });
}


$(window).scroll(function() {  //This handles the loader on scroll event
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
        if ($('.grey-background').length == $('.noOfEvents').val()) {
            end();
        }else {
            $('#loader').show();
            setTimeout(function () {
                show()
            }, 3000);
        }
    }
});


function end(){ //This shows the end animation when all the events have been looked at
    $('#loader').hide();
    var options = [
        {selector: '#loader', offset: 250, callback: function(el) {
            Materialize.toast('This is the end!', 4000, 'rounded');
        } }];
    Materialize.scrollFire(options);
}

border = $('.drop-down').css('border');
$('.drop-down').css('border', '0');

//This is the search for the user page
//Company section
function company_search(){


    $('.company_name').focusin(function(){
        experience_field  = $(this);
        experience_field.next('.drop-down').slideDown(400);
        experience_field.next('.drop-down').css('border', border);
    });

    $('.company_name').focusout(function(){
        setTimeout(function(){
            $('.drop-down').css('border', '0');
            experience_field.next('.drop-down').slideUp(400);
        }, 100);
    });

    $('.company_name').on('input', function(){
        $('.company_names').html("");
        if(this.value.trim()){
            $.post(baseUrl + "/public/company/name", {name: this.value})
                .done(function (data) {
                    var array = $.map(data, function (value, index) {
                        return [value];
                    });
                    tags = "<h3 class='blue-text'>Companies <small class='black-text'>(Click to add)</small></h3><div class='divider'></div>";
                    if (array[0].length > 0) {
                        array[0].forEach(function (entry) {
                            tags += `<button type="button" class='btn btn-flat company_chip' onclick="change_company($(this))">`+entry['name']+`</button>`;
                        });
                        if(tags){
                            experience_field.next('.drop-down').html(tags);
                            // experience_field.closest(".company_body").next('.company_names').html(tags);
                        }
                    }
                });
        }else{
            experience_field.next('.drop-down').slideUp(400);
            // experience_field.closest(".company_body").next('.company_names').html("No results found!");
        }

    });


}


function change_company(company){
    var text = company.html();

    /*
     $('.company_names').html("");
     */
    experience_field.val(text);
}



/*

 function fetch(){
 var txt = $('#search').val();
 if(txt != '')
 {
 $.ajaxSetup({
 headers: {
 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
 }
 });
 $.ajax({
 url: "search",
 type:"GET",
 data: { search : txt },
 success:function(data){
 $('.wrapper').slideDown();
 $('#result').html(data);
 }
 });
 }
 else
 {
 $('#result').fadeOut('slow');
 $('#result').html('No results');
 $('#result').fadeIn('400');
 $('.wrapper').slideUp();
 document.getElementById('result').style.border="0";

 }
 }

 $(document).ready(function(){
 $('.wrapper').hide();
 $('#search').keyup(function(){
 fetch();
 });
 });
 */
