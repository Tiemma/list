var $search_term = $('#search-box');
var $type = $('#select-type option:selected');
var $category = $('#category option:selected').val();
var $location = $('#location option:selected').val();
var searchType;
var selectedLocation;

function hideWelcomeBanner() {
    $('.welcome-banner').on('click',function (event) {
        event.preventDefault();
        $(this).fadeOut('slow');
    });
}

function navLogin() {
    $('#nav-login').on('click', function (event) {
        event.preventDefault();
        $('#login-panel').fadeIn('slow');
        $('ul.tabs').tabs('select_tab', 'login');
    });
}



function showLogin() {
    $( window ).load(function() {
        $('#login-panel').fadeIn('slow');
        $('ul.tabs').tabs('select_tab', 'login');
    });
}






function loginClose() {
    $('#login-close').on('click', function (event) {
        event.preventDefault();
        $('#login-panel').fadeOut();
    });
}

function onSearchFocus() {
    $('.search-box').on('focus', function () {
        $('.search-board').slideDown("slow");
        console.log('search focus');

        // $('#search-page').effect('slide', { direction: 'down', mode: 'show' }, 1000);
    });

}
function onSearchClose() {
    //search option close
    $('.search-close').on('click', function (event) {
        event.preventDefault();
        $('.search-board').slideUp("slow");
        $('#search-page').effect('slide', { direction: 'down', mode: 'hide' }, 1000);
    });
    //on search page close
    $('.close-btn').on('click', function (event) {
        event.preventDefault();
        $('#search-page').effect('slide', { direction: 'down', mode: 'hide' }, 1000);
    });
}

function onSearch() {
    $('.search-btn').on('click', function () {
        $('.search-pack').empty();
        //send users to page
        if(searchType == 'user'){
            searchUsers($search_term.val());
        }
        searchStartup();

        $('.search-board').slideUp("slow");
        $('.close-btn').show('slow');

    });
}


function searchStartup(){
    if(searchType =='startup' && !isEmpty(selectedLocation)){
        searchStartupsByLoc($search_term.val(),selectedLocation);
    } else if(searchType =='startup'){
        searchStartups($search_term.val());
    }
}

function selectImage(){
    $('.select-img').on('click', function() {
        $('.upload').trigger('click');
    });


    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.upload').html('Change');
                $('.startup-image').prop('src', e.target.result);
                //alert(e.target.result);
                //$('.remove_button').show();
                //$('.remove').val('null');

            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".upload").change(function(){
        readURL(this);
    });
}




