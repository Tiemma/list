
//Init section
//Calls all the necessary functions that are needed for each page load
$(document).ready(function () {
    $('#select-type').on('change', function(){
        var type = $('#select-type option:selected').val();
    });
    hideWelcomeBanner();
    onSearchFocus();
    onSearch();
    onSearchClose();
    loginClose();
    navLogin();
    navRegister();
    selectImage();
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    $('select[name=category]').on('change', function(){
       if($('select[name=category] option:selected').text() == "Other"){
            $('.other-category').show();
           $('input[name=other-category]').attr('required', 'true');
       }else{
           $('.other-category').hide();
           $('input[name=other-category]').removeAttr('required');
       }
    });

});


$('.file_div').click(function(){ $('.file').trigger('click'); });
$('.upload_button').click(function(){ $('.file').trigger('click'); });

$(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
});

$(document).ready(function(){
    $('.collapsible').collapsible();
    $('.modal').modal();
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.upload_button').html('Change');
            $('.logo').attr('src', e.target.result);
            $('.remove_button').show();
            $('.remove').val('null');

        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(".file").change(function(){
    readURL(this);
});

/*My additions to this file*/
function project() {
    var dummy = ` 
         <div class="row">
            <div class="input-field col s12">
            <h5 class="center-align"><span class="col s4 m4" style="float: left">Projects worked on</span><input id="project_name" placeholder="Name of project" name="project_name[]" type="text" class="validate col s4 offset-s4" style=" width: 50%; height: 1rem"></h5>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12" >
                <h5 class="center-align">
                <textarea id="project_description" placeholder="Description" name="project_description[]" type="text" class="validate col s4 offset-s4 materialize-textarea" style=" width: 50%; height: 1rem"></textarea>
                </h5>
                </div>
            </div>
`;
    document.getElementById('project').insertAdjacentHTML( 'beforeend', dummy );
}

/*
function channel() {
    var dummy = ` 
                            <div class="row social_channel">
                                                <div class="input-field col s8 m7 push-m1">
                                                    <h5 class="center-align">
                                                        <span class="col s3 push-s1" style="float: left; margin-left: -20%; width: 40%">Social Channel</span>
                                                        <div class="col s8 pull-s1 select_input">
                                                            <select class="browser-default col s4 push-s4 left-align required select border" name="channel[]"  required style="width: 40%">
                                                                <option disabled="">Select a media channel</option>
                                                                                                                                                                                                            <option value="Facebook">Facebook</option>
                                                                                                                                                                                                                                                                                <option value="Twitter">Twitter</option>
                                                                                                                                                                                                                                                                                <option value="Instagram">Instagram</option>
                                                                                                                                                                                                                                                                                <option value="LinkedIn">LinkedIn</option>
                                                                                                                                                                                                                                                                                <option value="Youtube">Youtube</option>
                                                                                                                                                                                                </select>
                                                            <input id="handle" class="col s4 push-s4" required placeholder="Enter your social handle" name="handle[]" type="text" style=" width: 50%; height: 1rem;">
                                                        </div>
                                                    </h5>
                                                </div>

                                                <div class="col s4 m5 push-m1">
                                                    <div class="input-field">
                                                        <button type="button" class="btn-flat waves-effect waves-light col s11 m4 pull-s4 select_remove" onclick="$(this).closest('div.social_channel').remove()">Remove</button>
                                                    </div>
                                                </div>
                                            </div>
`;
    document.getElementById('channel').insertAdjacentHTML( 'beforeend', dummy );
}*/

function chips(event){
    var skills = "";
    for(var i = 0; i < $('.chips').material_chip('data').length; ++i){
        if($('.chips').material_chip('data')[i]['tag'] != '')
            skills += $('.chips').material_chip('data')[i]['tag'] + ", ";
    }
    $('#skills').html("<input type='hidden' value='"+skills+"' name='skills'/>");

}


function founder_check(){
    if (emails_added.length){
        $('.autocomplete').removeAttr('required');
    }
}

function init() {
    window.addEventListener('scroll', function(e){
        if($('body').width() > 700) {
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 150;
            setTimeout(function(){

                if (distanceY > shrinkOn) {
                    $('nav').css('position', 'fixed');
                    $('nav div.divider').hide(700);
                    $('#low-nav').hide(600);
                    $('nav').addClass('nav_height');
                } else {
                    $('nav').css('position', 'relative');
                    $('nav div.divider').show(700);
                    $('#low-nav').show(600);
                    $('nav').removeClass('nav_height');
                }

            }, 100);


        }
    });
}
window.onload = init();




