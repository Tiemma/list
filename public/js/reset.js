
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

//This is used on the page where he/she puts in an email address
$('button.send_email').click(function(){
    var email = $('input[name=user_email]').val();
    if(!email){
        Materialize.toast("Please enter an email", 2000, "rounded");
        return 0;
    }
        $('.grey-overlay').show();
        $('.pulse').show();
    $.post(baseUrl+"/public/reset", {user_email: email}).done(function(data){

        setTimeout(function(){
            $('.grey-overlay').hide();
            $('.pulse').hide();
            if(data == 1){
                $('.email').html('<h3 class="blue-text center-align">Your request has been submitted.</h3><p></p><h3 class="blue-text center-align">Check your email for the reset link</h3>');
                setTimeout(function(){
                    window.location.href = baseUrl+"/public";
                }, 4000)
            }else{
                Materialize.toast("This user doesn't exist", 2000, "rounded");
                Materialize.toast("Please check your email and try again", 2000, "rounded");
            }
        }, 2000);

    });
});

//This is used where the link sent to the email redirects to
$('button.reset_password').click(function(){

    password = $('.reset input[name=password]').val().trim();

    password_confirmation = $('.reset input[name=confirm_password]').val().trim();

    email_hash = $('input[name=email_hash]').val().trim();

if(password) {
    if (password == password_confirmation) {

        $('.grey-overlay').show();
        $('.pulse').show();

        $.post(baseUrl + '/public/reset/user',
            {password: password, email_hash: email_hash})
            .done(function (data) {
                setTimeout(function () {
                    $('.grey-overlay').hide();
                    $('.pulse').hide();
                }, 2000);

                if (data == 1) {
                    $('.email').html('<h3 class="blue-text center-align" style="margin: 10%">Your password has been reset.</h3>');
                    setTimeout(function () {
                        window.location.href = baseUrl + "/public";
                    }, 4000);
                } else {
                    $('.email').html('<h3 class="blue-text center-align" style="margin: 10%">An error occurred!.<p>Please try again later!</p></h3>');
                }

            });
    } else {
        Materialize.toast("The passwords do not match", 2000, "rounded");
    }
}else{
    Materialize.toast("Enter a password please!", 2000, "rounded");
}
});

