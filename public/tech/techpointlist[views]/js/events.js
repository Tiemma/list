function hideWelcomeBanner() {
    $('.welcome-banner').on('click',function (event) {
        event.preventDefault();
        $(this).fadeOut('slow');
    });
}