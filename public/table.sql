create database `techpointlist`;

use `techpointlist`;

create table `admins`
 (
`id` int NOT NULL AUTO_INCREMENT,
`name` varchar(200) NULL ,
`username` varchar(200) NULL,
`password` varchar(200) NULL,
`email` varchar(200) NULL,
`phone_number` varchar(200) NULL,
`created_at` datetime null,
`updated_at` datetime null,
`remember_token` varchar(200) NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`username`, `email`, `phone_number`)
);

create table `schools`
(
`id` int NOT NULL AUTO_INCREMENT,
`name` varchar(200) NULL,
`location` varchar(200) NULL,
`brief_summary` varchar(200) NULL,
`logo_url` varchar(200) NULL,
`trending` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
UNIQUE KEY (`name`)
);

create table `investors` 
(
`id` int NOT NULL AUTO_INCREMENT,
`name` varchar(200) NULL,
`email` varchar(200) NULL,
`phone_number` varchar(200) NULL,
`dob` varchar(200) NULL,
`brief_profile` varchar(200) NULL,
`profile_pics_url` varchar(200) NULL,
`company_id` int NULL,
`startup_id` int NULL,
`trending` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
UNIQUE KEY (`email`, `phone_number`)
);


create table `industries`
(
`id` int NOT NULL AUTO_INCREMENT,
`industry_name` varchar(200) NULL,
`industry_profile` varchar(200) NULL,
`trending` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
UNIQUE KEY (`industry_name`)
);

create table `users`
(
`id` int NOT NULL AUTO_INCREMENT,
`school_id` int NULL,
`first_name` varchar(200) NULL,
`last_name` varchar(200) NULL,
`email` varchar(200) NULL,
`password` varchar(200) NULL,
`profile_pics_url` varchar(200) NULL,
`sex`varchar(200) NULL,
`dob` varchar(200) NULL,
`phone_number` varchar(200) NULL,
`brief_profile` text NULL,
`social_channel` varchar(200) NULL,
`confirmation_status` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
`remember_token` varchar(200) NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`email`, `phone_number`)
);

create table `skills` 
(
`id` int NOT NULL AUTO_INCREMENT,
`user_id` int NULL,
`name` varchar(200) NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`)
);

create table `companies`
(
`id` int NOT NULL AUTO_INCREMENT,
`industry_id` int NULL,
`name`  varchar(200) NULL,
`logo_url` varchar(200) NULL,
`location` varchar(200) NULL,
`brief_profile` varchar(200) NULL,
`trending` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
UNIQUE KEY (`name`)
);

create table `startups`
(
`id` int NOT NULL AUTO_INCREMENT,
`user_id` int NULL,
`industry_id` int NULL,
`startup_name` varchar(200) NULL,
`startup_brief` varchar(200) NULL,
`founder_name` varchar(200) NULL,
`logo_url` varchar(200) NULL,
`address` varchar(200) NULL,
`cac_registered` boolean NULL,
`cac_registration_date` varchar(200) NULL,
`confirmation_status` boolean NULL,
`trending` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
UNIQUE KEY (`startup_name`),
FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);

create table `projects` 
(
`id` int NOT NULL AUTO_INCREMENT,
`user_id` int NULL,
`name` varchar(200) NULL,
`description` varchar(200) NULL,
`role` varchar(200) NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);

create table `investments`
(
`id` int NOT NULL AUTO_INCREMENT,
`investor_id` int NULL,
`startup_id` int NULL,
`amount` int NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
FOREIGN KEY (`startup_id`) REFERENCES startups(`id`) ON DELETE CASCADE
);

create table `events`
(
`id` int NOT NULL AUTO_INCREMENT,
`industry_id` int NULL,
`name` varchar(200) NULL,
`address` varchar(200) NULL,
`brief_profile` text NULL,
`logo_url` varchar(200) NULL,
`contact_name` varchar(200) NULL,
`contact_email` varchar(200) NULL,
`contact_phone_no` varchar(200) NULL,
`trending` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`)
);

create table `work_experiences`
(
`id` int NOT NULL AUTO_INCREMENT,
`user_id` int NULL,
`company_name` varchar(200) NULL,
`role` varchar(200) NULL,
`description` varchar(200) NULL,
`start_date` varchar(200) NULL,
`end_date` varchar(200) NULL,
`employed_status` boolean NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);

create table `ratings`
(
`id` int NOT NULL AUTO_INCREMENT,
`user_id` int NULL,
`startup_id` int NULL,
`rating_number` int NULL,
`comment` varchar(200) NULL,
`created_at` datetime null,
`updated_at` datetime null,
PRIMARY KEY (`id`),
FOREIGN KEY (`startup_id`) REFERENCES startups(`id`) ON DELETE CASCADE
);

