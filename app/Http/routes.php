<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('testpage', 'UserController@testPage');

Route::get('/coming', function(){
    return view('user.coming');
});


Route::get('search', 'Search\SearchController@search');


//search api

Route::group(['prefix'=>'search', 'namespace' => 'Search'], function(){
    Route::get('user', 'SearchControllerApi@searchUser');
    Route::get('investor', 'SearchControllerApi@searchInvestor');
    Route::get('startup', 'SearchControllerApi@searchStartup');
    Route::get('project', 'SearchControllerApi@searchProject');
    Route::get('company', 'SearchControllerApi@searchCompany');
});

Route::group(['prefix'=>'search'], function(){
    Route::post('register_startup', 'StartupSearchController@register');
    Route::get('/update_startup', 'StartupSearchController@update');
});


Route::get('/facebook', 'SocialController@facebook');
Route::get('/twitter', 'SocialController@twitter');
Route::get('/linkedin', 'SocialController@linkedin');
Route::get('/callback_facebook', 'SocialController@callback_facebook');
Route::get('/callback_twitter', 'SocialController@callback_twitter');
Route::get('/callback_linkedin', 'SocialController@callback_linkedin');


Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

//Nav bar pages
Route::get('/companies', 'CompanyController@index');
Route::get('/people', 'PeopleController@index');
Route::get('/schools', 'SchoolController@index');
Route::get('/event/{id}', 'EventsController@index');
Route::get('/events', 'EventsController@events');
Route::get('/school/{id}', 'SchoolController@school');
Route::get('/reset', 'ResetEmailController@index');
Route::get('/hash', 'EmailHashController@make_hash');
Route::get('/reset/{email_hash}', 'ResetEmailController@reset');


Route::post('/people', 'PeopleController@search');
Route::post('/companies', 'CompanyController@search');
Route::post('/schools', 'SchoolController@search');
Route::post('/events/show', 'EventsController@show');
Route::post('/events/search', 'EventsController@search');
Route::post('/reset', 'ResetEmailController@send');
Route::post('/reset/user', 'ResetEmailController@user');


Route::auth();
//User pages for logged in users
Route::group(['middleware' => ['auth']], function(){

    Route::get('/startup/{id}', 'UserController@redirectToStartup');
    Route::get('/startup', 'UserController@showStartup');
    Route::get('/update', 'UserController@getUpdate');
    Route::get('/registerstartup', function(){ return view('user.registerstartup'); });

    Route::get('/user', 'UserController@userProfile');
    Route::get('/user/{id}', 'UserController@redirectToUser');
    Route::get('/entrepreneur/{id}', 'UserController@entrepreneur');
    Route::get('/entrepreneur', 'UserController@showEntrepreneur');

    Route::get('/updatestartup/{id}', 'StartupController@updateStartup');
    Route::get('/startupselect', 'StartupController@startupSelect');
    Route::get('/selectstartup', 'UserController@getUpdate');
    Route::get('/reviews/{id}', 'RatingController@index');

    Route::post('/reviews', 'RatingController@submit');
    Route::post('/update', 'UserController@postUpdate');
    Route::post('/registerstartup', 'StartupController@saveStartup');
    Route::post('/updatestartup/{id}', 'StartupController@updateStartupData');
    Route::post('/company/name', 'CompanyController@search');
});




//For the admin... The following routes are for the admin management only
Route::group(['middleware' => ['web']], function(){
    //Login Routes...
    Route::get('/admin/login', 'AdminAuth\AuthController@showLoginForm');
    Route::post('/admin/login', 'AdminAuth\AuthController@doLogin');

    //Logout Route
    Route::get('/admin/logout', 'AdminAuth\AuthController@doLogout');

    // Registration Routes... for creating new admins within the admin portal
    Route::get('/admin/register', 'AdminAuth\AuthController@showRegistrationForm');
    Route::post('/admin/register', 'AdminAuth\AuthController@doRegister');
});

Route::group(['middleware' => ['web', 'admin']], function () {
    //Landing page(dashboard) for admin
    Route::get('/admin', 'Admin\AdminController@index');
    Route::get('/admin/home', 'Admin\AdminController@index');

    //User Management

    Route::get('admin/users', 'Admin\AdminController@showUsers');
    //Route::get('admin/user/{id}', 'Admin\UserController@listUser');
    Route::get('admin/user/create', 'Admin\UserController@createUserPage');
    Route::post('admin/user/create', 'Admin\UserController@createUser');
    Route::get('admin/user/{id}/edit', 'Admin\UserController@editUserPage');
    Route::post('admin/user/{id}/edit', 'Admin\UserController@editUser');
    Route::delete('admin/user/{id}/delete', 'Admin\UserController@deleteUser');
    Route::get('admin/user/search', 'Admin\UserController@searchUser');

    //Startup Management
    Route::get('admin/startups', 'Admin\AdminController@showStartups');
    //Route::get('admin/startup/{id}', 'Admin\StartupController@listStartup');
    Route::get('admin/startup/create', 'Admin\StartupController@createStartupPage');
    Route::post('admin/startup/create', 'Admin\StartupController@createStartup');
    Route::get('admin/startup/{id}/edit', 'Admin\StartupController@editStartupPage');
    Route::post('admin/startup/{id}/edit', 'Admin\StartupController@editStartup');
    Route::delete('admin/startup/{id}/delete', 'Admin\StartupController@deleteStartup');
    Route::get('admin/startup/search', 'Admin\StartupController@searchStartup');

    //Event Management
    Route::get('admin/events', 'Admin\AdminController@showEvents');
    //Route::get('admin/event/{id}', 'Admin\EventController@listEvent');
    Route::get('admin/event/create', 'Admin\EventController@createEventPage');
    Route::post('admin/event/create', 'Admin\EventController@createEvent');
    Route::get('admin/event/{id}/edit', 'Admin\EventController@editEventPage');
    Route::post('admin/event/{id}/edit', 'Admin\EventController@editEvent');
    Route::delete('admin/event/{id}/delete', 'Admin\EventController@deleteEvent');

    //School Management
    Route::get('admin/schools', 'Admin\AdminController@showSchools');
    //Route::get('admin/school/{id}', 'Admin\SchoolController@listschool');
    Route::get('admin/school/create', 'Admin\SchoolController@createSchoolPage');
    Route::post('admin/school/create', 'Admin\SchoolController@createSchool');
    Route::get('admin/school/{id}/edit', 'Admin\SchoolController@editSchoolPage');
    Route::post('admin/school/{id}/edit', 'Admin\SchoolController@editSchool');
    Route::delete('admin/school/{id}/delete', 'Admin\SchoolController@deleteSchool');

    //Investor Management
    Route::get('admin/investors', 'Admin\AdminController@showInvestors');
    //Route::get('admin/investor/{id}', 'Admin\InvestorController@listInvestor');
    Route::get('admin/investor/create', 'Admin\InvestorController@createInvestorPage');
    Route::post('admin/investor/create', 'Admin\InvestorController@createInvestor');
    Route::get('admin/investor/{id}/edit', 'Admin\InvestorController@editInvestorPage');
    Route::post('admin/investor/{id}/edit', 'Admin\InvestorController@editInvestor');
    Route::delete('admin/investor/{id}/delete', 'Admin\InvestorController@deleteInvestor');
    Route::get('admin/investor/search', 'Admin\InvestorController@searchInvestor');

    //Company Management
    Route::get('admin/companies', 'Admin\AdminController@showCompanies');
    //Route::get('admin/company/{id}', 'Admin\CompanyController@listCompany');
    Route::get('admin/company/create', 'Admin\CompanyController@createCompanyPage');
    Route::post('admin/company/create', 'Admin\CompanyController@createCompany');
    Route::get('admin/company/{id}/edit', 'Admin\CompanyController@editCompanyPage');
    Route::post('admin/company/{id}/edit', 'Admin\CompanyController@editCompany');
    Route::delete('admin/company/{id}/delete', 'Admin\CompanyController@deleteCompany');

    //Industry Management
    Route::get('admin/industries', 'Admin\AdminController@showIndustries');
    //Route::get('admin/industry/{id}', 'Admin\IndustryController@listIndustry');
    Route::get('admin/industry/create', 'Admin\IndustryController@createIndustryPage');
    Route::post('admin/industry/create', 'Admin\IndustryController@createIndustry');
    Route::get('admin/industry/{id}/edit', 'Admin\IndustryController@editIndustryPage');
    Route::post('admin/industry/{id}/edit', 'Admin\IndustryController@editIndustry');
    Route::delete('admin/industry/{id}/delete', 'Admin\IndustryController@deleteIndustry');

    //Investment Mgmt
    Route::get('admin/investments', 'Admin\AdminController@showInvestments');
});
