<?php

namespace App\Http\Controllers;

use App\Startup;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;




class StartupController extends Controller {


    public function startupSelect(){
        $userID = Auth::user()->id;
        $startups = json_decode(Startup::where('user_id', $userID)
            ->get());

        return view('user.selectstartup')
            ->with('startups', $startups);

    }


    public function updateStartup($id){

        $startup = json_decode(Startup::where('id',$id )
            ->first());
        if($startup->user_id == Auth::user()->id){
            return view('user.updatestartup')
                ->with('startup', $startup);
        }
        return redirect('/home');

    }

    public function updateStartupData(Request $request, $id){
        $inputs = $request->all();
        $startup = Startup::where('id', $id)
            ->first();
        $rules = $this->validationForUpdate($startup);
        $validator = Validator::make($inputs, $rules);

        // redirecting if validation fails
        $data = array();
        $this->initializeData($request, $data, $id);
        $startup->update($data);
        if($validator->fails()){
            return redirect('updatestartup/'.$startup->id)
                ->withErrors($validator)
                ->withInput();
        }
        setcookie('update', 1, time()+10);
        return redirect('updatestartup/'.$startup->id);

    }



    public function saveStartup(Request $request){

        $inputs = $request->all();
        $rules = $this->valiationForRegister();
        $validator = Validator::make($inputs, $rules);
        // redirecting if validation fails

        if($validator->fails()){
            return redirect('registerstartup')
                ->withErrors($validator)
                ->withInput();
        }

        $data = array();
        $this->initializeData($request, $data);

        $startup = Startup::create($data);
        if($startup){
            return redirect('updatestartup/'.$startup->id);
        }

    }


    private function saveProfilePic($request, $id){
        if ($request->hasFile('profile_pic')) {
            $file = $request->file('profile_pic');
            $destination_path = base_path().'/public/startup_logos/';
            $file_name = rand(11111, 99999).'.'.$file->extension();
            $file->move($destination_path, $file_name);
            $url = '/startup_logos/'.$file_name;
            return $url;
        }else{
            if(count(Startup::where('id', '=', $id)->first())){return Startup::where('id', '=', $id)->first()->logo_url;}
        }
    }



    private function initializeData($request, &$data, $id=null){
        $data['logo_url'] = $this->saveProfilePic($request, $id);
        $data['founder_name'] = serialize($request->input('founder'));
        /*        $data['founder_image_url'] = serialize($this->founderProfilePic($request));*/
        $data['user_id'] = Auth::user()->id;
        $data['startup_name'] = $request->input('startup_name');
        $data['startup_brief'] =  $request->input('startup_brief');
        $data['category'] =  $request->input('category');
        $data['website'] = $request->input('website');
        $data['facebook'] = $request->input('facebook');
        $data['twitter'] = $request->input('twitter');
        $data['linkedin'] = $request->input('linkedin');
        $data['founded'] = $request->input('founded');
        $data['phone_number'] = $request->input('phone_number');
        $data['email'] = $request->input('email');
        $data['team_size'] = $request->input('team_size');
        $data['startup_long'] = $request->input('startup_long');
        $data['date']= $request->input('date');
        $data['amount'] = $request->input('amount');
        $data['round'] = $request->input('round');
        $data['investor'] = $request->input('investor');
    }


    private function valiationForRegister(){
        return $rules = array(
            'startup_name'=> 'required|unique:startups',
            'email'=> 'required|unique:startups',
            'startup_brief'=>'required',
            'website'=>'required',
            'phone_number'=>'unique:startups',
        );

    }

    private function validationForUpdate( $startup){
        return $rules = array(
            'startup_name'=> 'required|unique:startups,startup_name,'.$startup->id,
            'email'=> 'required|unique:startups,email,'.$startup->id,
            'startup_brief'=>'required',
            'website'=>'required',

        );
    }





}
