<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use Validator;

use Redirect;

use DB;

Use App\Rating;

class RatingController extends Controller
{
    //
    public function index($id){
        $rating = Rating::where('startup_id', '=', $id)->orderBy('created_at', 'DESC')->get();


        return view('user.reviews')->with('ratings', $rating)->with('id', $id);
    }

    public function submit(Request $request){

        $rating_data = array();

        if(empty($request->input('rating_number'))){
            $rules = array(
                'comment' => 'required',
                'id' => 'required'
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect::back()->withErrors($validator);
            }
            $rating_data['comment'] = $request->input('comment');
        }

        $rating_data['user_id'] = Auth::user()->id;
        $rating_data['startup_id'] = $request->input('id');
        $rating_data['rating_number'] = $j = $request->input('rating_number');
        Rating::updateOrCreate(array('user_id'=>Auth::user()->id, 'startup_id'=>$request->input('id')), $rating_data);
        $rating = (DB::select("select avg(rating_number) as rating_number from ratings where startup_id=".$request->input('id')))[0]->rating_number;

        if(empty($request->input('rating_number'))) {
            setcookie('success', 'True', time() + 10);
            return redirect('startup');
        }

        return ceil($rating);

    }
}
