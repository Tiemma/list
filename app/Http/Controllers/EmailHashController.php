<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class EmailHashController extends Controller
{
    //
    public function make_hash(){
        foreach(User::all() as $user){
            $user->email_hash =  "";
            $user->save();
        }
    }
}
