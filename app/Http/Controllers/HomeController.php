<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Startup;

use App\School;

use App\User;

use App\Industry;

use App\Investor;

class HomeController extends Controller
{
    public function index(){
        $user = User::where('confirmation_status', '=', 1)
            ->orderBy('updated_at', 'desc')
            ->take(2)
            ->get();

        $startup = Startup::where('trending', '=', 1)
            ->orderBy('updated_at', 'desc')
            ->take(2)
            ->get();

        $investor = Investor::where('trending', '=', 1)
            ->orderBy('updated_at', 'desc')
            ->take(2)
            ->get();

        $schools = School::where('trending', 1)
            ->orderBy('updated_at', 'desc')
            ->take(2)
            ->get();

        return view('user.index')
            ->with('users', $user)
            ->with('startups', $startup)
            ->with('investors', $investor)
            ->with('schools', $schools);

    }

}
