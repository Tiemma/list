<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use DB;

class PeopleController extends Controller
{
    //
    public function index(){
        $users = User::orderBy('id', 'ASC')->paginate(50);
        return view('user.people')->with('users', $users);
    }

    public function search(Request $request){
        $name = explode(' ', preg_replace('/\s+/', ' ', $request->input('name')));

        if(isset($name[1])) {

            $user = DB::table('users')
                ->where([
                    ['first_name', 'like', '%'.$name[0].'%'],
                    ['last_name', 'like', '%'.$name[1].'%'],
                ])
                ->take(50)->get(array('id', 'first_name', 'last_name', 'profile_pics_url'));


            return response()
                ->json(['user' => $user]);

        }else{

            $user = DB::table('users')
                ->where('first_name', 'like', '%'.$name[0].'%')
                ->orWhere('last_name', 'like', '%'.$name[0].'%')
                ->take(50)->get(array('id', 'first_name', 'last_name', 'profile_pics_url'));


            return response()
                ->json(['user' => $user]);
        }
    }
}
