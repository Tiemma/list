<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Company;

class CompanyController extends Controller
{
    public function createCompanyPage(){
        $industries = Industry::all();
        return view('admin.create_company')->with('industries', $industries);
    }

    public function createCompany(Request $request){
    	$data = $request->all();

        //return $data['brief_profile'];
        $validator = Validator::make($data, [
            'company_name' => 'required',
            'company_location' => 'required',
        ]);

        

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/company/create')
                ->withErrors($validator)
                ->withInput();
        }

        else{
        	//return 'yes';
            $url = NULL;
	        if ($request->hasFile('company_logo')){
	            $file = $request->file('company_logo');
	            $destination_path = base_path().'/public/company_logo/';
	            $file_name = $request['company_name'].'.'.$file->extension();
	            $file->move($destination_path, $file_name);
	            $url = '/company_logo/'.$file_name;
	            //return "file saved";
	        }

            $newC = Company::create([
                'name' => $request['company_name'],
                'location' => $request['company_location'],
                'logo_url' => $url,
                'industry_id' => $request['industry_id'],
                'brief_profile' => $request['brief_profile'],
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/companies')->with('success', 'New companies added');
        }
    }

    public function editCompanyPage($id){
        $company = Company::find($id);
        $industries = Industry::all();
        return view('admin.edit_company')->with('company', $company)->with('industries', $industries);
    }

    public function editCompany(Request $request, $id){
        $data = $request->all();

        $company = Company::find($id);
        //return $data;
        $validator = Validator::make($data, [
            'company_name' => 'required',
            'company_location' => 'required',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/company/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            //return 'yes';
            $url = NULL;
            if ($request->hasFile('company_logo')){
                $file = $request->file('company_logo');
                $destination_path = base_path().'/public/company_logo/';
                $file_name = $request['company_name'].'.'.$file->extension();
                $file->move($destination_path, $file_name);
                $url = '/company_logo/'.$file_name;
                //return "file saved";
            }

            $company = $company->update([
                'name' => $request['company_name'],
                'location' => $request['company_location'],
                'logo_url' => $url,
                'industry_id' => $request['industry_id'],
                'brief_profile' => $request['brief_profile'],
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/companies')->with('success', 'Updated Company details');
        }
    }

    public function deleteCompany(Request $request, $id){
        $company = Company::find($id);
        $company->delete();
        return redirect('/admin/companies')->with('success', 'Company deleted');
    }
}
