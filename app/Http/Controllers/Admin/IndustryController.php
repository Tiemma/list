<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Industry;

class IndustryController extends Controller
{
    public function createIndustryPage(){
        return view('admin.create_industry');
    }

    public function createIndustry(Request $request){
    	$data = $request->all();

        //return $data['brief_profile'];
        $validator = Validator::make($data, [
            'industry_name' => 'required|unique:industries',
        ]);

        

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/industry/create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
        	Industry::create([
                'industry_name' => $request['industry_name'],
                'industry_profile' => $request['industry_profile'],
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/industries')->with('success', 'New industry added');
        }

    }

    public function editIndustryPage($id){
        $industry = Industry::find($id);
        return view('admin.edit_industry')->with('industry', $industry);
    }

    public function editIndustry(Request $request, $id){
    	$data = $request->all();

       	$industry = Industry::find($id);
        //return $data['brief_profile'];
        $validator = Validator::make($data, [
            'industry_name' => 'required',
        ]);

        

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/industry/create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
        	$industry->update([
                'industry_name' => $request['industry_name'],
                'industry_profile' => $request['industry_profile'],
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/industries')->with('success', 'New industry added');
        }
    }

    public function deleteIndustry($id){
        $industry = Industry::find($id);
        $industry->delete();
        return redirect('/admin/industries')->with('success', 'Industry deleted');
    }

}