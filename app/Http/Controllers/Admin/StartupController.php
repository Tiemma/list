<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Startup;
use App\Industry;
use App\User;
use App\Investor;
use App\Investment;

class StartupController extends Controller
{

    public function listStartup($id){
    	//
    }

    public function createStartupPage(){
        $users = User::all();
        $industries = Industry::all();
        $investors = Investor::all();
        return view('admin.create_startup')
                ->withUsers($users)
                ->withIndustries($industries)
                ->withInvestors($investors);
    }

    public function createStartup(Request $request){
    	$data = $request->all();

    /*    //return $data;
        if($request->hasFile('logo_url')){
            $file = $request->file('logo_url');
            $destination_path = base_path().'/public/startup_logos/';
            $file_name = $request['startup_name'].'.'.$file->extension();
            $file->move($destination_path, $file_name);
            $logo_url = '/startup_logos/'.$file_name;
            //return "file saved";
        }*/

        $logo_url = '/images/techpoint.png';
     /*   else
            return redirect('/admin/startup/create')
                ->withErrors("Please include startup logo");*/

        $validator = Validator::make($data, [
            'startup_name' => 'required|max:255',
            'address' => 'required|max:255',
            'brief_profile' => 'required',
            'user_id' => 'required',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/startup/create')
                ->withErrors($validator)
                ->withInput();
        }

        else{
            //return "yes";
            $newSt = Startup::create([
                'startup_name' => $request['startup_name'],
                'address' => $request['address'],
                'startup_brief' => $request['brief_profile'],
                'user_id' => $request['user_id'],
                'industry_id' => $request['industry_id'],
                'confirmation_status' => $request['status'] ? 1 : 0,
                'trending' => $request['trending'] ? 1 : 0,
                'cac_registered' => $request['cac_registered'] ? 1 : 0,
                'cac_registration_date' => $request['cac_date'],
                'logo_url' => $logo_url,
            ]);

            Investment::create([
                'investor_id' => $request['investor_id'],
                'amount' => $request['amount'],
                'startup_id' => $newSt['id'],
                ]);

            return redirect('admin/startups')
                    ->with('message', 'New startup Added to List');
        }
    }

    public function editStartupPage($id){
        $startup = Startup::find($id);
        $user = User::find($startup['user_id']);
        $industry = Industry::find($startup['industry_id']);
        $users = User::all();
        $industries = Industry::all();
        $investors = Investor::all();
        $investments = Investment::all();
        //return var_dump($industry);
        //return $investments->where('startup_id', $startup['id'])->get();
        return view('admin.edit_startup')
                ->with('user', $user)
                ->with('startup', $startup)
                ->with('industry', $industry)
                ->with('users', $users)
                ->with('industries', $industries)
                ->with('investors', $investors)
                ->with('investments', $investments);
    }

    public function editStartup(Request $request, $id){
        $startup = Startup::find($id);
        
        $data = $request->all();
        //return $request->all();
        
        //return $data;
        if($request->hasFile('logo_url')){
            $file = $request->file('logo_url');
            $destination_path = base_path().'/public/startup_logos/';
            $file_name = $id.'.'.$file->extension();
            $file->move($destination_path, $file_name);
            $logo_url = '/startup_logos/'.$file_name;
            $startup->update(['logo_url' => $logo_url,]);
            //return "file saved";
        }

        $validator = Validator::make($data, [
            'startup_name' => 'required|max:255',
            'address' => 'required|max:255',
            'brief_profile' => 'required',
            'user_id' => 'required',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/startup/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        else{
            //return "yes";
            $newSt = $startup->update([
                'startup_name' => $request['startup_name'],
                'address' => $request['address'],
                'startup_brief' => $request['brief_profile'],
                'user_id' => $request['user_id'],
                'industry_id' => $request['industry_id'],
                'confirmation_status' => $request['status'] ? 1 : 0,
                'trending' => $request['trending'] ? 1 : 0,
                'cac_registered' => $request['cac_registered'] ? 1 : 0,
                'cac_registration_date' => $request['cac_date'],
                
            ]);

         /*   $investment = Investment::where('startup_id', $startup['id'])->first();

           // return $investment;
            $investment->update([
                'investor_id' => $request['investor_id'],
                'amount' => $request['amount'],
                'startup_id' => $newSt['id'],
                ]);*/

            return redirect('admin/startups')
                    ->with('message', 'Startup Details updated');
        }
    
    }

    public function deleteStartup($id){
        $startup = Startup::find($id);
        //var_dump($startup);

        //echo $startup['startup_name']; 
        $startup->delete();
        return redirect()->back();
    }

    public function searchStartup(Request $request){
        //return $request->all();
        $industries = Industry::all();
        $query = $request['search'];
        $startups = Startup::where('startup_name', 'LIKE', '%'.$query.'%');
        $total = $startups->count();
        
        return view('admin.startups')
                    ->with('startups', $startups->get())
                    ->with('industries', Industry::all())
                    ->with('query', $query)
                    ->with('total', $total);
    }
}
