<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\School;

class SchoolController extends Controller
{
    //
    public function createSchoolPage(){
        return view('admin.create_school');
    }

    public function createSchool(Request $request){
    	$data = $request->all();

        //return $data;
        $validator = Validator::make($data, [
            'name' => 'required|unique:schools',
            'school_location' => 'required',
        ]);

        

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/school/create')
                ->withErrors($validator)
                ->withInput();
        }

        else{
        	//return 'yes';
            $url = NULL;
	        if ($request->hasFile('school_logo')){
	            $file = $request->file('school_logo');
	            $destination_path = base_path().'/public/school_logo/';
	            $file_name = $request['name'].'.'.$file->extension();
	            $file->move($destination_path, $file_name);
	            $url = '/school_logo/'.$file_name;
	            //return "file saved";
	        }

            School::create([
                'name' => $request['name'],
                'location' => $request['school_location'],
                'brief_summary' => $request['school_brief'],
                //'known_for' => $request['known_for'],
                'logo_url' => $url,
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/schools')->with('success', 'New School added');
        }
    }

    public function editSchoolPage($id){
        $school = School::find($id);
        return view('admin.edit_school')->with('school', $school);
    }

    public function editSchool(Request $request, $id){
        $data = $request->all();

        $school = School::find($id);
        //return $data;
        $validator = Validator::make($data, [
            'name' => 'required',
            'school_location' => 'required',
        ]);

        

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/school/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        else{
            //return 'yes';
            $url = NULL;
            if ($request->hasFile('school_logo')){
                $file = $request->file('school_logo');
                $destination_path = base_path().'/public/school_logo/';
                $file_name = $request['name'].'.'.$file->extension();
                $file->move($destination_path, $file_name);
                $url = '/school_logo/'.$file_name;
                //return "file saved";
            }

            $school->update([
                'name' => $request['name'],
                'location' => $request['school_location'],
                'brief_summary' => $request['school_brief'],
                'known_for' => $request['known_for'],
                'logo_url' => $url,
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/schools')->with('success', 'School updated');
        }
    }

    public function deleteSchool($id){
        $school = School::find($id);
        $school->delete();
        return redirect('admin/schools')->with('success', 'School deleted');
    }
}
