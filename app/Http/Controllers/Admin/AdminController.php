<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Startup;
use App\Skill;
use App\School;
use App\Company;
use App\Investor;
use App\Investment;
use App\Event;
use App\Industry;
use App\Project;

class AdminController extends Controller
{
    //
    function index(){
    	$users = User::all();
    	$startups = Startup::all();
        $companies = Company::all();
        $investors = Investor::all();

    	return view('admin.home', [
    		'users' => $users,
    		'startups' => $startups,
            'investors' => $investors,
            'companies' => $companies,
    		]);
    }

    public function showUsers(){
    	$users = User::all();
    	$schools = School::all();

    	return view('admin.users')->with('users', $users)->with('schools', $schools);
    }

    public function showStartups(){
    	$startups = Startup::all();
        $users = User::all();
        $investors = Investor::all();
        $investment = Investment::all();
        $industries = Industry::all();
    	//return $startups->count();
    	return view('admin.startups')->with('startups', $startups)
                            ->with('users', $users)
                            ->with('industries', $industries);
    }

    public function showEvents(){
        $events = Event::all();
        $industries = Industry::all();
        return view('admin.events')->with('events', $events)->with('industries', $industries);
    }

    public function showSchools(){
        $schools = School::all();
        return view('admin.schools')->with('schools', $schools);
    }

    public function showInvestors(){
        $investors = Investor::all();
        $companies = Company::all();
        $startups = Startup::all();
        return view('admin.investors')
                    ->with('investors', $investors)
                    ->with('companies', $companies)
                    ->with('startups', $startups);
    }

    public function showCompanies(){
        $companies = Company::all();
        $industries = Industry::all();
        return view('admin.companies')->with('companies', $companies)->with('industries', $industries);
    }

    public function showIndustries(){
        $industries = Industry::all();
        return view('admin.industries')->with('industries', $industries);
    }

    public function showInvestments(){
        $investments = Investment::all();
        $investors = Investor::all();
        $startups = Startup::all();
        return view('admin.investments')    
                        ->with('investments', $investments)
                        ->with('investors', $investors)
                        ->with('startups', $startups);
    }
}
