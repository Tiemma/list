<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\School;
use App\Skill;
use App\Project;

class UserController extends Controller
{

    public function listUser($id){
    	//
    }

    public function createUserPage(){
        $schools = School::all();
        return view('admin.create_user');
    }

    public function createUser(Request $request){
    	$data = $request->all();

        $validator = Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/user/create')->with('error', 'Something Happened');
        }

        else{
            User::create([
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'phone_number' => $request['phone_number'],
                'dob' => $request['DOB'],
                'sex' =>$request['gender'],
                'confirmation_status' => $request['status'] ? 1 : 0,
            ]);

            return redirect('/admin/users')->with('success', 'New User Added to List');
        }
    }

    public function editUserPage($id){
        $user = User::find($id);
        $schools = School::all();
        $skills = Skill::where('user_id', $user['id'])->whereRaw('id = (select max(`id`) from skills)')->first()['name'];
        //return $skills;
        return view('admin.edit_user')
                ->with('user', $user)
                ->with('schools', $schools)
                ->with('skills', $skills);
    }

    public function editUser(Request $request, $id){
        $data = $request->all();
        $user = User::find($id);

        //return $data;
        $validator = Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/user/'.$id.'/edit')->with('error', 'Something Happened');
        }

        //return $data;
        

        if ($request->hasFile('profile_pics_url')){
            $file = $request->file('profile_pics_url');
            $destination_path = public_path().'/profile_pics/';
            $file_name = $id.'.'.$file->extension();
            $file->move($destination_path, $file_name);
            $url = '/profile_pics/'.$file_name;
            $user->update(['profile_pics_url' => $url,]);
        }

        $social_channels = ['twitter' => $request->twitter, 
                            'facebook' => $request->facebook, 
                            'linkedin' => $request->linkedin,];
        $social_channels = serialize($social_channels);

        if ($data['password'] != NULL)
            $user->update(['password' => $request->password,]);

        $user->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone_number' => $request['phone_number'],
            'DOB' => $request['DOB'],
            'brief_profile' => $request['brief_profile'],
            'sex' => $request['gender'],
            'confirmation_status' => $request['status'] ? 1 : 0,
            'social_channel' => $social_channels,
            'school_id' => $request['school_id'],
        ]);

        Skill::updateOrCreate([
            'user_id' => $user['id'],
            'name' => $request['skills'],
            ]);

        //return Skill::where('user_id', $user['id'])->whereRaw('id = (select max(`id`) from skills)')->get();

        //return $
        Project::updateOrCreate([
            'user_id' => $user['id'],
            'name' => $request['project_name'],
            'role' => $request['project_role'],
            'description' => $request['project_description'],
            ]);
        
        return redirect('/admin/users');
    }

    public function deleteUser($id){
        $user = User::find($id);
        //var_dump($user);

        echo $user['first_name']; 
        $user->delete();
        return redirect('/admin/users');
    }

    public function searchUser(Request $request){
        //return $request->all();
        $query = $request['search'];
        $users = User::where('first_name', 'LIKE', '%'.$query.'%')->orWhere('last_name', 'LIKE', '%'.$query.'%');
        $total = $users->count();

        return view('admin.users')
                    ->with('users', $users->get())
                    ->with('query', $query)
                    ->with('total', $total);
    }
}
