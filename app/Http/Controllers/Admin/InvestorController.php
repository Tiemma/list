<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Startup;
use App\Investor;
use App\Company;

class InvestorController extends Controller
{
    public function createInvestorPage(){
    	$startups = Startup::all();
    	$companies = Company::all();
        return view('admin.create_investor')->with('startups', $startups)->with('companies', $companies);
    }

    public function createInvestor(Request $request){
    	$data = $request->all();

        //return $data;
        $validator = Validator::make($data, [
            'investor_name' => 'required',
            'email' => 'required|unique:investors',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/investor/create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
        	//return 'yes';
            $url = '/images/techpoint.png';
	      /*  if ($request->hasFile('profile_pic')){
	            $file = $request->file('profile_pic');
	            $destination_path = base_path().'/public/investor_profile_pic/';
	            $file_name = $request['email'].'.'.$file->extension();
	            $file->move($destination_path, $file_name);
	            $url = '/investor_profile_pic/'.$file_name;
	            //return "file saved";
	        }*/

            $newevent = Investor::create([
                'name' => $request['investor_name'],
                'DOB' => $request['DOB'],
                'email' => $request['email'],
                'phone_number' => $request['investor_phone'],
                'brief_profile' => $request['investor_profile'],
                'profile_pics_url' => $url,
                'company_id' => $request['company_id'],
                'startup_id' => $request['startup_id'],
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/investors')->with('success', 'New School added');
        }
    }

    public function editInvestorPage($id){
        $investor = Investor::find($id);
        $startups = Startup::all();
        $companies = Company::all();
        return view('admin.edit_investor')->with('investor', $investor)->with('startups', $startups)->with('companies', $companies);
    }

    public function editInvestor(Request $request, $id){
        $data = $request->all();

        $investor = Investor::find($id);
        //return $data;
        $validator = Validator::make($data, [
            'investor_name' => 'required',
            'email' => 'required',
        ]);

        if($validator->fails()){
            return $validator->errors()->all();
            return redirect('/admin/investor/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            //return 'yes';
            $url = NULL;
            if ($request->hasFile('profile_pic')){
                $file = $request->file('profile_pic');
                $destination_path = base_path().'/public/investor_profile_pic/';
                $file_name = $id.'.'.$file->extension();
                $file->move($destination_path, $file_name);
                $url = '/investor_profile_pic/'.$file_name;
            }

                $investor = $investor->update([
                'name' => $request['investor_name'],
                'DOB' => $request['DOB'],
                'email' => $request['email'],
                'phone_number' => $request['investor_phone'],
                'brief_profile' => $request['investor_profile'],
                'profile_pics_url' => $url,
                'company_id' => $request['company_id'],
                'startup_id' => $request['startup_id'],
                'trending' =>  $request['trending'] ? 1 : 0,
            ]);

            return redirect('admin/investors')->with('success', 'Updated Investor details');
        }
    }

    public function deleteInvestor(Request $request, $id){
        $investor = Investor::find($id);
        //var_dump($user);

        echo $investor['first_name'];
        //return $request->all;
        $investor->delete();
        return redirect('/admin/investors')->with('success', 'Investor deleted');
    }

    public function searchInvestor(Request $request){
        //return $request->all();
        $query = $request['search'];
        $investors = Investor::where('name', 'LIKE', '%'.$query.'%');
        $total = $investors->count();

        return view('admin.investors')
                    ->with('investors', $investors->get())
                    ->with('query', $query)
                    ->with('total', $total);
    }
}
