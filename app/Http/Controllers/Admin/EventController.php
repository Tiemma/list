<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;
use App\Industry;

class EventController extends Controller
{
	public function createEventPage(){
        $industries = Industry::all();
        return view('admin.create_event')->with('industries', $industries);
    } 

    public function createEvent(Request $request){
    	$data = $request->all();


        //return $data;
        $validator = Validator::make($data, [
            'event_name' => 'required',
            'event_address' => 'required',
            'event_brief' => 'required',
            'event_contact_name' => 'required',
            'event_contact_email' => 'required',
        ]);

        

        if($validator->fails()){
            //return $validator->errors()->all();
            return redirect('/admin/event/create')
                ->withErrors($validator)
                ->withInput();
        }

        else{
        	$url = NULL;
	        if ($request->hasFile('event_logo')){
	            $file = $request->file('event_logo');
	            $destination_path = base_path().'/public/event_logo/';
	            $file_name = $request['event_name'].'.'.$file->extension();
	            $file->move($destination_path, $file_name);
	            $url = '/event_logo/'.$file_name;
	            //return "file saved";
	        }

            $newevent = Event::create([
                'name' => $request['event_name'],
                'address' => $request['event_address'],
                'brief_profile' => $request['event_brief'],
                'contact_name' => $request['event_contact_name'],
                'contact_email' => $request['event_contact_email'],
                'contact_phone_no' => $request['event_contact_phone'],
                'industry_id' => $request['industry_id'],
                'logo_url' => $url,
                'trending' =>  $request['trending'] ? 1 : 0,
                'registration_link' => $request['registration_link'],
                //'industry_id' => $request['industry_id'] ? $request['industry_id'] : 1,
            ]);

            return redirect('admin/events')->with('success', 'New Event added');
        }
    }

    public function editEventPage($id){
        $event = Event::find($id);
        $industries = Industry::all();
        return view('admin.edit_event')->with('event', $event)->with('industries', $industries);
    }

    public function editEvent(Request $request, $id){
        $data = $request->all();

        $event = Event::find($id);
        //return $data;
        $validator = Validator::make($data, [
            'event_name' => 'required',
            'event_address' => 'required',
            'event_brief' => 'required',
            'event_contact_name' => 'required',
            'event_contact_email' => 'required',
        ]);

        

        if($validator->fails()){
            //return $validator->errors()->all();
            return redirect('/admin/event/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        else{
            $url = NULL;
            if ($request->hasFile('event_logo')){
                $file = $request->file('event_logo');
                $destination_path = base_path().'/public/event_logo/';
                $file_name = $request['event_name'].'.'.$file->extension();
                $file->move($destination_path, $file_name);
                $url = '/event_logo/'.$file_name;
                //return "file saved";
            }

            $event->update([
                'name' => $request['event_name'],
                'address' => $request['event_address'],
                'brief_profile' => $request['event_brief'],
                'contact_name' => $request['event_contact_name'],
                'contact_email' => $request['event_contact_email'],
                'contact_phone_no' => $request['event_contact_phone'],
                'industry_id' => $request['industry_id'],
                'logo_url' => $url,
                'trending' =>  $request['trending'] ? 1 : 0,
                'registration_link' => $request['registration_link'],
                //'industry_id' => $request['industry_id'] ? $request['industry_id'] : 1,
            ]);

            return redirect('admin/events')->with('success', 'Event updated');
        }
    }

    function deleteEvent(Request $request, $id){
        $event = Event::find($id);
        $event->delete();
        return redirect('/admin/events')->with('success', 'Event deleted');
    }
}
