<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\School;

use DB;

class SchoolController extends Controller
{
    //
    public function index(){
        $schools = School::orderBy('id', 'ASC')->paginate(50);
        return view('user.schools')->with('schools', $schools);
    }

    public function school($id){
        $school = School::where('id', $id)->get()->first();
        return view('user.school')->with('school', $school);
    }

    public function search(Request $request){
        $name = $request->input('name');

        if(isset($name)){

            $user = DB::table('schools')
                ->where('name', 'like', '%'.$name.'%')
                ->take(50)->get(array('id', 'name', 'logo_url', 'location'));


            return response()
                ->json(['user' => $user]);
        }
    }
}
