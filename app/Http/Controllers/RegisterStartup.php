<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Startup as Startup;

class RegisterStartup extends Controller
{
    //

    function show(){
        return view('startup');
    }

    function register(Request $request){
        $startup = Startup::where('user_id','=',Auth::user()->id)->get();
        $data=array();
        $data['startup_name']=$request->name;
        $data['startup_brief']=$request->short_description;
        /**
         * Missing fields
         * - category
         * - website
         *  - social media fields (facebook, linkedin etc)
         *  - phone_number
         *  - email
         *  - long_description
         */
        var_dump($request->all());
    }

}
