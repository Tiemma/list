<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

use DB;
use App\Http\Requests;

class EventsController extends Controller
{
    //

    public function index($id){
        $event = Event::where('id', $id)->first();

        return view('user.event')->with('event', $event);
    }

    public function events(){
        $events = Event::orderBy('created_at', 'ASC')->get()->take(20);
        return view('user.events')->with('events', $events);
    }

    public function show(Request $request){
        $number = $request->number;
        $event = Event::where('id', '>', $number)->get()->take(20);
        return response()
            ->json(['user' => $event]);
    }

    public function search(Request $request){
        $name = $request->input('name');

        if(isset($name)){

            $event = DB::table('events')
                ->where('name', 'like', '%'.$name.'%')
                ->get(array('id', 'name', 'logo_url', 'address', 'event_date'));

            return response()
                ->json(['event' => $event]);
        }
    }


}
