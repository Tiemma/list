<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Startup;

use Auth;

use DB;

class StartupSearchController extends Controller
{
    //
    public function register(Request $request){
        $name = explode(' ', preg_replace('/\s+/', ' ', $request->input('name')));

        if(isset($name[1])) {

            $user = DB::table('users')
                ->where([
                    ['first_name', 'like', '%'.$name[0].'%'],
                    ['last_name', 'like', '%'.$name[1].'%'],
                ])
                ->orWhere('email', 'like', '%'.$name[0].'%')
                ->take(10)->get(array('first_name', 'last_name', 'profile_pics_url',  'email'));


            return response()
                ->json(['user' => $user]);

        }else{

            $user = DB::table('users')
                ->where('first_name', 'like', '%'.$name[0].'%')
                ->orWhere('last_name', 'like', '%'.$name[0].'%')
                ->orWhere('email', 'like', '%'.$name[0].'%')
                ->take(10)->get(array('first_name', 'last_name', 'profile_pics_url', 'email'));


            return response()
                ->json(['user' => $user]);
        }

    }


    public function update(Request $request){


       $founder_email =  Startup::where('id', '=', $request->input('id'))->get(array('founder_name'));
       $email = array();
       foreach($founder_email as $emails){
           $email = $emails->founder_name;
       }
       return response()->json(['user'=>DB::table('users')->whereIn('email', unserialize($email))->get(array('first_name', 'last_name', 'profile_pics_url', 'email'))]);
    }
}
