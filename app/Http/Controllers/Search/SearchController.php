<?php

namespace App\Http\Controllers\Search;

use App\Investor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Startup;
use App\User;
use App\Project;
use App\Company;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Psy\Util\Json;

class SearchController extends Controller{


    public function search(Request $request){

        $type = $request->input('type');
        $users_result= $this->searchUsers($request);
        $startups_result = $this->searchStartup($request);
        $company_result = $this->searchCompanies($request);

        if($type == 'user' || !isset($type)){
            return view('user.search')
                ->with('users',$users_result[0])
                ->with('search',$users_result[1]);
        }
        if($type == 'startup'){
            return view('user.search')
                ->with('startups',$startups_result[0])
                ->with('search',$startups_result[1]);
        }

        if($type == 'company'){
            return view('user.search')
                ->with('companies',$company_result[0])
                ->with('search',$company_result[1]);
        }
        return view('user.search')->with('error', 'No Search Found');
    }


    public function searchUsers($request){
        $search = $request->input('search');
        $user = json_decode(User::where('first_name', 'LIKE', '%' . $search . '%')
            ->orwhere('last_name', 'LIKE', '%' . $search . '%')
            ->get());
             return [$user, $search];
    }

    public function searchStartup($request){
        $location = $request->input('location');
        $startup_name = $request->input('search');
        $category = $request->input('category');

        if(isset($location) && isset($category)){
            $results = json_decode(Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->where('address','LIkE', '%'. $location.'%')
                ->where('category', $category)
                ->get());
                return [$results, $startup_name];
        }

        elseif(isset($category)){
            $results = json_decode(Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->where('category', $category)
                ->get());
            return [$results, $startup_name];
        }

        elseif(isset($location)){
            $results = json_decode(Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->where('address','LIkE', '%'. $location.'%')
                ->get());
                return [$results, $startup_name];
        }
        $results = json_decode(Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
            ->get());
        return [$results, $startup_name];
    }

    function searchCompanies($request){
        $company_name = $request->input('search');
        $results = json_decode(Company::where('name', 'LIKE', '%' . $company_name . '%')
            ->get());
        return [$results, $company_name];
    }
}
