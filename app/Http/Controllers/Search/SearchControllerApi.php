<?php

namespace App\Http\Controllers\Search;

use App\Investor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Startup;
use App\User;
use App\Project;
use App\Company;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Psy\Util\Json;

class SearchControllerApi extends Controller
{


    public function search(Request $request){
        return view('search');
    }

    public function searchUser(Request $request){
        $name = $request->input('search');
        $user = User::where('first_name', 'LIKE', '%' . $name . '%')
            ->orwhere('last_name', 'LIKE', '%' . $name . '%')
            ->get();
            $results = array("status"=>"ok", "data"=>$user);
            return response()->json($results);

    }

    public function searchInvestor(Request $request){
        $investor = $request->input('search');
        $results = Investor::where('name', 'LIKE', '%' . $investor . '%')
            ->get();
            return response()->json(["status"=>"ok","data"=>$results]);
    }

    public function searchStartup(Request $request){
        $location = $request->input('location');
        $startup_name = $request->input('search');
        $category = $request->input('category');
        if(isset($location) && isset($category)){
            $results = Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->where('address','LIkE', '%'. $location.'%')
                ->where('category', $category)
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
        }

        if(isset($category)){
            $results = Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->where('category', $category)
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
        }
        if(isset($location)){
            $results = Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->where('address','LIkE', '%'. $location.'%')
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
        }
        $results = Startup::where('startup_name', 'LIKE', '%' . $startup_name . '%')
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);;
    }


    public function searchProject(Request $request){
        $project_name = $request->input('search');
        $category = $request->input('category');

        if(isset($project_name) && isset($category)){
            $results = Project::where('name', 'LIKE', '%' . $project_name . '%')
                ->where('category', $category)
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
        }
        $results = Project::where('name', 'LIKE', '%'.$project_name.'%')
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
    }

    public function searchCompany(Request $request, $company, $category, $location){
        $location = $request->input('location');
        $company = $request->input('search');
        $category = $request->input('category');
        if(isset($location) && isset($category)){
            $results = Company::where('name', 'LIKE', '%' . $company . '%')
                ->where('location','LIkE', '%'. $location.'%')
                ->where('category', $category)
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
        }

        if(isset($category)){
            $results = Company::where('name', 'LIKE', '%' . $company . '%')
                ->where('category', $category)
                ->get();
                 return response()->json(["status"=>"ok","data"=>$results]);
        }
        if(isset($location)){
            $results = Company::where('name', 'LIKE', '%' . $company . '%')
                ->where('location','LIkE', '%'. $location.'%')
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
        }

        $results = Company::where('name', 'LIKE', '%'.$company.'%')
                ->get();
                return response()->json(["status"=>"ok","data"=>$results]);
    }

    

}
