<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use League\Flysystem\Exception;

use Socialite;

use Auth;

use DB;

use App\User;

use Illuminate\Routing\Redirector as Redirect;

class SocialController extends Controller
{
    //
    public function facebook(Redirect $redirect){
            return Socialite::driver('facebook')->redirect();
    }

    public function twitter(){
        return Socialite::driver('twitter')->redirect();
    }

    public function linkedin(){
        return Socialite::driver('linkedin')->redirect();
    }

    public function callback_facebook(Redirect $redirect, Request $request){
        if(isset($request->code)) {

            $user = Socialite::driver('facebook')->user();

            $check_user = User::where('email', '=', $user->getEmail())->first();

            if (count($check_user) == 1) {

                Auth::login($check_user);
                return $redirect->to('/home');

            }

            //Get the total number of users
            $numberOfUsers = count(User::all());

            $new_user = new User;

            $name = explode(' ', $user->name);
            $new_user->first_name = $name[0];
            $new_user->last_name = $name[1];
            $new_user->email = $user->email;

            //This saves the user's avatar
            $new_user->profile_pics_url = $user->avatar_original;

            $new_user->save();

            //Login and remember the user
            $check_user = User::where('email', '=', $user->getEmail())->first();

            if (count($check_user) == 1) {

                Auth::login($check_user);
                return $redirect->to('/home');

            }
        }

        //return $redirect->to('/home');

    }

    public function callback_twitter(Redirect $redirect, Request $request)
    {
        //This is to prevent an error that happens when the user doesnt authenticate
        //It happens if he does something like clicking cancel on the login page for the media channel
        if (isset($request->oauth_token)) {


            /*
             * This section is if an email exists for the user
             */
            $user = Socialite::driver('twitter')->user();

            if(NULL !== $user->getEmail()) {
                $check_user = User::where('email', '=', $user->getEmail())->first();

                if (count($check_user) == 1) {
                    Auth::login($check_user);
                    return $redirect->to('/home');
                }
            }

            /*
             * This section is for already registered users but with no email
             * PS. You can sign up on twitter without an email
             */
            //My twitter account doesnt have an email field so this
            //is the best I could think of to prevent any error
            //and also log me in
            $name = explode(' ', $user->name);
            if (!(isset($check_user))) {
                $check_user = DB::table('users')
                    ->where([
                        ['first_name', '=', $name[0]],
                        ['last_name', '=', $user->nickname]
                    ])->first();
                if (count($check_user) == 1) {
                    Auth::loginUsingId($check_user->id);
                    return $redirect->to('/home');
                }
            }

            /*
             * This is for non0-registered users with no data
             * First time users
             */
            $numberOfUsers = count(User::all());

            $new_user = new User;

            //This saves the user's avatar
            $new_user->profile_pics_url = $user->avatar_original;

            $name = explode(' ', $user->name);
         
            $new_user->first_name = $name[0];
            $new_user->last_name = $user->nickname;
            $new_user->email = $user->email;
            $new_user->save();


            $check_user = DB::table('users')
                ->where([
                    ['first_name', '=', $name[0]],
                    ['last_name', '=', $user->nickname]
                ])->first();
            if (count($check_user) == 1) {
                Auth::loginUsingId($check_user->id);
                return $redirect->to('/home');
            }
        }

        return $redirect->to('/home');

    }

    public function callback_linkedin(Redirect $redirect, Request $request){
        //Get the user's details
        if(isset($request->code)) {
            $user = Socialite::driver('linkedin')->user();

            //Check if the user is registered in our database
            $check_user = User::where('email', '=', $user['emailAddress'])->first();

            //Log him in if so else register him
            if (count($check_user) == 1) {
                Auth::login($check_user);
                return $redirect->to('/home');
            }

            $numberOfUsers = count(User::all());

            //I hardcoded the data fetched by the api
            //This was because some of the data been offered doesn't remain the same
            //Facebook's names are in a different manner than linkedin's name
            //I did so remove the case of debugging unnecessary errors later on
            $user = $user->user;

            $new_user = new User;

            $new_user->first_name = $user['firstName'];
            $new_user->last_name = $user['lastName'];
            $new_user->email = $user['emailAddress'];
            $new_user->profile_pics_url = $user['pictureUrls']['values'][0];

            $new_user->save();

            $check_user = User::where('email', '=', $user->getEmail())->first();

            //Log him in after registering
            if (count($check_user) == 1) {
                Auth::login($check_user);
                return $redirect->to('/home');
            }


        }

        return $redirect->to('/home');

        }

}



