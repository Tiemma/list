<?php

namespace App\Http\Controllers\AdminAuth;

use App\Admin;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class AuthController extends Controller
{

    public function showLoginForm()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        if(Auth::guard('admin')->check())
            return redirect('admin/home');

        return view('admin.auth.login');
    }


    public function showRegistrationForm()
    {
        if(Auth::guard('admin')->check())
            return redirect('admin');
        return view('admin.auth.register');
    }  

    
    public function doLogin(Request $request)
    {

        $data = $request->all();

        $rules = array(
            'email'    => 'required',
            'password' => 'required'
        );

        $validator = Validator::make($data, $rules);

        if($validator->fails()){
            //return 0;
            return redirect('admin/login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else{
            $email = $request['email'];
            $password = $request['password'];

            if (Auth::guard('admin')->attempt(['email' => $email, 'password' => $password])) {
                return redirect('admin/home');
            } 
            else{
                return redirect('admin/login')
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            }
        }

    }

    public function doLogout(){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    public function doRegister(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6|confirmed',
        ]);

        if($validator->fails()){
            return $validator->errors();
        }

        else{
            //return 'yes';
            $newAdmin = Admin::create([
                'name' => $request['name'],
                'username' => $request['username'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
            ]);

            Auth::guard('admin')->login($newAdmin);
            return redirect('admin/home');
        }
        
   
    }
}