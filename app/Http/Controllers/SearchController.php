<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;

use App\Http\Requests;

class SearchController extends Controller
{

    private $startup;
    private $users;
    private $projects;
    private $investors;
    private $companies;
    private $array;

    public function startup(Request $request){
        $name = $request->get('search');
        $results = \App\Startup::where('startup_name', 'LIKE', '%' . $name . '%')->get();
        $this->startup =  '<a href="#" class="collection-item" style="color: white;font-weight:bolder; background-color: #4F9ACC; "><h4 style="font-weight: bolder">Startups</h4></a>';
        $this->array["startup"] = count($results);
        if (count($results) > 0) {
            $results->each(function ($name) {
                $startup_name = $name->startup_name;
                $this->startup .= '<a href="#" class="collection-item" style="color: black">'.$startup_name.'</a>';
            });
            return $this->startup;
        }else{
            $this->startup .= '<a href="#" class="collection-item" style="color: black">No startups found</a>';
        }

    }

    public function user(Request $request){
        $name = $request->get('search');
        $results = \App\User::where('first_name', 'LIKE', '%' . $name . '%')->orwhere('last_name', 'LIKE', '%' . $name . '%')->get();
        $this->users =  '<a href="#" class="collection-item" style="color: white;font-weight:bolder; background-color: #4F9ACC; "><h4 style="font-weight: bolder">Users</h4></a>';
        $this->array["users"] = count($results);
        if (count($results) > 0) {
            $results->each(function ($name) {
                $user = $name->first_name.' '.$name->last_name;
                $this->users .= '<a href="#" class="collection-item" style="color: black">'.$user.'</a>';
            });
        }else{
            $this->users .= '<a href="#" class="collection-item" style="color: black">No users found</a>';
        }
    }

    public function investor(Request $request){
        $name = $request->get('search');
        $results = \App\Investor::where('name', 'LIKE', '%' . $name . '%')->get();
        $this->array["investors"] = count($results);
        $this->investors = '<a href="#" class="collection-item" style="color: white;font-weight:bolder; background-color: #4F9ACC; "><h4 style="font-weight: bolder">Investors</h4></a>';
        if (count($results) > 0) {
            $results->each(function ($name) {
                $investor = $name->name;
                $this->investors .= '<a href="#" class="collection-item" style="color: black">'.$investor.'</a>';
            });
        }else{
            $this->investors .= '<a href="#" class="collection-item" style="color: black">No investors found</a>';
        }
    }

    public function company(Request $request){
        $name = $request->get('search');
        $results = \App\Company::where('name', 'LIKE', '%' . $name . '%')->get();
        $this->array["company"] = count($results);
        $this->companies = '<a href="#" class="collection-item" style="color: white;font-weight:bolder; background-color: #4F9ACC; "><h4 style="font-weight: bolder">Companies</h4></a>';
        if (count($results) > 0) {
            $results->each(function ($name) {
                $company = $name->name;
                $this->companies .= '<a href="#" class="collection-item" style="color: black">'.$company.'</a>';
            });
        }else{
            $this->companies .= '<a href="#" class="collection-item" style="color: black">No companies found</a>';
        }
    }

    public function project(Request $request){
        $name = $request->get('search');
        $results = \App\Project::where('name', 'LIKE', '%' . $name . '%')->get();
        $this->array["project"] = count($results);
        $this->projects = '<a href="#" class="collection-item" style="color: white;font-weight:bolder; background-color: #4F9ACC; "><h4 style="font-weight: bolder">Projects</h4></a>';
        if (count($results) > 0) {
            $results->each(function ($name) {
                $project = $name->name;
                $this->projects .= '<a href="#" class="collection-item" style="color: black">'.$project.'</a>';
            });
        }else{
            $this->projects .= '<a href="#" class="collection-item" style="color: black">No projects found</a>';
        }
    }


    //
   public function search(Request $request)
   {
       $type=$_COOKIE['type'];
       if($type == 'All'){
           $this->project($request);
           $this->user($request);
           $this->company($request);
           $this->investor($request);
           $this->startup($request);
           $project = $this->projects;
           $company = $this->companies;
           $investor = $this->investors;
           $user = $this->users;
           $startup = $this->startup;
           arsort($this->array);
           $user_data = array("project"=>$project,"company"=> $company,"investors"=> $investor,"users"=> $user,"startup"=> $startup);
           foreach($this->array as $key=>$value ){
                echo $user_data[$key];
           }
       }elseif($type == 'User'){
           $this->user($request);
           echo $this->users;
       }elseif($type=='Investor'){
           $this->investor($request);
           echo $this->investors;
       }elseif($type=='Project'){
           $this->project($request);
           echo $this->projects;
       }elseif($type=='Company'){
           $this->company($request);
           echo $this->companies;
       }elseif($type=='Startup'){
           $this->startup($request);
           echo $this->startup;
       }

    }
}
