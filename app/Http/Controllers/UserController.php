<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Redirector;

use App\Http\Requests;
use App\School;
use App\User;
use App\Skill;
use App\Startup;
use App\Project;
use App\WorkExperience as Experience;
use App\Rating;

use Auth;

class UserController extends Controller
{

    public function getUpdate()
    {
        $user = User::where('email','=',Auth::user()->email)->first();
        $school = School::lists('name');
        $school_id = $user->school_id;
        if($school_id != NULL)
            $user_school = School::where('id','=',$school_id )->get()->first();
        else{
            $user_school = array();
        }

        $project = Project::where('user_id','=', Auth::user()->id)->get();
        $experience = Experience::where('user_id','=', Auth::user()->id)->get();
        $skill = Skill::where('user_id','=', Auth::user()->id)->get(array('name'))->first();
        $months = array('', 'January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December');

        if(count($user) == 1){
            return view('user.update_user')
                ->with('user', $user)
                ->with('schools', $school )
                ->with('months', $months)
                ->with('projects', $project)
                ->with('experiences', $experience)
                ->with('user_school', $user_school)
                ->with('skills', $skill);
        }

    }

    public function postUpdate(Request $request, Redirector $redirect){
        $rules = array(
            'name'=>'regex:/^[\pL\s\-]+$/u|required',
            'email'=>'email|required',
            'phone_number'=>'numeric|required',
            'gender'=>'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if($validator->passes()){
            /*
             * The collective data
             */
            $user = User::where('email','=',Auth::user()->email)->get();
            $school  = School::where('name','=', $request->school)->first();
            $name = explode(' ', $request->name);
            $skills = trim($request->input('skills'));

            /*
             * The user array
             */
            $user_array = array(
                'first_name'=>$name[0],
                'last_name'=>$name[1],
                'email'=>$request->email,
                'phone_number'=>$request->phone_number,
                'dob'=>$request->day.' '.$request->month.' '.$request->year,
                'school_id'=>$school['original']['id'],
                'sex'=>$request->gender,
                'brief_profile'=>$request->profile
            );

            /*
             * Store the users logo
             */

            if (count($request->file('logo')) > 0  and $request->remove != 'removed') {
                $file = $request->file('logo');
                $destination_path = public_path() . '/profile_pics/';
                $file_name = Auth::user()->id . '.' . $file->extension();
                $file->move($destination_path, $file_name);
                $url = 'profile_pics/' . $file_name;
                $user_array['profile_pics_url'] = $url;
            }else if($request->remove == 'removed'){
                $email_hash =  md5( strtolower( trim(Auth::user()->email) ) );
                $user_array['profile_pics_url'] = " http://www.gravatar.com/avatar/".$email_hash."?s=400";
            }

            /*
             * The project section
             */
            $project_name_array = array();
            $project_description_array = array();


            /**
             *Create an array of the user's projects
             */
            if($request->project_name && $request->project_description){
                $combined_array = array_combine($request->project_name, $request->project_description);
                foreach($combined_array as $project_name=>$project_description){
                    if(isset($project_name) and $project_name != "") {
                        $project_name_array[] = $project_name;
                        $project_description_array[] = $project_description;
                    }
                }
            }

            
            $project_array['user_id'] = Auth::user()->id;
            $project_array['name'] = serialize($project_name_array);
            $project_array['description'] = serialize($project_description_array);

            Project::updateOrCreate(['user_id'=>Auth::user()->id], $project_array);

            /*
             * The experience section
             */

            $company_name_array = array();
            $role_array = array();
            $description_array = array();
            $employed_status_array = array();
            $start_date = NULL;
            $end_date = NULL;
            $primary_work_index = NULL;

            $i = 0;

            if(count($request->company_name)) {
                foreach ($request->company_name as $company_name) {
                    if (isset($request->from_month[$i]) and isset($request->from_year[$i]) and isset($company_name)) {
                        $company_name_array[] = $company_name;
                        $role_array[] = $request->role[$i];
                        $description_array[] = $request->work_description[$i];

                        if ($request->work_status[$i] === "on")
                            $employed_status_array[] = "1";
                        else
                            $employed_status_array[] = "0";

                        $start_date .= $request->from_month[$i] . ' ' . $request->from_year[$i] . "*";

                        if ($request->work_status[$i] != "on" and (isset($request->to_month[$i]) and isset($request->to_year[$i]))) {
                            $end_date .= $request->to_month[$i] . ' ' . $request->to_year[$i] . "*";
                        }
                    }
                    ++$i;
                }
            }
                $experience_array = array();
                $experience_array['user_id'] = Auth::user()->id;
                $experience_array['company_name'] = serialize($company_name_array);
                $experience_array['role'] = serialize($role_array);
                $experience_array['description'] = serialize($description_array);
                $experience_array['employed_status'] = serialize($employed_status_array);
                $experience_array['start_date'] = $start_date;
                $experience_array['end_date'] = $end_date;
                $experience_array['primary_work_index'] = $request->input("index");
                Experience::updateOrCreate(['user_id' => Auth::user()->id], $experience_array);

            
            /*
             * Serialize the user's skills
             */
                Skill::updateOrCreate(['user_id'=>Auth::user()->id],
                    array('name' => $skills, 'user_id'=>Auth::user()->id));


            $social_array = array();
            $social_array['facebook'] = $request->facebook;
            $social_array['twitter'] = $request->twitter;
            $social_array['linkedin'] = $request->linkedin;

            $item = serialize($social_array);
            $user_array['social_channel'] = $item;

            /*
             * The final update for the user
             */
            User::where('id','=',Auth::user()->id)->update($user_array);
            $id = $user[0]['original']['id'];
            setcookie('success', 'True', time()+10);

            return $redirect->back();
        }else{
            return $redirect->back()->withErrors($validator);
        }
    }

    public function redirectToUser($id, Redirector $redirect){
        setcookie('id', $id);
        return $redirect->to('user');
    }


    public function userProfile(Redirector $redirect){

        $id = $_COOKIE['id'];
        $experience = json_decode(Experience::where('user_id', '=', $id)->get());
        $skills = json_decode(Skill::where('user_id', '=', $id)->get(array('name'))->first());
        $months = array('', 'January', 'February', 'March', 'April', 'May',
                    'June', 'July', 'August', 'September', 'October',
                    'November', 'December');
        $user = json_decode(User::where('id','=', $id)->first());
        $projects = json_decode(Project::where('user_id', '=', $id)->get());
        $startups = json_decode(Startup::where('user_id', $id)
            ->get());
       
        //If the id is the same as the user's id then redirect to the user route without an id
        if(count($user) == 0){
            return $redirect->to('home');
        }

        return view('user.user')
            ->with('user', $user)
            ->with('experience', $experience)
            ->with('months', $months)
            ->with('skills', $skills)
            ->with('startup', $startups)
            ->with('projects', $projects);

    }

    public function redirectToStartup($id, Redirector $redirect){
        setcookie('id', $id);
        return $redirect->to('startup');
    }

    public function showStartup(Redirector $redirect){
        $numberOfStartup = count(Startup::all());
        if(isset($_COOKIE['id'])) {

            $id = $_COOKIE['id'];
            $startup = json_decode(Startup::where('id','=', $id)->first());
            $founder_email = unserialize($startup->founder_name);
            $user = User::whereIn('email', $founder_email)->get();
            $rating = Rating::where('startup_id', '=', $id)->orderBy('created_at', 'DESC')->take(5)->get();
            $rating_users = array();

            foreach($rating as $rating_user){
                $rating_users[] = $rating_user->user_id;
            }

            if (count($startup) > 0) {
                return view('user.startup')
                    ->with('startup', $startup)
                    ->with('users', $user)
                    ->with('ratings', $rating)
                    ->with('rating_user', $rating_users);
            } else {
                return $redirect->to('home');
            }

        }  else{
            return $redirect->to('home');
        }
    }

    public function entrepreneur($id, Redirector $redirect){
        setcookie('id', $id);
        return $redirect->to('entrepreneur');
    }

    public function showEntrepreneur(Redirector $redirect){

        $numberOfUsers = count(User::all());
        if(isset($_COOKIE['id']) and $_COOKIE['id'] <= $numberOfUsers) {

            $id = $_COOKIE['id'];
            $startup = Startup::where('user_id', '=', $id)->get();
            $experience = json_decode(Experience::where('user_id', '=', $id)->get());
            $months = array('', 'January', 'February', 'March', 'April', 'May', 'June',
                            'July', 'August', 'September', 'October', 'November', 'December');
            $skills = json_decode(Skill::where('user_id', '=', $id)->get(array('name'))->first());
            $user = json_decode(User::where('id','=', $id)->first());

            if (count($startup) > 0) {
                return view('user.entrepreneur')->with('experience', $experience)
                    ->with('months', $months)
                    ->with('skills', $skills)
                    ->with('startup', $startup)
                    ->with('user', $user);
            } else {
                return $redirect->to('home');
            }
        }else{
            return $redirect->to('home');
        }

    }
}

