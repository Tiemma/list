<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Mail;

use App\Http\Requests;

class ResetEmailController extends Controller
{
    //
    public function index(){
        return view('email.reset_email');
    }

    public function send(Request $request){

        $email = trim($request->input('user_email'));

        $user = User::where('email', '=',  $email)->first();

        if(count($user) == 1) {


            $user->email_hash =  md5(trim($user->email));

            $user->save();

            Mail::send('email.reset_email_view', ['first_name' => $user->first_name, 
'last_name' => $user->last_name,
 'email'=>$email], function ($message)  use($email){

                $message->subject("Reset your password");

                $message->from('no-reply@techpoint.ng', 'List Admin');

                $message->to($email);

            });

            return 1;

        }else{

            return 0;

        }
    }

    public function reset($email_hash, Request $request){

        return view('email.reset_password')->with('email_hash', $email_hash);
    }

    public function user(Request $request){

        $password = $request->input('password');

        $email_hash = $request->input('email_hash');

        $user = User::where('email_hash', $email_hash)->get()->first();

        if(count($user)){
            
            $user->password = bcrypt(trim($password));

            $user->email_hash = "";
            
            $user->save();

            return 1;
        }else{

            return 0;

        }



    }

}
