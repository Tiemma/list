<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Company;

use App\Startup;

use DB;

class CompanyController extends Controller
{
    //
    public function index(){
        $company = Company::orderBy('trending', 'ASC')->paginate(25);
        $startup = Startup::orderBy('trending', 'ASC')->paginate(25);

        return view('user.companies')->with('companies', $company)->with('startups', $startup);
    }

    public function search(Request $request){
        $name = $request->input('name');

        if(isset($name)){

            $user = DB::table('companies')
                ->where('name', 'like', '%'.$name.'%')
                ->take(50)->get(array('id', 'name', 'logo_url', 'location'));


            return response()
                ->json(['user' => $user]);
        }
    }
}
